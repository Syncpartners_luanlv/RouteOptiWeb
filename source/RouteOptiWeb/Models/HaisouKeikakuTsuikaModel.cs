﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RouteOptiWeb.Common;
//using RouteOptiWeb.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; // [Required(ErrorMessage = 入力が必要です。")]
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using X.PagedList;

namespace RouteOptiWeb.Models
{
    public class HaisouKeikakuTsuikaModel
    {
        [Required(ErrorMessage = "配送日付が必要です。")]
        [Display(Name = "配送日付")]
        public DateTime Deli_Date { set; get; }

        [Display(Name = "配送区分")]
        public IEnumerable<SelectListItem> Lst_HaisouKubun { set; get; }//List box 配送区分を取得

        public string HaisouKubun { set; get; }// 配送区分を取得

        [Display(Name = "納入先コード")]
        public IEnumerable<SelectListItem> Lst_Into_Cd { set; get; }//List box 納入先コードを取得
        public string Into_Cd { set; get; }// 納入先コード

        [Display(Name = "便No")]
        public string Bin_No { set; get; }

        [Display(Name = "納入先名")]
        public string Into_Name { set; get; }


        [Display(Name = "住所")]
        public string Into_Address { set; get; }

        [Display(Name = "可能車両")]
        public string Avalible_car { set; get; }

        [Required(ErrorMessage = "納入先作業時間(分)の入力が必要です。")]
        [Display(Name = "納入先作業時間(分)")]
        [Range(0, 2147483647, ErrorMessage = "納入先作業時間(分)は正の数のみ入力可能")]
        [RegularExpression(@"^[0-9]{1,10}$", ErrorMessage = "納入先作業時間(分)は整数のみ入力可能")]
        public int Work_Time { set; get; }

        //[Required(ErrorMessage = "時間指定開始の入力が必要です。")]
        [Display(Name = "時間指定開始")]
        public string Assign_Start_Time { get; set; }

        //[Required(ErrorMessage = "時間指定終了の入力が必要です。")]
        [Display(Name = "時間指定終了")]
        public string Assign_End_Time { get; set; }
        [Required(ErrorMessage = "トラックコードが必要です。")]
        public string Track_Cd { get; set; }

        [Required(ErrorMessage = "トラックコードの入力が必要です。")]
        [Display(Name = "トラックコード")]
        public IEnumerable<SelectListItem> Lst_Track_Cd { set; get; } //List box トラックコードを取得       

        [Required(ErrorMessage = "指示Noの入力が必要です。")]
        [Display(Name = "指示No")]
        [RegularExpression("(.{1,20}$)", ErrorMessage = "指示Noは20文字まで入力可能です。")]
        public string Instruction_No { set; get; }


        //[Required(ErrorMessage = "出発時間の入力が必要です。")]
        [Display(Name = "出発時間")]
        //[RegularExpression("([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]", ErrorMessage = "出発時間は5文字まで入力可能です。")]
        public string Leave_Time { set; get; }   
        public string Arrive_Time { set; get; }// 到着時間(「---」固定でDBに保存)
        [Required(ErrorMessage = "到着順の入力が必要です。")]
        [Display(Name = "到着順")]
        [Range(0, 2147483647, ErrorMessage = "到着順は正の数のみ入力可能")]
        [RegularExpression(@"^[0-9]{1,10}$", ErrorMessage = "到着順は整数のみ入力可能")]
        public int Arrive_No { set; get; }

        [Display(Name = "有料道路使用")]
        public int Toll_Road { set; get; }
        [Required(ErrorMessage = "質量(kg)の入力が必要です。")]
        [Display(Name = "質量(kg)")]
        [RegularExpression(@"^[0-9]{1,10}$", ErrorMessage = "質量(kg)は整数のみ入力可能")]
        [Range(0, 2147483647, ErrorMessage = "質量(kg)は正の数のみ入力可能")]
        public int Shitsuryo { set; get; } //ship_weight
        public int Take_Weight { set; get; } //take_weight

        [Display(Name = "備考")]        
        [RegularExpression("(.{1,200}$)", ErrorMessage = "備考は200文字まで入力可能です。")]
        public string Remark { set; get; }

        [Display(Name = "時間指定")]
        public int Time_FLG { set; get; }
        public string ErrorMessage { get; set; }

        //積載可能質量
        public int Avaliable_Weight { set; get; } //0固定
        //配送No
        public int Deli_No { set; get; }
        //エリア名
        public string Area_Name { get; set; }
        //配送区分 type no
        public int Deli_Type_No { set; get; }
        public int Previous_Flag { set; get; }
        public int Take_Plan_Weight { set; get; }
    }

}
