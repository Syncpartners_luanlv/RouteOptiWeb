﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RouteOptiWeb.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; // [Required(ErrorMessage = 入力が必要です。")]
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using X.PagedList;

namespace RouteOptiWeb.Models
{
    public class HaisouKeikakuShuuseiModel
    {
        public string SEQID { get; set; }

        [Display(Name = "配送日付")]
        public DateTime Deli_Date { set; get; }

        [Display(Name = "配送区分")]
        [MaxLength(30)]
        public string Deli_Type_Name { set; get; }// 配送区分を取得
        public int Deli_Type_No { set; get; }

        [Display(Name = "納入先名")]
        public string Into_Name { set; get; }// 納入先名

        [Display(Name = "住所")]
        public string Into_Address { set; get; }

        [Display(Name = "可能車両")]
        public string Available_Car { set; get; }

        [Required(ErrorMessage = "納入先作業時間(分)の入力が必要です。")]
        [Display(Name = "納入先作業時間(分)")]
        [Range(0, 2147483647, ErrorMessage = "納入先作業時間は0または、正の数のみ入力可能です。")]
        [RegularExpression(@"^[0-9]{1,10}$", ErrorMessage = "納入先作業時間(分)は整数のみ入力可能")]
        public int Work_Time { set; get; }

        [Required(ErrorMessage = "時間指定の入力が必要です。")]
        [Display(Name = "時間指定")]
        public int Assign_Time_Flag { get; set; }

        //[Required(ErrorMessage = "時間指定開始の入力が必要です。")]
        [Display(Name = "時間指定開始")]
        //[RegularExpression("([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]", ErrorMessage = "時間指定開始は[HH:MM]で入力可能です。")]
        [MaxLength(5)]
        public string Assign_Start_Time { get; set; }

        //[Required(ErrorMessage = "時間指定終了の入力が必要です。")]
        [Display(Name = "時間指定終了")]
        //[RegularExpression("([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]", ErrorMessage = "時間指定終了は[HH:MM]で入力可能です。")]
        [MaxLength(5)]
        public string Assign_End_Time { get; set; }

        [Required(ErrorMessage = "トラックコードの入力が必要です。")]
        [Display(Name = "トラックコード")]
        [MaxLength(10)]
        public string Track_Cd { set; get; }

        [Required(ErrorMessage = "指示Noの入力が必要です。")]
        [Display(Name = "指示No")]
        [MaxLength(20)]
        public string Instruction_No { set; get; }

        //[Required(ErrorMessage = "出発時間の入力が必要です。")]
        [Display(Name = "出発時間")]
        //[RegularExpression("([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]", ErrorMessage = "出発時間は[HH:MM]で入力可能です。")]
        [MaxLength(5)]
        public string Leave_Time { set; get; }

        [Required(ErrorMessage = "到着順の入力が必要です。")]
        [Display(Name = "到着順")]
        public int Arrive_No { set; get; }

        [Display(Name = "有料道路使用")]
        public int Toll_Road { set; get; }

        [Required(ErrorMessage = "質量(kg)の入力が必要です。")]
        [Display(Name = "質量(kg)")]
        [Range(0, 2147483647, ErrorMessage = "質量(kg)は0または、正の数のみ入力可能です。")]
        public int Ship_Weight { set; get; }
        
        [Required(ErrorMessage = "質量(kg)の入力が必要です。")]
        [Display(Name = "質量(kg)")]
        [Range(0, 2147483647, ErrorMessage = "質量(kg)は0または、正の数のみ入力可能です。")]
        public int Take_Weight { set; get; }


        [Display(Name = "備考")]
        [MaxLength(200)]
        public string Remark { set; get; }


        public HaisouKeikakuShuuseiModel()
        {
            //Deli_Date = DateTime.Now;
            //Deli_Type_Name = "出荷";
            //Into_Name = "東山株式会社​";
            //Into_Address = "愛知県名古屋市緑区大高町字二番割23-1​";
            //Avalible_Car = "8tまで​";
            //Work_Time = 30;
            //Assign_Time_Flag = 0;
            //Assign_Start_Time = "10:30";
            //Assign_End_Time = "11:00";
            //Track_Cd = "000887";
            //Instruction_No = "F210800175";
            //Leave_Time = "5:00";
            //Arrive_No = 1;
            //Toll_Road = 0;
            //Ship_Weight = 100;
            //Take_Weight = 4000;
            //Remark = "コメント";
        }
    }

}
