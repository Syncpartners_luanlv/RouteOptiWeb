﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RouteOptiWeb.Common;
//using RouteOptiWeb.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; // [Required(ErrorMessage = 入力が必要です。")]
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using X.PagedList;

namespace RouteOptiWeb.Models
{
    public class AreaTorikomiModel
    {
        [Display(Name = "エリア番号")]
        public string Area_No { set; get; }
        [Display(Name = "エリア名")]
        public string Area_Name { set; get; }
        public IEnumerable<SelectListItem> Lst_Area_No { set; get; }//アリア番号リスト
        public IEnumerable<SelectListItem> Lst_Area_Name { set; get; }//アリア名リスト
        public IEnumerable<SelectListItem> Lst_ErrorMsg { set; get; }//エラーメッセージリスト
        public string Message { set; get; }
    }

}
