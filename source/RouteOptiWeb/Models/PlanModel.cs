﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace RouteOptiWeb.Models
{
    public class PlanModel
    {
        public string STATUS { get; set; }

        public string MESSAGE { get; set; }

        public string SEQID { get; set; }

        public string PROGRESS { get; set; }
    }
}