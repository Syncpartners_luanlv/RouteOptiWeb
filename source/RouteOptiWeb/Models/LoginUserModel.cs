using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace RouteOptiWeb.Models
{
    public class LoginUserModel
    {
        public int WID { get; set; }
        public string WName { get; set; }

        public string UserCode { get; set; }
        public string UserName { get; set; }

        public int Role { get; set; }

        public int DepoCode { get; set; }
        public string DepoName { get; set; }
    }
}