﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RouteOptiWeb.Models
{
    public class NonyusakiTorikomiModel
    {
        public string Into_Cd { get; set; }
        //（納入先コード)
        [MaxLength(1)]
        public string Into_Name { get; set; }
        //（納入先名）
        [MaxLength(1)]
        public string Address { get; set; }
        //（住所）
        [MaxLength(5)]
        public string Bin_No { get; set; }
        //便No
        [MaxLength(5)]
        public string Available_car { get; set; }
        //(可能車両)
        [MaxLength(2)]
        public string Time_FLG { get; set; }
        //（時間指定）
        [MaxLength(5)]
        public string Start_Time { get; set; }
        //(時間指定開始)
        public string End_Time { get; set; }
        //（時間指定終了）
        [MaxLength(4)]
        public string Work_time { get; set; }
        //（納入先作業時間(分)）
        public string Week { get; set; }
        //(曜日指定)
        [MaxLength(1)]
        public string Week_DB { get; set; }
        //発行日・年月日（伝票作成日時）
        public string Area_Name { get; set; }
        //(エリア)
        public string Area_No { get; set; }
        //
        public string Depo_FLG { get; set; }
        //("デポ)
    }
}
