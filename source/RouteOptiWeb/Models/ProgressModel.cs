﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;
using RouteOptiWeb.Common;

namespace RouteOptiWeb.Models
{
    // ！！！注意！！！シングルトン
    public class ProgressModel
    {
        private Dictionary<string, Dictionary<string, string>> _dicRecive = new Dictionary<string, Dictionary<string, string>>();
        private static ProgressModel _progressModel = null;

        public static ProgressModel GetInstance()
        {
            if (_progressModel == null)
            {
                _progressModel = new ProgressModel();
            }
            return _progressModel;
        }

        public string setData(string strSeqId, Dictionary<string, string> dicData)
        {
            //keyが存在しない場合は追加
            if (!_dicRecive.ContainsKey(strSeqId))
            {
                _dicRecive.Add(strSeqId, dicData);
                return Consts.C_JSON_OK;
            }

            //keyが存在する場合は上書き
            _dicRecive[strSeqId] = dicData;
            return Consts.C_JSON_OK;
        }

        public string getStatus(string strSeqId)
        {
            //keyが存在しない場合はErr
            if (!_dicRecive.ContainsKey(strSeqId))
            {
                return Consts.C_JSON_NG;
            }

            Dictionary<string, string> dicData = _dicRecive[strSeqId];

            //keyが存在しない場合はErr
            if (!dicData.ContainsKey(Consts.C_JSON_STATUS))
            {
                return Consts.C_JSON_NG;
            }
            return dicData[Consts.C_JSON_STATUS];
        }

        public string getMessage(string strSeqId)
        {
            //keyが存在しない場合はErr
            if (!_dicRecive.ContainsKey(strSeqId))
            {
                return Consts.C_JSON_NG;
            }

            Dictionary<string, string> dicData = _dicRecive[strSeqId];

            //keyが存在しない場合はErr
            if (!dicData.ContainsKey(Consts.C_JSON_MESSAGE))
            {
                return Consts.C_JSON_NG;
            }
            return dicData[Consts.C_JSON_MESSAGE];
        }

        public string getProgress(string strSeqId)
        {
            //keyが存在しない場合はErr
            if (!_dicRecive.ContainsKey(strSeqId))
            {
                return Consts.C_JSON_NG;
            }

            Dictionary<string, string> dicData = _dicRecive[strSeqId];

            //keyが存在しない場合はErr
            if (!dicData.ContainsKey(Consts.C_JSON_PROGRESS))
            {
                return Consts.C_JSON_NG;
            }
            return dicData[Consts.C_JSON_PROGRESS];
        }
    }
}