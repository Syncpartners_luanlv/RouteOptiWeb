using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace RouteOptiWeb.Models
{
    public class AccountModel
    {
        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            public int WID { get; set; }

            [DataType(DataType.Password)]
            public string WPassword { get; set; }

            public int DepoCode { get; set; }

            [Required(ErrorMessage = "ログインIDは入力必須項目です")]
            [RegularExpression(@"[a-zA-Z0-9]+", ErrorMessage = "ログインIDは半角英数字のみ入力できます")]
            public string UserCode { get; set; }

            [Required(ErrorMessage = "パスワードは入力必須項目です")]
            [RegularExpression(@"[a-zA-Z0-9]+", ErrorMessage = "パスワードは半角英数字のみ入力できます")]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }

            public string CompanyName { get; set; }

            public string SecurityStamp { get; set; }

        }

    }
}
