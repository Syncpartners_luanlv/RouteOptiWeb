﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RouteOptiWeb.Common;
//using RouteOptiWeb.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; // [Required(ErrorMessage = 入力が必要です。")]
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using X.PagedList;

namespace RouteOptiWeb.Models
{
    public class DeliCar
    {
        public DateTime deli_date { set; get; }
        public string track_cd { set; get; } 
        public string driver_name { set; get; }
        public string car_type { set; get; }
        public string max_weight { set; get; }
        public string max_available_weight { set; get; }
        public string speed_factor { set; get; }
    }
    public class DeliPlan
    {
        public DateTime deli_date { set; get; } //1
        public string track_cd { set; get; } //3
        //public string driver_name { set; get; } //4 [t_deli_car].driver_name
        public int arrive_no{ set; get; } //11
        public string instruction_no{ set; get; } //12
        public int deli_type_no{ set; get; } //13
        public string into_name { set; get; } //14
        public string into_address { set; get; } //15
        public string arrive_time{ set; get; } //16
        public string leave_time { set; get; } //17
        public int work_time { set; get; } //18
        public int ship_weight { set; get; } //19
        public int take_weight { set; get; } //20
        public int available_weight { set; get; } //21
        public int take_plan_weight { set; get; } //68
        public string available_car { set; get; } //22
        public string assign_start_time { set; get; } //23
        public string assign_end_time { set; get; } //24
        public string area_name { set; get; } //25
        public int deli_no { set; get; } //26
        public int toll_road { set; get; } //27
        public string remark { set; get; } //28
    }
    public class HaisouIchiranModel
    {
        //1
        [Required(ErrorMessage = "配送日付が必要です。")]
        [Display(Name = "配送日付")]
        public DateTime txt_date { set; get; }

        //3
        [Display(Name = "トラックコード")]
        public IEnumerable<SelectListItem> drop_top_track_code_lst { set; get; }    //List box 配送計画情報のトラックコード
        public string drop_top_track_code{ set; get; }  // 配送計画情報のトラックコード

        //4
        [Required(ErrorMessage = "運転手名が必要です。")]
        [Display(Name = "運転手名")]
        public string txt_top_driver_name { set; get; } //車両情報.運転手名(トラックコードが未割付選択時は非表示)


        //5
        [Display(Name = "車格")]
        public string lbl_top_car_type{ set; get; } //車両情報.車格(トラックコードが未割付選択時は非表示)

        //6
        [Display(Name = "最大積載可能質量")]
        public int lbl_top_max_available_weight { set; get; }   //車両情報.最大積載可能質量(トラックコードが未割付選択時は非表示)

        [Display(Name = "住所")]
        public string Adress { set; get; }

        [Display(Name = "可能車両")]
        public string Avalible_car { set; get; }

        [Required(ErrorMessage = "納入先作業時間(分)の入力が必要です。")]
        [Display(Name = "納入先作業時間(分)")]
        public int Work_Time { set; get; }

        [Required(ErrorMessage = "時間指定開始の入力が必要です。")]
        [Display(Name = "時間指定開始")]
        [MaxLength(5)]
        public string Assign_Start_Time { get; set; }

        [Required(ErrorMessage = "時間指定終了の入力が必要です。")]
        [Display(Name = "時間指定終了")]
        [MaxLength(5)]
        public string Assign_End_Time { get; set; }

        [Required(ErrorMessage = "トラックコードの入力が必要です。")]
        [Display(Name = "トラックコード")]
        public IEnumerable<SelectListItem> Lst_Track_Cd { set; get; } //List box トラックコードを取得       

        [Required(ErrorMessage = "指示Noの入力が必要です。")]
        [Display(Name = "指示No")]
        [MaxLength(20)]
        public string Instruction_No { set; get; }


        [Required(ErrorMessage = "出発時間の入力が必要です。")]
        [Display(Name = "出発時間")]
        [MaxLength(5)]
        public string Leave_Time { set; get; }

        [Required(ErrorMessage = "到着順の入力が必要です。")]
        [Display(Name = "到着順")]
        [MaxLength(5)]
        public string Arrive_Time { set; get; }

        [Display(Name = "有料道路使用")]
        public int Toll_Road { set; get; }

        [Required(ErrorMessage = "質量(kg)の入力が必要です。")]
        [Display(Name = "質量(kg)")]
        public int Shitsuryo { set; get; }


        [Display(Name = "備考")]
        [MaxLength(200)]
        public string Remark { set; get; }

        [Display(Name = "時間指定")]
        public Boolean Time_FLG { set; get; }

        //[Display(Name = "検索")]
        //public string NonyusakiSearch { get; set; }
    }

}
