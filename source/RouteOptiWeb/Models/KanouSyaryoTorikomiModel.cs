﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RouteOptiWeb.Common;
//using RouteOptiWeb.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; // [Required(ErrorMessage = 入力が必要です。")]
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using X.PagedList;

namespace RouteOptiWeb.Models
{
    public class KanouSyaryoTorikomiModel
    {
        [Display(Name = "可能車両")]
        public string Available_Car { set; get; }
        [Display(Name = "車格")]
        public string Car_Type { set; get; }
        [Display(Name = "割付係数")]
        public string Assign_Factor { set; get; }
        public IEnumerable<SelectListItem> Lst_Available_Car { set; get; }//可能車両リスト
        public IEnumerable<SelectListItem> Lst_Car_Type { set; get; }//車格リスト
        public IEnumerable<SelectListItem> Lst_Assign_Factor { set; get; }//割付係数リスト
        public IEnumerable<SelectListItem> Lst_ErrorMsg { set; get; }//エラーメッセージリスト        
    }

}
