﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RouteOptiWeb.Common;
//using RouteOptiWeb.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; // [Required(ErrorMessage = 入力が必要です。")]
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using X.PagedList;

namespace RouteOptiWeb.Models
{
    public class NonyusakiTourokuModel
    {
        [Required(ErrorMessage = "納入先コードが必要です。")]
        [Display(Name = "納入先コード")]
        [RegularExpression("(.{1,10}$)", ErrorMessage = "納入先コードは10文字まで入力可能です。")]
        public string Into_Cd { set; get; }

        [Required(ErrorMessage = "納入先名の入力が必要です。")]
        [Display(Name = "納入先名")]
        [RegularExpression("(.{1,50}$)", ErrorMessage = "納入先名は50文字まで入力可能です。")]
        public string Into_Name { set; get; }

        [Required(ErrorMessage = "住所の入力が必要です。")]
        [Display(Name = "住所")]
        [RegularExpression("(.{1,200}$)", ErrorMessage = "住所名は200文字まで入力可能です。")]
        public string Address { set; get; }

        [Required(ErrorMessage = "便Noの入力が必要です。")]
        [Display(Name = "便No")]
        [RegularExpression("(.{1,2}$)", ErrorMessage = "便Noは2文字まで入力可能です。")]
        public string Bin_No { set; get; }

        [Display(Name = "可能車両")]
        public string Available_car { set; get; }

        [Display(Name = "時間指定")]
        public Boolean Time_FLG { set; get; }

        [Display(Name = "時間指定開始")]
        public string Start_Time { get; set; }

        [Display(Name = "時間指定終了")]
        public string End_Time { get; set; }

        [Required(ErrorMessage = "納入先作業時間(分)の入力が必要です。")]
        [Display(Name = "納入先作業時間(分)")]
        public string Work_time { set; get; }

        [Display(Name = "曜日指定")]
        public string Week { set; get; }

        [Display(Name = "修正前の曜日指定の値")]//納入先編集画面を使用
        public string Week_DB { set; get; }

        [Display(Name = "エリア")]
        public string Area_Name { set; get; }

        public int Area_No { set; get; }//DBの読み書きに使用

        [Display(Name = "デポ")]
        public Boolean Depo_FLG { set; get; }

        public IPagedList<NonyusakiTourokuModel> NonyusakiList { set; get; }//一覧画面のlistを取得と設定

        [Display(Name = "検索")]
        public string NonyusakiSearch { get; set; }

        public IEnumerable<SelectListItem> Lst_Available_car { set; get; }//List box 可能車両の名称を取得


        public IEnumerable<SelectListItem> Lst_Area_name { set; get; }//List box areaの名称を取得

    }

    public class NonyusakiEditModel
    {
        [Display(Name = "納入先コード")]
        public string Into_Cd { set; get; }

        [Required(ErrorMessage = "納入先名の入力が必要。")]
        [Display(Name = "納入先名")]
        [RegularExpression("(.{1,50}$)", ErrorMessage = "納入先名の文字数が多すぎます。")]
        public string Into_Name { set; get; }

        [Required(ErrorMessage = "住所の入力が必要。")]
        [Display(Name = "住所")]
        [RegularExpression("(.{1,200}$)", ErrorMessage = "住所名の文字数が多すぎます。")]
        public string Address { set; get; }

        [Display(Name = "便No")]
        public string Bin_No { set; get; }

        [Display(Name = "可能車両")]
        public string Available_car { set; get; }

        [Display(Name = "時間指定")]
        public Boolean Time_FLG { set; get; }

        [Display(Name = "時間指定開始")]
        public string Start_Time { get; set; }

        //[Required(ErrorMessage = "時間指定終了が必要。")]
        [Display(Name = "時間指定終了")]
        public string End_Time { get; set; }

        [Required(ErrorMessage = "納入先作業時間(分)を入力する必要です。")]
        [Display(Name = "納入先作業時間(分)")]
        public string Work_time { set; get; }

        [Required(ErrorMessage = "曜日指定を入力する必要です。")]
        [Display(Name = "曜日指定")]
        public string Week { set; get; }

        [Required(ErrorMessage = "エリアを入力する必要です。")]
        [Display(Name = "エリア")]
        public int Area_No { set; get; }

        [Display(Name = "エリア_Name")]
        public string Area_Name { set; get; }

        [Display(Name = "デポ")]
        public Boolean Depo_FLG { set; get; }
    }
}
