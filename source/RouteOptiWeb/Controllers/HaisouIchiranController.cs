﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RouteOptiWeb.Common;
using RouteOptiWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RouteOptiWeb.Controllers
{
    public class HaisouIchiranController : Controller
    {
        public IActionResult Index(HaisouIchiranModel model)
        {

            //get 配送日付
            model.txt_date = GetMaxDeliDate();
            return View(model);
        }

        [HttpGet]
        public JsonResult GetDeliPlans(string deliDate)
        {
            //yyyy-MM-dd
            var date = DateTime.Parse(deliDate);
            var conn = new SqlConnection(new GetConnectString().ConnectionString);

            var lst = new List<DeliPlan>();
            {
                try
                {
                    conn.Open();
                    //SQLの準備
                    //string commandText = $@"SELECT * FROM (SELECT * FROM t_deli_plan WHERE deli_date=@deli_date) as t1 "
                    //    + "JOIN t_deli_car ON t1.deli_date = t_deli_car.deli_date AND t1.track_cd = t_deli_car.track_cd";
                    string commandText = $@"SELECT * FROM t_deli_plan WHERE deli_date=@deli_date";

                    var cmd = new SqlCommand(commandText, conn);
                    cmd.Parameters.Add(new SqlParameter("@deli_date", date));
                    var rd = cmd.ExecuteReader();

                    while (rd.Read())
                    {
                        var model = new DeliPlan();
                        BindDataToModel(model, rd);
                        lst.Add(model);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return JsonOK(lst);
        }

        [HttpGet]
        public JsonResult GetDeliCars(string deliDate)
        {
            //yyyy-MM-dd
            var date = DateTime.Parse(deliDate);
            var conn = new SqlConnection(new GetConnectString().ConnectionString);

            var lst = new List<DeliCar>();
            {
                try
                {
                    conn.Open();
                    string commandText = $@"SELECT * FROM t_deli_car WHERE deli_date=@deli_date";

                    var cmd = new SqlCommand(commandText, conn);
                    cmd.Parameters.Add(new SqlParameter("@deli_date", date));
                    var rd = cmd.ExecuteReader();

                    while (rd.Read())
                    {
                        var model = new DeliCar();
                        BindDataToModel(model, rd);
                        lst.Add(model);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return JsonOK(lst);
        }

        //
        public JsonResult JsonNG(string message)
        {
            return Json(new { status = false, message });
        }
        public JsonResult JsonOK(object data)
        {
            return Json(new { status = true, data });
        }

        private void BindDataToModel<T>(T model, SqlDataReader rd)
        {
            // Get the public properties.
            PropertyInfo[] propInfos = model.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var propInfo in propInfos)
            {
                string key = propInfo.Name.ToLower();
                propInfo.SetValue(model, rd[key]);
            }
        }
        public DateTime GetMaxDeliDate()
        {
            var maxDeliDate = DateTime.Now;
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();

                    //SQLの準備
                    string commandText = $@"SELECT max(deli_date) FROM t_deli_plan";

                    maxDeliDate = Convert.ToDateTime(database.ExecuteScalar(commandText));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return maxDeliDate;
        }
    }
}
