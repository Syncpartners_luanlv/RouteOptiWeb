﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RouteOptiWeb.Common;
using RouteOptiWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace RouteOptiWeb.Controllers
{
    public class HaisouKeikakuShuuseiController : Controller
    {
        [HttpGet]
        public IActionResult Index(string deliDate, string trackCd, string arriveNo, string instructionNo)
        {
            //deliDate = "2022-01-14";
            //trackCd = "000887";
            //arriveNo = "1";
            //instructionNo = "B210705052";
            //var model = GetDeliPlan(deliDate,trackCd, Convert.ToInt32(arriveNo),instructionNo);
            var model = GetDeliPlan();
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(HaisouKeikakuShuuseiModel model)
        {
            if (model == null)
            {
                model = new HaisouKeikakuShuuseiModel();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> Save()
        {
            try
            {
                var model = await JsonSerializer.DeserializeAsync<HaisouKeikakuShuuseiModel>(Request.Body);
                
                //check input
                if (model.Assign_Time_Flag != 0)
                {
                    var startTime = ConvertTimeToMinutes(model.Assign_Start_Time);
                    var endTime = ConvertTimeToMinutes(model.Assign_End_Time);
                    if (startTime >= endTime)
                    {
                        return JsonNG("時間指定終了が先になっています。");
                    }
                }
                else
                {
                    model.Assign_Start_Time = "---";
                    model.Assign_End_Time = "---";
                }

                //[取引　or 引取納め（引取）]選ぶなら ⑲の質量の値を保存。以外なら　値は 0を保存
                switch(model.Deli_Type_Name)
                {
                    case "出荷":
                    case "引取納め（納め）":
                        break;
                    default:
                        model.Take_Weight = 0;
                        model.Ship_Weight = 0;
                        break;
                }

                var n = UpdateDeliPlan2(model);
                if (n == 0)
                {
                    return JsonNG("配送計画無し。");
                }

                return JsonOK(model);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return JsonNG("接続異常のためネットワーク接続を確認してください。");
            }
        }

        [HttpPost]
        public async Task<JsonResult> Delete()
        {
            try
            {
                var model = await JsonSerializer.DeserializeAsync<HaisouKeikakuShuuseiModel>(Request.Body);
                
                int ret = DeleteDeliPlan(model.Deli_Date, model.Track_Cd, model.Arrive_No, model.Instruction_No);
                if (ret == 0)
                {
                    return JsonNG("配送計画無し。");
                }

                return JsonOK(model);
            }
            catch
            {
                return JsonNG("接続異常のためネットワーク接続を確認してください。");
            }
        }

        public IActionResult Delete(string deliDate, string trackCd, string arriveNo, string instructionNo)
        {
            try
            {
                int ret = DeleteDeliPlan(Convert.ToDateTime(deliDate), trackCd, Convert.ToInt32(arriveNo), instructionNo);
            }
            catch
            {
            }
            return RedirectToAction("Index");
        }

        public JsonResult JsonNG(string message)
        {
            return Json(new { status = false, message });
        }
        public JsonResult JsonOK(object data)
        {
            return Json(new { status = true, data });
        }

        public int DeleteDeliPlan(DateTime deliDate, string trackCd, int arriveNo, string instructionNo)
        {
            var conn = new SqlConnection(new GetConnectString().ConnectionString);
            int n;
            try
            {
                conn.Open();

                //SQLの準備
                string[] cnds = {
                    "(deli_date = @deli_date_old)",
                    "(track_cd = @track_cd_old)",
                    "(arrive_no = @arrive_no_old)",
                    "(instruction_no = @instruction_no_old)"
                };

                string commandText = string.Format(
                    "DELETE FROM  [t_deli_plan] WHERE {0}",
                    string.Join(" AND ", cnds)
                    );

                var cmd = new SqlCommand(commandText, conn);
                cmd.Parameters.Add(new SqlParameter("@deli_date_old", deliDate));
                cmd.Parameters.Add(new SqlParameter("@track_cd_old", trackCd));
                cmd.Parameters.Add(new SqlParameter("@arrive_no_old", arriveNo));
                cmd.Parameters.Add(new SqlParameter("@instruction_no_old", instructionNo));

                n = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new Exception("DeleteDeliPlanエラー");
            }
            conn.Close();
            return n;
        }
        public HaisouKeikakuShuuseiModel GetDeliPlan(string deliDate, string trackCd, int arriveNo, string instructionNo)
        {
            HaisouKeikakuShuuseiModel model = new HaisouKeikakuShuuseiModel();
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    string commandText = $@"SELECT * FROM [t_deli_plan]" +
                        " WHERE(deli_date = @deli_date)" +
                        " AND (track_cd = @track_cd)" +
                        " AND (arrive_no = @arrive_no)" +
                        " AND (instruction_no = @instruction_no)";

                    database.ClearParam();
                    database.SetParam("@deli_date", SqlDbType.Date, DateTime.Parse(deliDate));
                    database.SetParam("@track_cd", SqlDbType.NVarChar, trackCd);
                    database.SetParam("@arrive_no", SqlDbType.Int, arriveNo);
                    database.SetParam("@instruction_no", SqlDbType.NVarChar, instructionNo);

                    var ret = database.ExecuteReaderSetSQL(commandText);
                    var dict = database.ExecuteReader();
                    database.ExecuteReaderClose();

                    BindDataToModel(model, dict);

                    model.Deli_Type_Name = GetDeliTypeNameByNo(model.Deli_Type_No);
                    model.Assign_Time_Flag = model.Assign_Start_Time == "---" ? 0 : 1;
                    return model;
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw new Exception("GetDeliPlanエラー");
                }
            }
        }

        public HaisouKeikakuShuuseiModel GetDeliPlan()
        {
            HaisouKeikakuShuuseiModel model = new HaisouKeikakuShuuseiModel();
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    string commandText = $@"SELECT TOP(1) * FROM [t_deli_plan]";

                    var ret = database.ExecuteReaderSetSQL(commandText);
                    var dict = database.ExecuteReader();
                    database.ExecuteReaderClose();

                    BindDataToModel(model, dict);

                    model.Deli_Type_Name = GetDeliTypeNameByNo(model.Deli_Type_No);
                    model.Assign_Time_Flag = ((model.Assign_Start_Time == "---") || (model.Assign_End_Time == "---")) ? 0 : 1;
                    if (model.Assign_Time_Flag == 0)
                    {
                        model.Assign_Start_Time = "";
                        model.Assign_End_Time = "";
                    }
                    return model;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw new Exception("GetDeliPlanエラー");
                }
            }
        }

        private void BindDataToModel(HaisouKeikakuShuuseiModel model, Dictionary<string, object> dict)
        {
            Type t = model.GetType();
            // Get the public properties.
            PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach(var propInfo in propInfos)
            {
                string key = propInfo.Name.ToLower();
                if(dict.ContainsKey(key))
                {
                    propInfo.SetValue(model, dict[key]);
                }
            }
        }

        private string GetDeliTypeNameByNo(int deliTypeNo)
        {
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    string commandText = $@" SELECT deli_division_name FROM [m_deli_division]" +
                        " WHERE(deli_division_no = @deli_division_no)";

                    database.ClearParam();
                    database.SetParam("@deli_division_no", SqlDbType.Int, deliTypeNo);

                    return Convert.ToString(database.ExecuteScalar(commandText));
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw new Exception("GetDeliTypeNameByNoエラー");
                }
            }
        }

        public int UpdateDeliPlan(string deliDate, string trackCd, string arriveNo, string instructionNo, Dictionary<string, object> changes)
        {
            var conn = new SqlConnection(new GetConnectString().ConnectionString);
            int n;
            try
            {
                conn.Open();

                //SQLの準備
                string[] cnds = {
                    "(deli_date = @deli_date_old)",
                    "(track_cd = @track_cd_old)",
                    "(arrive_no = @arrive_no_old)",
                    "(instruction_no = @instruction_no_old)"
                };

                var exprs = new List<string>();
                foreach (string key in changes.Keys)
                {
                    exprs.Add(string.Format("{0}=@{0}", key));
                }
                string commandText = string.Format(
                    "UPDATE  [t_deli_plan] SET {0} WHERE {1}",
                    string.Join(", ", exprs),
                    string.Join(" AND ", cnds)
                    );

                var cmd = new SqlCommand(commandText, conn);
                cmd.Parameters.Add(new SqlParameter("@deli_date_old", Convert.ToDateTime(deliDate)));
                cmd.Parameters.Add(new SqlParameter("@track_cd_old", trackCd));
                cmd.Parameters.Add(new SqlParameter("@arrive_no_old", arriveNo));
                cmd.Parameters.Add(new SqlParameter("@instruction_no_old", instructionNo));

                n = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new Exception("");
            }
            conn.Close();
            return n;
        }
        public int UpdateDeliPlan2(HaisouKeikakuShuuseiModel model)
        {
            var conn = new SqlConnection(new GetConnectString().ConnectionString);
            int n;
            {
                try
                {
                    conn.Open();
                    //SQLの準備
                    string commandText = $@" UPDATE  [t_deli_plan] SET work_time = @work_time," +
                        " leave_time=@leave_time, " +
                        " ship_weight=@ship_weight, " +
                        " take_weight=@take_weight, " +
                        " assign_start_time=@assign_start_time, " +
                        " assign_end_time=@assign_end_time, " +
                        " area_name='手修正', " +
                        " toll_road=@toll_road, " +
                        " remark=@remark " +
                        " WHERE (deli_date = @deli_date_old)" +
                        " AND (track_cd = @track_cd_old)" +
                        " AND (arrive_no = @arrive_no_old)" +
                        " AND (instruction_no = @instruction_no_old)"
                        ;

                    //パラメーターをクリアー
                    var cmd = new SqlCommand(commandText,conn);
                    cmd.Parameters.Add(new SqlParameter("@work_time", model.Work_Time));
                    cmd.Parameters.Add(new SqlParameter("@leave_time", model.Leave_Time));
                    cmd.Parameters.Add(new SqlParameter("@ship_weight", model.Ship_Weight));
                    cmd.Parameters.Add(new SqlParameter("@take_weight", model.Take_Weight));
                    cmd.Parameters.Add(new SqlParameter("@assign_start_time", model.Assign_Start_Time));
                    cmd.Parameters.Add(new SqlParameter("@assign_end_time", model.Assign_End_Time));
                    cmd.Parameters.Add(new SqlParameter("@toll_road", model.Toll_Road));
                    cmd.Parameters.Add(new SqlParameter("@remark", model.Remark));

                    cmd.Parameters.Add(new SqlParameter("@deli_date_old", model.Deli_Date));
                    cmd.Parameters.Add(new SqlParameter("@track_cd_old", model.Track_Cd));
                    cmd.Parameters.Add(new SqlParameter("@arrive_no_old", model.Arrive_No));
                    cmd.Parameters.Add(new SqlParameter("@instruction_no_old", model.Instruction_No));

                    n = cmd.ExecuteNonQuery();
                    conn.Close();
                    return n;
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    return 0;
                }
            }
        }
        public int ConvertTimeToMinutes(string time)
        {
            var arr = time.Split(new char[] { ':' });
            return Convert.ToInt32(arr[0]) * 60 + Convert.ToInt32(arr[1]);
        }
    }
}
