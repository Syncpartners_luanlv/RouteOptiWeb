﻿using Microsoft.AspNetCore.Mvc;
using RouteOptiWeb.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RouteOptiWeb.Controllers
{
    public class KanouSyaryouController : Controller
    {
        private readonly string ErrorMsg = "接続異常のためネットワーク接続を確認してください。";
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ExcelButton()
        {
            DataTable dt = GetAvailableCarData();

            //会社ID取得
            int wid = int.Parse(User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).First().Value);
            //ユーザー名取得
            string userCode = User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Skip(2).First().Value.ToString();

            // ファイル名
            var tmpFilename = "可能車両車格取込_" + wid + "_" + userCode + DateTime.Now.ToString("yyyyMMddHHmmss");

            // 出力フォルダパス
            var rootPath = Directory.GetCurrentDirectory();
            var folderPath = Path.Combine(rootPath, @"wwwroot\DownloadFiles\Kanousyaryou");

            //Webサーバーのダウンロードのフォルダーが存在しないる場合に新規作成
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            // 出力パス
            var expPath = Path.Combine(folderPath, tmpFilename);

            //ヘッダーのリストを作成
            List<string> hederList = new List<string>();
            hederList.Add("可能車両");
            hederList.Add("車格");
            hederList.Add("割付係数");

            // Excelファイル生成
            bool exportRs = Util.ExportExcel(dt, expPath, hederList, null, "", "可能車両車格取込");
            if (exportRs)
            {
                var file = System.IO.File.ReadAllBytes(expPath);
                return File(file, System.Net.Mime.MediaTypeNames.Application.Octet, "可能車両車格取込.xlsx");
            }
            else
            {
                ViewData["Error"] = ErrorMsg;
                return View("Index");
            }
        }

        private DataTable GetAvailableCarData()
        {
            DataTable data;
            using (MSSQLAccess database = new MSSQLAccess())
            {
                //パラメーターをクリアー
                database.ClearParam();

                //---------------------------------------------------------------------------
                string sqlstr = @$" SELECT * FROM  [m_available_car] ";
                data = database.SelectData(sqlstr);
            }
            return data;
        }
    }
}
