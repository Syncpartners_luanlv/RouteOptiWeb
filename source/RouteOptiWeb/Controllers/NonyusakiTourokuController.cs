﻿using Microsoft.AspNetCore.Mvc;
using RouteOptiWeb.Models;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace RouteOptiWeb.Controllers
{
    public class NonyusakiTourokuController : Controller
    {
        public IActionResult Index()
        {
            NonyusakiTourokuModel nonyusakiTourokuModel = new NonyusakiTourokuModel();

            nonyusakiTourokuModel.Lst_Available_car = GetAvailable_List();
            nonyusakiTourokuModel.Lst_Area_name = GetAreaname_List();
            return View(nonyusakiTourokuModel);
        }

        public IEnumerable<SelectListItem> GetAvailable_List()
        {
            //nonyusakiTourokuModel.Lst_Available_car.Clear();
            var available_List = new List<SelectListItem>();
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    database.SetTransaction();

                    //SQLの準備
                    string commandText = $@" SELECT DISTINCT [available_car] as Available FROM [m_available_car] order by Available";
                    //string commandText = $@"
                    //SELECT DISTINCT [avaliable_car] as Avaliable,
                    //(case when UNICODE(substring([avaliable_car],1,1)) > 100 then 9999 else UNICODE(substring([avaliable_car],1,1)) end  
                    //+ case when UNICODE(substring([avaliable_car],2,1)) > 100 then 0 else UNICODE(substring([avaliable_car],2,1)) end ) FROM [m_avalible_car] order by
                    //(case when UNICODE(substring([avaliable_car],1,1)) > 100 then 9999 else UNICODE(substring([avaliable_car],1,1)) end  
                    //+ case when UNICODE(substring([avaliable_car],2,1)) > 100 then 0 else UNICODE(substring([avaliable_car],2,1)) end ),[avaliable_car]";

                    var selectData = database.SelectData(commandText);

                    for (int i = 0; i < selectData.Rows.Count; i++)
                    {
                        var num1 = selectData.Rows[i]["Available"].ToString();
                        var item = new SelectListItem { Text = num1 };
                        available_List.Add(item);
                    }

                    //string userData = selectData.Rows[0]["Avaliable"];
                    //return Avaliable_no;
                }
                catch
                {
                    ViewData["Error"] = "接続異常のためネットワーク接続を確認してください。";
                    return available_List;
                }
            }
            return available_List;
        }

        public IEnumerable<SelectListItem> GetAreaname_List()
        {
            //nonyusakiTourokuModel.Lst_Area_name .Clear();
            var areaname_List = new List<SelectListItem>();
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    database.SetTransaction();

                    //SQLの準備
                    string commandText = $@" SELECT DISTINCT [area_name] as Area FROM [m_area] order by Area";

                    var selectData = database.SelectData(commandText);

                    for (int i = 0; i < selectData.Rows.Count; i++)
                    {
                        var num1 = selectData.Rows[i]["Area"].ToString();
                        var item = new SelectListItem { Text = num1 };
                        areaname_List.Add(item);
                    }
                }
                catch
                {
                    ViewData["Error"] = "接続異常のためネットワーク接続を確認してください。";
                    return areaname_List;
                }
            }
            return areaname_List;
        }

        [HttpPost]
        public ActionResult Create(NonyusakiTourokuModel value)
        {
            value.Lst_Available_car = GetAvailable_List();
            value.Lst_Area_name = GetAreaname_List();
            using (MSSQLAccess database = new MSSQLAccess())
            {
                int depo_flg = 0;
                int time_flg = 0;
                depo_flg = (value.Depo_FLG == true) ? 1 : 0;
                time_flg = (value.Time_FLG == true) ? 1 : 0;

                //check depo_flg----check week---check bin_no--check into_cd===============================
                string sql_into = $@"SELECT (into_cd) AS value_into
                                       FROM [m_into]";
                var select_into = database.SelectData(sql_into);

                string sql_depo = $@"SELECT (depo_flg) AS value_depo
                                       FROM [m_into]";
                var select_depo = database.SelectData(sql_depo);

                string sql_week = $@"SELECT (week) AS value_week
                                       FROM [m_into]";
                var select_week = database.SelectData(sql_week);

                string sql_bin = $@"SELECT (bin_no) AS value_bin
                                       FROM [m_into]";
                var select_bin = database.SelectData(sql_bin);
                //=========================================================================================

                for (int j = 0; j < select_into.Rows.Count; j++)
                {
                    if (depo_flg == 1)
                    {
                        if ((int)select_depo.Rows[j]["value_depo"] == 1)
                        {
                            ViewData["Error"] = "既にデポが登録済みのため、登録出来ません。";
                            return View("Index", value);
                        }
                    }
                }

                /// 納入先コード,便No,曜日指定,デポの値をチェックします。重複ならエラーにする==========================
                for (int i = 0; i < select_into.Rows.Count; i++)
                {
                    var num1 = select_into.Rows[i]["value_into"].ToString();

                    /// 納入先コードが重複時============
                    if (value.Into_Cd == num1)
                    {
                        if ((int)select_depo.Rows[i]["value_depo"] == 1)
                        {
                            ViewData["Error"] = "納入先コードがデポで登録済みのため、登録出来ません。";
                            return View("Index", value);
                        }
                        else
                        {
                            if (depo_flg == 1)
                            {
                                ViewData["Error"] = "納入先コードが登録済のためデポの登録は出来ません。";
                                return View("Index", value);
                            }
                        }
                        var num2 = select_bin.Rows[i]["value_bin"].ToString();

                        /// 便Noが重複時==============
                        if (value.Bin_No == num2)
                        {
                            int num3 = Convert.ToInt32(int.Parse(select_week.Rows[i]["value_week"].ToString()).ToString(), 2);//値の型を2進数から10進数に変換します
                            int num4 = Convert.ToInt32((int.Parse(value.Week)).ToString(), 2);//値の型を2進数から10進数に変換します

                            //曜日指定が重複時==========
                            if ((num3 & num4) != 0)
                            {
                                ViewData["Error"] = "納入先コードと便(No)と曜日の重複があるので登録出来ません。";
                                return View("Index", value);
                            }
                        }
                    }
                }

                //曜日全てチェックなしならエラーにする
                if (value.Week == "0000000")
                {
                    ViewData["Error"] = "いづれかの曜日をチェックしてください。";
                    return View("Index", value);
                }
                if (int.TryParse(value.Work_time, out int WorkTime) == true)
                {
                    if (WorkTime < 0 || WorkTime > 120)
                    {
                        ViewData["Error"] = "納入先作業時間(分)は0から120の間で入力してください。";
                        return View("Index", value);
                    }
                }
                else
                {
                    ViewData["Error"] = "納入先作業時間(分)は小数点で入力不可です。";
                    return View("Index", value);
                }

                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    //トランザクションが成功ならべてのデータ変更がデータベースに保存されます。エラーがあるならデータはトランザクションが実行される前の状態に復元されます！
                    database.SetTransaction();

                    if (depo_flg == 1)
                    {
                        //パラメータ必須[m_into]============================================
                        database.SetParam("@Into_Cd", SqlDbType.NVarChar, value.Into_Cd);//納入先コード
                        database.SetParam("@Into_Name", SqlDbType.NVarChar, value.Into_Name);//納入先名
                        database.SetParam("@Address", SqlDbType.NVarChar, value.Address);//住所
                        database.SetParam("@Bin_No", SqlDbType.NVarChar, "0");//便No
                        database.SetParam("@Work_time", SqlDbType.Int, WorkTime);//納入先作業時間(分)
                        database.SetParam("@Week", SqlDbType.NVarChar, "1111111");//曜日指定
                        database.SetParam("@Depo_FLG", SqlDbType.Int, depo_flg);//デポ
                        //=================================================================
                    }
                    else
                    {

                        //パラメータ必須[m_into]==============================================
                        database.SetParam("@Into_Cd", SqlDbType.NVarChar, value.Into_Cd);//納入先コード
                        database.SetParam("@Into_Name", SqlDbType.NVarChar, value.Into_Name);//納入先名
                        database.SetParam("@Address", SqlDbType.NVarChar, value.Address);//住所
                        database.SetParam("@Bin_No", SqlDbType.NVarChar, value.Bin_No);//便No
                        database.SetParam("@Available_car", SqlDbType.NVarChar, value.Available_car);//可能車両
                        database.SetParam("@Time_FLG", SqlDbType.Int, time_flg);//時間指定
                        if (time_flg == 0)
                        {
                            database.SetParam("@Start_Time", SqlDbType.NVarChar, "08:00");//時間指定開始
                            database.SetParam("@End_Time", SqlDbType.NVarChar, "09:00");//時間指定終了
                        }
                        else
                        {
                            database.SetParam("@Start_Time", SqlDbType.NVarChar, value.Start_Time);//時間指定開始
                            database.SetParam("@End_Time", SqlDbType.NVarChar, value.End_Time);//時間指定終了
                        }
                        database.SetParam("@Work_time", SqlDbType.Int, WorkTime);//納入先作業時間(分)
                        database.SetParam("@Week", SqlDbType.NVarChar, value.Week);//曜日指定
                        database.SetParam("@Area_Name", SqlDbType.NVarChar, value.Area_Name);//エリア
                        database.SetParam("@Depo_FLG", SqlDbType.Int, depo_flg);//デポ
                        //[m_area]Tableでは(area_no)値を見ます================================
                        string sql_area = $@"SELECT (area_no) AS value_area
                                       FROM [m_area]
                                       WHERE (1 = 1) 
                                       AND(area_name = @Area_Name) ";
                        var select_area = database.SelectData(sql_area);
                        int value_area = Convert.ToInt32(select_area.Rows[0]["value_area"]);
                        database.SetParam("@Area_No", SqlDbType.Int, value_area);//エリア
                        //=================================================================
                    }

                    //[m_address]Tableでは(address_no)値を見ます===============================
                    string sql = $@"SELECT Count(address_no) AS value_address
                                       FROM [m_address]
                                       WHERE (1 = 1) 
                                       AND(address = @Address) ";
                    var selectData = database.SelectData(sql);
                    int value_address = Convert.ToInt32(selectData.Rows[0]["value_address"]);
                    //=====================================================================

                    if (value_address != 0)
                    {
                        string sql1 = $@"SELECT address_no
                                     FROM [m_address]
                                     WHERE (1 = 1) 
                                     AND(address = @Address) ";
                        var selectData1 = database.SelectData(sql1);

                        int Address_no = Convert.ToInt32(selectData1.Rows[0]["address_no"]);
                        database.SetParam("@Address_No", SqlDbType.Int, Address_no);//エリア
                        if (depo_flg == 1)
                        {
                            string sql2 = $@"INSERT INTO [m_into] (
                                        into_cd, 
                                        into_name,
                                        address_no,
                                        bin_no,
                                        work_time,
                                        week,
                                        depo_flg)
                                VALUES (
                                        @Into_Cd, 
                                        @Into_Name,
                                        @Address_No,
                                        @Bin_No,
                                        @Work_time,
                                        @Week,
                                        @Depo_FLG)";
                            bool result = database.InsertData(sql2);
                        }
                        else
                        {
                            string sql2 = $@"INSERT INTO [m_into] (
                                        into_cd, 
                                        into_name,
                                        address_no,
                                        bin_no,
                                        available_car, 
                                        time_flg, 
                                        start_time,
                                        end_time, 
                                        work_time,
                                        week,
                                        area_no,
                                        depo_flg)
                                VALUES (
                                        @Into_Cd, 
                                        @Into_Name,
                                        @Address_No,
                                        @Bin_No,
                                        @Available_car,
                                        @Time_FLG,
                                        @Start_Time,
                                        @End_Time,
                                        @Work_time,
                                        @Week,
                                        @Area_No,
                                        @Depo_FLG)";
                            bool result = database.InsertData(sql2);
                        }

                    }
                    else
                    {
                        //「連番の中で 「抜け番」見つけるSQL」-----------------------
                        string sql1 = $@"
                                       SELECT DISTINCT address_no +1 as address_no
                                       FROM m_address
                                       WHERE address_no + 1 NOT IN (SELECT DISTINCT address_no FROM m_address)    
                                                                                     ";
                        var insertaddress_no = database.SelectData(sql1);
                        //------------------------------------------------------

                        //SELECT address_no-------------------------------------- 
                        string sql_address = $@"SELECT (address_no) AS value_address
                                       FROM [m_address]";
                        var select_address = database.SelectData(sql_address);
                        //------------------------------------------------------

                        int Address_no = Convert.ToInt32(insertaddress_no.Rows[0]["address_no"]); // 不足している値
                        database.SetParam("@Address_No", SqlDbType.Int, Address_no);//エリア

                        //[m_transit_time]Tableで(address_no_from と　address_noto )にINSERTする。------------------

                        for (int j = 0; j < select_address.Rows.Count; j++)
                        {
                            int address = int.Parse(select_address.Rows[j]["value_address"].ToString());
                            database.SetParam("@Address_insert", SqlDbType.Int, address);//address追加。
                            database.SetParam("@Transit_time", SqlDbType.Int, 0);
                            if (address < Address_no)
                            {
                                for (int k = 0; k < 2; k++)
                                {
                                    database.SetParam("@wayType", SqlDbType.Int, k);
                                    string sql_transit = $@"INSERT INTO [m_transit_time] (
                                            address_no_from,
                                            address_no_to,
                                            wayType,
                                            transit_time)
                                    VALUES (
                                            @Address_insert,
                                            @Address_No,
                                            @wayType,
                                            @Transit_time)";
                                    bool result_transit = database.InsertData(sql_transit);
                                }
                            }
                            else
                            {
                                for (int k = 0; k < 2; k++)
                                {
                                    database.SetParam("@wayType", SqlDbType.Int, k);
                                    string sql_transit = $@"INSERT INTO [m_transit_time] (
                                            address_no_from, 
                                            address_no_to,
                                            wayType,
                                            transit_time)
                                    VALUES (
                                            @Address_No,
                                            @Address_insert,
                                            @wayType,
                                            @Transit_time)";
                                    bool result_transit = database.InsertData(sql_transit);
                                }
                            }
                        }
                        //[m_address]Tableで(address_no)にINSERTする。------------------

                        string sql2 = $@"INSERT INTO [m_address] (
                                        address_no, 
                                        address)
                                VALUES (
                                        @Address_No, 
                                        @Address)";
                        bool result = database.InsertData(sql2);

                        //[m_into]Tableで登録の値をINSERTする。------------------

                        if (depo_flg == 1)
                        {
                            string sql3 = $@"INSERT INTO [m_into] (
                                        into_cd, 
                                        into_name,
                                        address_no,
                                        bin_no,
                                        work_time,
                                        week,
                                        depo_flg)
                                VALUES (
                                        @Into_Cd, 
                                        @Into_Name,
                                        @Address_No,
                                        @Bin_No,
                                        @Work_time,
                                        @Week,
                                        @Depo_FLG)";
                            bool result1 = database.InsertData(sql3);
                        }
                        else
                        {
                            string sql3 = $@"INSERT INTO [m_into] (
                                        into_cd, 
                                        into_name,
                                        address_no,
                                        bin_no,
                                        available_car, 
                                        time_flg, 
                                        start_time,
                                        end_time, 
                                        work_time,
                                        week,
                                        area_no,
                                        depo_flg)
                                VALUES (
                                        @Into_Cd, 
                                        @Into_Name,
                                        @Address_No,
                                        @Bin_No,
                                        @Available_car,
                                        @Time_FLG,
                                        @Start_Time,
                                        @End_Time,
                                        @Work_time,
                                        @Week,
                                        @Area_No,
                                        @Depo_FLG)";
                            bool result1 = database.InsertData(sql3);
                        }
                    }

                    //トランザクションが成功しました、べてのデータ変更がデータベースに保存されます。------------------
                    database.CommitTransaction();

                    ViewData["Message"] = "登録が完了しました。";
                    return View("Index", value);
                }
                catch (Exception ex)
                {
                    var sqlException = ex as SqlException;
                    //transaction.Rollback();

                    if (sqlException.Number == 2627)
                    {

                        ViewData["Error"] = "納入先コードと便Noが重複しています。";

                    }
                    else
                    {
                        ViewData["Error"] = "データベース登録処理エラー";
                    }

                    return View("Index", value);
                }
                finally
                {
                    database.Dispose();
                }
            }
        }

    }
}
