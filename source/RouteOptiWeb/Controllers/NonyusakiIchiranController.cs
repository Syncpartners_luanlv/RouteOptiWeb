﻿using Microsoft.AspNetCore.Mvc;
using RouteOptiWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using X.PagedList;


namespace RouteOptiWeb.Controllers
{
    public class NonyusakiIchiranController : Controller
    {
        public IActionResult Index(NonyusakiTourokuModel model)
        {
            if (model == null)
            {
                model = new NonyusakiTourokuModel();
            }
            var list = GetIchiran(model.NonyusakiSearch);
            if (list.Count > 0)
            {
                IEnumerable<NonyusakiTourokuModel> query = list.Select(s => s);
                model.NonyusakiList = query.ToPagedList();
            }
            return View(model);

        }

        //[HttpPost]--初めて納入先一覧画面表示する================================
        private List<NonyusakiTourokuModel> GetIchiran(string NonyusakiSearch)
        {
            DataTable dt;
            List<NonyusakiTourokuModel> strList = new List<NonyusakiTourokuModel>();

            using (MSSQLAccess database = new MSSQLAccess())
            {
                //パラメーターをクリアー
                database.ClearParam();

                string sqlstr = @$" SELECT * FROM [m_into] AS A
                                    LEFT OUTER JOIN [m_address] AS B
        		                    ON A.address_no = B.address_no
                                    LEFT OUTER JOIN [m_area] AS C
        		                    ON A.area_no = C.area_no 
                                    WHERE (1=1)";

                if (!string.IsNullOrEmpty(NonyusakiSearch))
                {
                    NonyusakiSearch = NonyusakiSearch.Trim();
                    database.SetParam("@value", SqlDbType.NVarChar, NonyusakiSearch);//Search値         

                    if (NonyusakiSearch == "有")
                    {
                        sqlstr += @$"AND (time_flg LIKE  (''+1+''))";

                    }
                    else if (NonyusakiSearch == "無")
                    {
                        sqlstr += @$"AND (time_flg LIKE  (''+0+''))";
                    }
                    else if (NonyusakiSearch == "○")
                    {
                        sqlstr += @$"AND (week LIKE ('%'+ '1' +'%')
                                   OR depo_flg LIKE (''+1+'')
                                   )";
                    }
                    else
                    {
                        sqlstr += @$"AND (into_cd LIKE  ('%'+@value+'%')
                                   OR into_name LIKE  ('%'+@value+'%')
                                   OR address LIKE  ('%'+@value+'%')
                                   OR bin_no LIKE  ('%'+@value+'%')
                                   OR area_name LIKE  ('%'+@value+'%')
                                   OR available_car LIKE  ('%'+@value+'%')
                                   OR start_time LIKE  ('%'+@value+'%')
                                   OR end_time LIKE  ('%'+@value+'%')
                                   OR work_time LIKE  ('%'+@value+'%')
                                   )";
                    }
                }
                //最後に並べ替えを行う
                //sqlstr += "ORDER BY into_cd";

                dt = database.SelectData(sqlstr);

                NonyusakiTourokuModel Nonyusaki;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Nonyusaki = new NonyusakiTourokuModel();
                    Nonyusaki.Into_Cd = dt.Rows[i]["into_cd"].ToString();
                    Nonyusaki.Into_Name = dt.Rows[i]["into_name"].ToString();
                    Nonyusaki.Address = dt.Rows[i]["address"].ToString();
                    Nonyusaki.Bin_No = dt.Rows[i]["bin_no"].ToString();
                    Nonyusaki.Available_car = dt.Rows[i]["available_car"].ToString();
                    if (dt.Rows[i]["time_flg"] != System.DBNull.Value)
                        Nonyusaki.Time_FLG = Convert.ToBoolean(dt.Rows[i]["time_flg"]);
                    Nonyusaki.Start_Time = dt.Rows[i]["start_time"].ToString();
                    Nonyusaki.End_Time = dt.Rows[i]["end_time"].ToString();
                    Nonyusaki.Work_time = dt.Rows[i]["work_time"].ToString();
                    Nonyusaki.Week = dt.Rows[i]["week"].ToString();
                    if (dt.Rows[i]["area_no"] != System.DBNull.Value && (int)dt.Rows[i]["area_no"] != 0)
                    {
                        Nonyusaki.Area_No = (int)dt.Rows[i]["area_no"];
                        database.SetParam("@Area_No", SqlDbType.Int, Nonyusaki.Area_No);//エリア
                        //[m_area]Tableでは(area_no)値を見ます===================================
                        string sql_areaname = $@"SELECT (area_name) AS area_name
                                       FROM [m_area]
                                       WHERE (1 = 1) 
                                       AND(area_no = @Area_No) ";
                        var select_area = database.SelectData(sql_areaname);
                        Nonyusaki.Area_Name = select_area.Rows[0]["area_name"].ToString();
                    }
                    Nonyusaki.Depo_FLG = Convert.ToBoolean(dt.Rows[i]["depo_flg"]);
                    strList.Add(Nonyusaki);
                }
                return strList;
            }
        }
        //========================================================================

        /////////POST:/Edit.--初めて納入先修正の画面表示する===========================
        public ActionResult Edit(string cd, string bin, string week)
        {
            try
            {
                List<NonyusakiTourokuModel> list = GetNonyusaki(cd, bin, week);
                return View(list.FirstOrDefault());
            }
            catch (Exception ex)
            {
                ViewData["Error"] = "データベース登録処理エラー";
                return RedirectToAction("Index");
            }
        }

        /////////Get: チェック値////Button.
        public ActionResult StylesButton(string editButton, string deleteButton, NonyusakiTourokuModel value)
        {
            if (editButton != null)
            {
                return EditUpdate(value);
            }
            else if (deleteButton != null)
            {
                return deleteData(value);
            }
            else
            {
                return View();
            }
        }

        //[HttpPost]-------------------------------------
        private List<NonyusakiTourokuModel> GetNonyusaki(string cd, string bin, string week)
        {
            DataTable dt;
            List<NonyusakiTourokuModel> strEditList = new List<NonyusakiTourokuModel>();
            using (MSSQLAccess database = new MSSQLAccess())
            {
                //パラメーターをクリアー
                database.ClearParam();
                //パラメータ必須-------------------------------------------------------------
                database.SetParam("@cd", SqlDbType.NVarChar, cd); //納入先コード
                database.SetParam("@bin", SqlDbType.NVarChar, bin); //便No
                database.SetParam("@week", SqlDbType.NVarChar, week);//曜日指定
                //---------------------------------------------------------------------------

                string sqlstr = @$" SELECT * FROM [m_into] AS A
                                    LEFT OUTER JOIN [m_address] AS B
        		                    ON A.address_no = B.address_no    
                                    LEFT OUTER JOIN [m_area] AS C
        		                    ON A.area_no = C.area_no
                                    WHERE (A.into_cd  = @cd)
                                      AND (A.bin_no   = @bin)
                                      AND (A.week     = @week) ";

                dt = database.SelectData(sqlstr);
                NonyusakiTourokuModel strEdit;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strEdit = new NonyusakiTourokuModel();
                    strEdit.Into_Cd = dt.Rows[i]["into_cd"].ToString();
                    strEdit.Into_Name = dt.Rows[i]["into_name"].ToString();
                    strEdit.Address = dt.Rows[i]["address"].ToString();
                    strEdit.Bin_No = dt.Rows[i]["bin_no"].ToString();
                    strEdit.Available_car = dt.Rows[i]["available_car"].ToString();
                    if (dt.Rows[i]["time_flg"] != System.DBNull.Value)
                        strEdit.Time_FLG = Convert.ToBoolean(dt.Rows[i]["time_flg"]);
                    strEdit.Start_Time = dt.Rows[i]["start_time"].ToString();
                    strEdit.End_Time = dt.Rows[i]["end_time"].ToString();
                    strEdit.Work_time = dt.Rows[i]["work_time"].ToString();
                    strEdit.Week = dt.Rows[i]["week"].ToString();
                    if (dt.Rows[i]["area_no"] != System.DBNull.Value)
                        strEdit.Area_No = (int)dt.Rows[i]["area_no"];
                    strEdit.Area_Name = dt.Rows[i]["area_name"].ToString();
                    strEdit.Depo_FLG = Convert.ToBoolean(dt.Rows[i]["depo_flg"]);
                    strEditList.Add(strEdit);
                }
                return strEditList;
            }
        }
        //==============================================================================

        ////// EDIT:Update納入先更新/======================================================
        [HttpPost]
        private ActionResult EditUpdate(NonyusakiTourokuModel Nonyusaki)
        {
            using (MSSQLAccess database = new MSSQLAccess())
            {
                int depo_flg = 0;
                int time_flg = 0;

                depo_flg = (Nonyusaki.Depo_FLG == true) ? 1 : 0;
                time_flg = (Nonyusaki.Time_FLG == true) ? 1 : 0;

                //曜日全てチェックなしならエラーにする
                if (Nonyusaki.Week == "0000000")
                {
                    ViewData["Error"] = "いづれかの曜日をチェックしてください。";
                    return View("Edit", Nonyusaki);
                }
                if (int.TryParse(Nonyusaki.Work_time, out int WorkTime) == true)
                {
                    if (WorkTime < 0 || WorkTime > 120)
                    {
                        ViewData["Error"] = "納入先作業時間(分)は0から120の間で入力してください。";
                        return View("Edit", Nonyusaki);
                    }
                }
                else
                {
                    ViewData["Error"] = "納入先作業時間(分)は小数点で入力不可です。";
                    return View("Edit", Nonyusaki);
                }

                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    //トランザクションが成功ならべてのデータ変更がデータベースに保存されます。エラーがあるならデータはトランザクションが実行される前の状態に復元されます！
                    database.SetTransaction();

                    if (depo_flg == 1)
                    {
                        //パラメータ必須[m_into]============================================
                        database.SetParam("@Into_Cd", SqlDbType.NVarChar, Nonyusaki.Into_Cd);//納入先コード
                        database.SetParam("@Into_Name", SqlDbType.NVarChar, Nonyusaki.Into_Name);//納入先名
                        database.SetParam("@Address", SqlDbType.NVarChar, Nonyusaki.Address);//住所
                        database.SetParam("@Bin_No", SqlDbType.NVarChar, "0");//便No
                        database.SetParam("@Week", SqlDbType.NVarChar, "1111111");//曜日指定
                        database.SetParam("@Work_time", SqlDbType.Int, Nonyusaki.Work_time);//納入先作業時間(分)
                        database.SetParam("@Depo_FLG", SqlDbType.Int, depo_flg);//デポ
                        //=================================================================

                        //[m_into]Tableでは(address_no)値を取得する============================
                        string sqladdress_no = @$" SELECT [address_no] 
                                            FROM [m_into] 
                                            WHERE (into_cd  = @Into_Cd)
                                            AND   (bin_no   = @Bin_No)
                                            AND   (week     = @Week) ";
                        var address_no = database.SelectData(sqladdress_no);
                        int Adr_no = Convert.ToInt32(address_no.Rows[0]["address_no"]); //(value address_no in DB)
                        database.SetParam("@Adr_no", SqlDbType.Int, Adr_no);//Address number
                        //=================================================================

                        //[m_address]Tableでは(address)値を取得する=============================
                        string sqlstr = @$" SELECT [address] 
                                            FROM [m_address]
                                            WHERE (1 = 1)
                                            AND (address_no= @Adr_no) ";
                        var address_value = database.SelectData(sqlstr);
                        var address_name = address_value.Rows[0]["address"].ToString();
                        //==================================================================
                        //新住所を入力には住所のDBが重複あるかどうかを確認。========================
                        if (address_name == Nonyusaki.Address) //(address 重複。)
                        {
                            //[m_into]TableでにUPDATEする====================================
                            string into_update1 = $@"UPDATE [m_into] SET 
                                                  [into_name]    = @Into_Name,
                                                  [work_time]    = @Work_time
                                                  WHERE (into_cd = @Into_Cd)
                                                    AND (bin_no  = @Bin_No)
                                                    AND (week    = @Week)";
                            bool result1 = database.UpdateData(into_update1);
                        }
                        else  //(address 重複しない。)
                        {
                            //[m_into]Tableでは(address_no)重複が確認===========================
                            //SQLの準備
                            string commandText = $@" SELECT count([address_no]) AS Address
                                         FROM [m_into]  
                                         WHERE address_no = (SELECT address_no FROM [m_into] WHERE (into_cd= @Into_Cd) AND (bin_no=@Bin_No) AND (week=@Week))";
                            var selectAddress = database.SelectData(commandText);

                            int value_address = Convert.ToInt32(selectAddress.Rows[0]["Address"]);
                            //===============================================================

                            //SELECT address-Row[address]が持っている値の数=======================
                            string sql_address = $@"SELECT (address) AS address
                                                FROM [m_address]";
                            var select_address = database.SelectData(sql_address);
                            int flg = 0;
                            //===============================================================
                            if (value_address > 1) //(address_no 重複、 他のaddress in DB 重複。)
                            {
                                for (int i = 0; i < select_address.Rows.Count; i++)
                                {
                                    var address = select_address.Rows[i]["address"].ToString();
                                    database.SetParam("@Address_no_into", SqlDbType.Int, i + 1);//address_no の値は重複
                                    if (Nonyusaki.Address == address)// (address_no trung va ten trung)
                                    {
                                        flg = 1;
                                        //[m_into]TableでにUPDATEする==========================
                                        string into_update2 = $@"UPDATE [m_into] SET 
                                                               [into_name]    = @Into_Name,
                                                               [work_time]    = @Work_time,
                                                               [address_no]    = @Address_no_into
                                                               WHERE (into_cd = @Into_Cd)
                                                               AND   (bin_no  = @Bin_No)
                                                               AND   (week    = @Week)";
                                        bool result2 = database.UpdateData(into_update2);
                                        break;
                                    }
                                }
                                if (flg != 1) //(address_no 重複、 他のaddress in DB 重複しない。)
                                {
                                    //「連番の中で 「抜け番」見つけるSQL」-----------------------
                                    string sql1 = $@"
                                       SELECT DISTINCT address_no +1 as address_no
                                       FROM m_address
                                       WHERE address_no + 1 NOT IN (SELECT DISTINCT address_no FROM m_address)    
                                                  ";
                                    var insertaddress_no = database.SelectData(sql1);
                                    int Address_no = Convert.ToInt32(insertaddress_no.Rows[0]["address_no"]); // 不足している値
                                    database.SetParam("@Address_No", SqlDbType.Int, Address_no);//Address_number不足
                                    //=========================================================
                                    //SELECT address_no-Row[address_no]が持っている値の数===========
                                    string sql_address_no = $@"SELECT (address_no) AS value_address
                                                              FROM [m_address]";
                                    var select_address_no = database.SelectData(sql_address_no);
                                    //==========================================================

                                    for (int j = 0; j < select_address_no.Rows.Count; j++)
                                    {
                                        int address_number = int.Parse(select_address_no.Rows[j]["value_address"].ToString());
                                        database.SetParam("@Address_insert", SqlDbType.Int, address_number);//address_number追加。
                                        database.SetParam("@Transit_time", SqlDbType.Int, 0);
                                        if (address_number < Address_no)
                                        {
                                            for (int k = 0; k < 2; k++)
                                            {
                                                database.SetParam("@wayType", SqlDbType.Int, k);
                                                string sql_transit = $@"INSERT INTO [m_transit_time](
                                                                               address_no_from,
                                                                               address_no_to,
                                                                               wayType,
                                                                               transit_time)
                                                                       VALUES (
                                                                               @Address_insert,
                                                                               @Address_No,
                                                                               @wayType,
                                                                               @Transit_time)";
                                                bool result_transit = database.InsertData(sql_transit);
                                            }
                                        }
                                        else
                                        {
                                            for (int k = 0; k < 2; k++)
                                            {
                                                database.SetParam("@wayType", SqlDbType.Int, k);
                                                string sql_transit = $@"INSERT INTO [m_transit_time] (
                                                                               address_no_from, 
                                                                               address_no_to,
                                                                               wayType,
                                                                               transit_time)
                                                                       VALUES (
                                                                               @Address_No,
                                                                               @Address_insert,
                                                                               @wayType,
                                                                               @Transit_time)";
                                                bool result_transit = database.InsertData(sql_transit);
                                            }
                                        }
                                    }
                                    //[m_address]Tableで(address_no)にINSERTする。==================
                                    string sql2 = $@"INSERT INTO [m_address] (
                                                            address_no, 
                                                            address)
                                                    VALUES (
                                                            @Address_No, 
                                                            @Address)";
                                    bool result = database.InsertData(sql2);
                                    //============================================================

                                    //[m_into]TableにUPDATEする=========================================
                                    string into_update2 = $@"UPDATE [m_into] SET 
                                                          [into_name]    = @Into_Name,
                                                          [work_time]    = @Work_time,
                                                          [address_no]    = @Address_No
                                                          WHERE (into_cd = @Into_Cd)
                                                          AND (bin_no    = @Bin_No)
                                                          AND (week      = @Week)";

                                    bool result2 = database.UpdateData(into_update2);
                                }
                            }
                            else // (address_no 重複しない。)
                            {
                                for (int i = 0; i < select_address.Rows.Count; i++)
                                {
                                    var address = select_address.Rows[i]["address"].ToString();
                                    database.SetParam("@Address_no_into", SqlDbType.Int, i + 1);//address_no の値は重複
                                    if (Nonyusaki.Address == address)// (address_no 重複しない、他のaddress in DB 重複 )
                                    {
                                        flg = 1;
                                        //[m_address]]Tableに(address_no)と(address)を削除します。=====
                                        string sql_deleteaddress = $@"DELETE FROM [m_address] 
　　　　　　　　　　　　　                                               WHERE (address_no = @Adr_no)";
                                        database.DeleteData(sql_deleteaddress);
                                        //=======================================================
                                        //[m_transit_time]]Tableに(address_no_from)とを削除します。==
                                        string sql_deletetransit = $@"DELETE FROM [m_transit_time] 
　　　　　　　　　　　　　                                              WHERE (address_no_from = @Adr_no)
                                                                    OR  (address_no_to = @Adr_no)";
                                        database.DeleteData(sql_deletetransit);
                                        //=======================================================

                                        //[m_into]TableでにUPDATEする==============================
                                        string into_update2 = $@"UPDATE [m_into] SET 
                                                              [into_name]    = @Into_Name,
                                                              [work_time]    = @Work_time,
                                                              [address_no]    = @Address_no_into
                                                              WHERE (into_cd = @Into_Cd)
                                                              AND (bin_no    = @Bin_No)
                                                              AND (week      = @Week)";
                                        bool result2 = database.UpdateData(into_update2);
                                        break;
                                    }
                                }
                                if (flg != 1) //(address_no 重複しない、他のaddress in DB 重複しない。)
                                {
                                    //[m_address]Tableで(address)にUPDATEする。------------------
                                    string sql2 = $@"UPDATE [m_address] SET
                                                 [address] =@Address       
                                                 WHERE (address_no = @Adr_no)  
                                                    ";
                                    bool result = database.UpdateData(sql2);

                                    //[m_into]TableにUPDATEする。------------------
                                    string sql3 = $@"UPDATE [m_into] SET
                                                 [into_name]    = @Into_Name,
                                                 [work_time]    = @Work_time       
                                                 WHERE (into_cd = @Into_Cd)
                                                 AND (bin_no    = @Bin_No)
                                                 AND (week      = @Week)  
                                                    ";
                                    bool result2 = database.UpdateData(sql3);
                                }
                            }
                        }
                    }
                    else
                    {
                        //パラメータ必須[m_into]==============================================
                        database.SetParam("@Into_Cd", SqlDbType.NVarChar, Nonyusaki.Into_Cd);//納入先コード
                        database.SetParam("@Into_Name", SqlDbType.NVarChar, Nonyusaki.Into_Name);//納入先名
                        database.SetParam("@Address", SqlDbType.NVarChar, Nonyusaki.Address);//住所
                        database.SetParam("@Bin_No", SqlDbType.NVarChar, Nonyusaki.Bin_No);//便No
                        database.SetParam("@Available_car", SqlDbType.NVarChar, Nonyusaki.Available_car);//可能車両
                        database.SetParam("@Time_FLG", SqlDbType.Int, time_flg);//時間指定
                        if (time_flg == 0)
                        {
                            database.SetParam("@Start_Time", SqlDbType.NVarChar, "08:00");//時間指定開始
                            database.SetParam("@End_Time", SqlDbType.NVarChar, "09:00");//時間指定終了
                        }
                        else
                        {
                            database.SetParam("@Start_Time", SqlDbType.NVarChar, Nonyusaki.Start_Time);//時間指定開始
                            database.SetParam("@End_Time", SqlDbType.NVarChar, Nonyusaki.End_Time);//時間指定終了
                        }
                        database.SetParam("@Work_time", SqlDbType.Int, Nonyusaki.Work_time);//納入先作業時間(分)
                        database.SetParam("@Week", SqlDbType.NVarChar, Nonyusaki.Week);//曜日指定
                        database.SetParam("@Week_DB", SqlDbType.NVarChar, Nonyusaki.Week_DB);//修正前の曜日指定の値
                        database.SetParam("@Area_Name", SqlDbType.NVarChar, Nonyusaki.Area_Name);//エリア
                        database.SetParam("@Depo_FLG", SqlDbType.Int, depo_flg);//デポ
                        //[m_area]Tableでは(area_no)値を見ます================================
                        string sql_area = $@"SELECT (area_no) AS value_area
                                       FROM [m_area]
                                       WHERE (1 = 1) 
                                       AND(area_name = @Area_Name) ";
                        var select_area = database.SelectData(sql_area);
                        int value_area = Convert.ToInt32(select_area.Rows[0]["value_area"]);
                        database.SetParam("@Area_No", SqlDbType.Int, value_area);//エリア
                        //=================================================================

                        //[m_into]Tableでは(row_week)値を取得する============================
                        string sqlweek = @$" SELECT [week] AS value_week
                                             FROM  [m_into] 
                                             WHERE (into_cd  = @Into_Cd)
                                             AND   (bin_no   = @Bin_No)
                                                    ";
                        var week = database.SelectData(sqlweek);

                        for (int i = 0; i < week.Rows.Count; i++)
                        {
                            if (Nonyusaki.Week_DB != week.Rows[i]["value_week"].ToString())
                            {
                                int num3 = Convert.ToInt32(int.Parse(week.Rows[i]["value_week"].ToString()).ToString(), 2);//値の型を2進数から10進数に変換します
                                int num4 = Convert.ToInt32((int.Parse(Nonyusaki.Week)).ToString(), 2);//値の型を2進数から10進数に変換します
                                //曜日指定が重複時==========
                                if ((num3 & num4) != 0)
                                {
                                    ViewData["Error"] = "納入先コードと便(No)と曜日の重複があるので登録出来ません。";
                                    return View("Edit", Nonyusaki);
                                }
                            }
                        }

                        //[m_into]Tableでは(address_no)値を取得する============================
                        string sqladdress_no = @$" SELECT [address_no] 
                                                   FROM  [m_into] 
                                                   WHERE (into_cd  = @Into_Cd)
                                                   AND   (bin_no   = @Bin_No)
                                                   AND   (week     = @Week_DB) ";
                        var address_no = database.SelectData(sqladdress_no);
                        int Adr_no = Convert.ToInt32(address_no.Rows[0]["address_no"]); //(value address_no in DB)
                        database.SetParam("@Adr_no", SqlDbType.Int, Adr_no);//Address number
                        //=================================================================

                        //[m_address]Tableでは(address)値を取得する==========================
                        string sqlstr = @$" SELECT [address] 
                                            FROM [m_address]
                                            WHERE (1 = 1)
                                            AND (address_no= @Adr_no) ";
                        var address_value = database.SelectData(sqlstr);
                        var address_name = address_value.Rows[0]["address"].ToString();//address_nameを取得する
                        //==================================================================

                        //新住所を入力には住所のDBが重複あるかどうかを確認。========================
                        if (address_name == Nonyusaki.Address) //(address 重複。)
                        {
                            //[m_into]TableでにUPDATEする====================================
                            string into_update1 = $@"UPDATE [m_into] SET 
                                                  [into_name]     = @Into_Name,
                                                  [available_car] = @Available_car,
                                                  [work_time]     = @Work_time,
                                                  [time_flg]      = @Time_FLG,
                                                  [start_time]    = @Start_Time,
                                                  [end_time]      = @End_Time,
                                                  [week]          = @Week,
                                                  [area_no]       = @Area_No
                                                   WHERE (into_cd = @Into_Cd)
                                                     AND (bin_no  = @Bin_No)
                                                     AND (week    = @Week_DB)";
                            bool result1 = database.UpdateData(into_update1);
                        }
                        else  //(address 重複しない。)
                        {
                            //[m_into]Tableでは(address_no)重複が確認===========================
                            string commandText = $@" SELECT count([address_no]) AS Address
                                         FROM [m_into]  
                                         WHERE address_no = (SELECT address_no FROM [m_into] WHERE (into_cd= @Into_Cd) AND (bin_no=@Bin_No) AND (week=@Week))";
                            var selectAddress = database.SelectData(commandText);

                            int value_address = Convert.ToInt32(selectAddress.Rows[0]["Address"]);
                            //===============================================================

                            //SELECT address-Row[address]が持っている値の数=======================
                            string sql_address = $@"SELECT (address) AS address
                                            FROM [m_address]";
                            var select_address = database.SelectData(sql_address);
                            int flg = 0;
                            //===============================================================
                            if (value_address > 1) //(address_no 重複、 他のaddress in DB 重複。)
                            {
                                for (int i = 0; i < select_address.Rows.Count; i++)
                                {
                                    var address = select_address.Rows[i]["address"].ToString();
                                    database.SetParam("@Address_no_into", SqlDbType.Int, i + 1);//address_no の値は重複
                                    if (Nonyusaki.Address == address)// (address_no 重複 and address 重複)
                                    {
                                        flg = 1;
                                        //[m_into]TableでにUPDATEする==========================
                                        string into_update2 = $@"UPDATE [m_into] SET 
                                                              [into_name]     = @Into_Name,
                                                              [available_car] = @Available_car,
                                                              [work_time]     = @Work_time,
                                                              [time_flg]      = @Time_FLG,
                                                              [start_time]    = @Start_Time,
                                                              [end_time]      = @End_Time,
                                                              [week]          = @Week,
                                                              [area_no]       = @Area_No,
                                                              [address_no]     = @Address_no_into
                                                                WHERE (into_cd = @Into_Cd)
                                                                AND (bin_no  = @Bin_No)
                                                                AND (week    = @Week_DB)";
                                        bool result2 = database.UpdateData(into_update2);
                                        break;
                                    }
                                }
                                if (flg != 1) //(address_no 重複、 他のaddress in DB 重複しない。)
                                {
                                    //「連番の中で 「抜け番」見つけるSQL」-----------------------
                                    string sql1 = $@"
                                       SELECT DISTINCT address_no +1 as address_no
                                       FROM m_address
                                       WHERE address_no + 1 NOT IN (SELECT DISTINCT address_no FROM m_address)    
                                                  ";
                                    var insertaddress_no = database.SelectData(sql1);
                                    int Address_no = Convert.ToInt32(insertaddress_no.Rows[0]["address_no"]); // 不足している値
                                    database.SetParam("@Address_No", SqlDbType.Int, Address_no);//Address_number不足
                                    //=========================================================
                                    //SELECT address_no-Row[address_no]が持っている値の数===========
                                    string sql_address_no = $@"SELECT (address_no) AS value_address
                                                              FROM [m_address]";
                                    var select_address_no = database.SelectData(sql_address_no);
                                    //==========================================================

                                    for (int j = 0; j < select_address_no.Rows.Count; j++)
                                    {
                                        int address_number = int.Parse(select_address_no.Rows[j]["value_address"].ToString());
                                        database.SetParam("@Address_insert", SqlDbType.Int, address_number);//address_number追加。
                                        database.SetParam("@Transit_time", SqlDbType.Int, 0);
                                        if (address_number < Address_no)
                                        {
                                            for (int k = 0; k < 2; k++)
                                            {
                                                database.SetParam("@wayType", SqlDbType.Int, k);
                                                string sql_transit = $@"INSERT INTO [m_transit_time](
                                                                               address_no_from,
                                                                               address_no_to,
                                                                               wayType,
                                                                               transit_time)
                                                                       VALUES (
                                                                               @Address_insert,
                                                                               @Address_No,
                                                                               @wayType,
                                                                               @Transit_time)";
                                                bool result_transit = database.InsertData(sql_transit);
                                            }
                                        }
                                        else
                                        {
                                            for (int k = 0; k < 2; k++)
                                            {
                                                database.SetParam("@wayType", SqlDbType.Int, k);
                                                string sql_transit = $@"INSERT INTO [m_transit_time] (
                                                                               address_no_from, 
                                                                               address_no_to,
                                                                               wayType,
                                                                               transit_time)
                                                                       VALUES (
                                                                               @Address_No,
                                                                               @Address_insert,
                                                                               @wayType,
                                                                               @Transit_time)";
                                                bool result_transit = database.InsertData(sql_transit);
                                            }
                                        }
                                    }
                                    //[m_address]Tableで(address_no)にINSERTする。==================
                                    string sql2 = $@"INSERT INTO [m_address] (
                                                            address_no, 
                                                            address)
                                                    VALUES (
                                                            @Address_No, 
                                                            @Address)";
                                    bool result = database.InsertData(sql2);
                                    //============================================================

                                    //[m_into]TableにUPDATEする=========================================
                                    string into_update2 = $@"UPDATE [m_into] SET 
                                                          [into_name]     = @Into_Name,
                                                          [work_time]     = @Work_time,
                                                          [available_car] = @Available_car,
                                                          [time_flg]      = @Time_FLG,
                                                          [start_time]    = @Start_Time,
                                                          [end_time]      = @End_Time,
                                                          [week]          = @Week,
                                                          [area_no]       = @Area_No,
                                                          [address_no]     = @Address_No
                                                           WHERE (into_cd = @Into_Cd)
                                                           AND (bin_no    = @Bin_No)
                                                           AND (week      = @Week_DB)";
                                    bool result2 = database.UpdateData(into_update2);
                                }
                            }
                            else // (address_no 重複しない。)
                            {
                                for (int i = 0; i < select_address.Rows.Count; i++)
                                {
                                    var address = select_address.Rows[i]["address"].ToString();
                                    database.SetParam("@Address_no_into", SqlDbType.Int, i + 1);//address_no の値は重複
                                    if (Nonyusaki.Address == address)// (address_no 重複しない、他のaddress in DB 重複 )
                                    {
                                        flg = 1;
                                        //[m_address]]Tableに(address_no)と(address)を削除します。=====
                                        string sql_deleteaddress = $@"DELETE FROM [m_address] 
　　　　　　　　　　　　　                                               WHERE (address_no = @Adr_no)";
                                        database.DeleteData(sql_deleteaddress);
                                        //=======================================================
                                        //[m_transit_time]]Tableに(address_no_from)とを削除します。==
                                        string sql_deletetransit = $@"DELETE FROM [m_transit_time] 
　　　　　　　　　　　　　                                               WHERE (address_no_from = @Adr_no)
                                                                    OR  (address_no_to = @Adr_no)";
                                        database.DeleteData(sql_deletetransit);
                                        //=======================================================

                                        //[m_into]TableにUPDATEする==============================
                                        string into_update2 = $@"UPDATE [m_into] SET 
                                                                [into_name]     = @Into_Name,
                                                                [work_time]     = @Work_time,
                                                                [available_car] = @Available_car,
                                                                [time_flg]      = @Time_FLG,
                                                                [start_time]    = @Start_Time,
                                                                [end_time]      = @End_Time,
                                                                [week]          = @Week,
                                                                [area_no]       = @Area_No,
                                                                [address_no]     = @Address_no_into
                                                                WHERE (into_cd  = @Into_Cd)
                                                                AND (bin_no     = @Bin_No)
                                                                AND (week       = @Week_DB)";
                                        bool result2 = database.UpdateData(into_update2);
                                        break;
                                    }
                                }
                                if (flg != 1) //(address_no 重複しない、他のaddress in DB 重複しない。)
                                {
                                    //[m_address]Tableで(address)にUPDATEする。------------------
                                    string sql2 = $@"UPDATE [m_address] SET
                                                  [address] =@Address       
                                                   WHERE (address_no = @Adr_no)  
                                                            ";
                                    bool result = database.UpdateData(sql2);

                                    //[m_into]TableにUPDATEする。------------------
                                    string sql3 = $@"UPDATE [m_into] SET
                                                   [into_name]     = @Into_Name,
                                                   [available_car] = @Available_car,
                                                   [work_time]     = @Work_time,
                                                   [time_flg]      = @Time_FLG,
                                                   [start_time]    = @Start_Time,
                                                   [end_time]      = @End_Time,
                                                   [week]          = @Week,
                                                   [area_no]       = @Area_No       
                                                   WHERE (into_cd  = @Into_Cd)
                                                   AND (bin_no     = @Bin_No)
                                                   AND (week       = @Week_DB) 
                                                                ";
                                    bool result_into = database.UpdateData(sql3);
                                }
                            }
                        }
                    }
                    //トランザクションが成功しました、べてのデータ変更がデータベースに保存されます。----
                    database.CommitTransaction();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ViewData["Error"] = "編集することは出来ません。";
                    return View("Edit", Nonyusaki);
                }
                finally
                {
                    database.Dispose();
                }
            }
        }
        //==============================================================================

        //--DeleteData-- 納入先削除/======================================================
        [HttpPost]
        public ActionResult deleteData(NonyusakiTourokuModel value)
        {
            DataTable dt;
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    //トランザクションが成功ならべてのデータ変更がデータベースに保存されます。エラーがあるならデータはトランザクションが実行される前の状態に復元されます！
                    database.SetTransaction();

                    //パラメータ必須-------------------------------------------------------------
                    database.SetParam("@Into_cd", SqlDbType.NVarChar, value.Into_Cd); //納入先コード
                    database.SetParam("@Bin_no", SqlDbType.NVarChar, value.Bin_No); //便No
                    database.SetParam("@Week", SqlDbType.NVarChar, value.Week);//曜日指定
                    //database.SetParam("@Address", SqlDbType.NVarChar, value.Address);//曜日指定
                    //---------------------------------------------------------------------------
                    //[m_into]Tableでは(address_no)重複が確認===============================
                    //SQLの準備
                    string commandText = $@" SELECT count([address_no]) AS Address
                                         FROM [m_into]  
                                         WHERE address_no = (SELECT address_no FROM [m_into] WHERE (into_cd= @Into_cd) AND (bin_no=@Bin_no) AND (week=@Week))";
                    var selectAddress = database.SelectData(commandText);

                    int value_address = Convert.ToInt32(selectAddress.Rows[0]["Address"]);
                    //=====================================================================

                    if (value_address > 1)
                    {
                        string sql = $@"DELETE FROM [m_into] 
　　　　　　　　　　　　　                     WHERE (into_cd= @Into_cd)
　　　　　　　　　　　　　                     AND (bin_no=@Bin_no)
　　　　　　　　　　　　　                     AND (week=@Week)";
                        database.DeleteData(sql);
                    }
                    else
                    {
                        //[m_address]Tableでは(address_no)値を取得する===============================
                        string sql_address = $@"SELECT address_no
                                               FROM [m_into]
                                               WHERE (into_cd= @Into_cd) 
                                               AND (bin_no=@Bin_no)
                                               AND (week=@Week)";
                        dt = database.SelectData(sql_address);
                        int Address_no = Convert.ToInt32(dt.Rows[0]["address_no"]);
                        database.SetParam("@Address_no", SqlDbType.Int, Address_no); //Address_no
                        //=====================================================================
                        string sql = $@"DELETE FROM [m_into] 
　　　　　　　　　　　　　                     WHERE (into_cd= @Into_cd)
　　　　　　　　　　　　　                     AND (bin_no=@Bin_no)
　　　　　　　　　　　　　                     AND (week=@Week)";
                        database.DeleteData(sql);

                        string sql2 = $@"DELETE FROM [m_address] 
　　　　　　　　　　　　　                     WHERE (1= 1)
　　　　　　　　　　　　　                     AND (address_no= @Address_no)";
                        database.DeleteData(sql2);
                        //=====================================================================
                        string sql3 = $@"DELETE FROM [m_transit_time] 
　　　　　　　　　　　　　                     WHERE (address_no_from = @Address_no)
　　　　　　　　　　　　　                     OR    (address_no_to   = @Address_no)";

                        database.DeleteData(sql3);
                    }
                    //トランザクションが成功しました、べてのデータ変更がデータベースに保存されます。------------------
                    database.CommitTransaction();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewData["Error"] = "削除することは出来ません。";
                    return View("Edit");
                }
            }
        }
    }
}
