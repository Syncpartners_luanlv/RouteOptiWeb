﻿using Microsoft.AspNetCore.Mvc;
using RouteOptiWeb.Common;
using RouteOptiWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RouteOptiWeb.Controllers
{
    public class NonyusakiTorikomiController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult Button_kata(List<IFormFile> UploadFile)
        //{
        //    var files = UploadFile;
        //    var fileList = new List<NonyusakiTorikomiModel>();
        //    var headerList = new List<List<NonyusakiTorikomiModel.Header>>();
        //    var detailrList = new List<List<NonyusakiTorikomiModel.Detail>>();

        //    // 対象のファイル形式================
        //    var file_xlsx = ".xlsx";
        //    var file_xls = ".xls";
        //    var file_xlw = ".xlw";
        //    //================================

        //    ViewData.Add("erroremess", null);
        //    ViewData.Add("successmess", null);



        //    return View();

        //}

        public IActionResult ExcelButton()
        {
            DataTable dt = GetNonyusakiData();

            //会社ID取得
            int wid = int.Parse(User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).First().Value);
            //ユーザー名取得
            string UserCode = User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Skip(2).First().Value.ToString();

            // ファイル名
            var tmpFilename = "納入先情報取込_" + wid + "_" + UserCode + DateTime.Now.ToString("yyyyMMddHHmmss");

            // 出力フォルダパス
            var rootPath = Directory.GetCurrentDirectory();
            var folderPath = Path.Combine(rootPath, @"wwwroot\DownloadFiles\Nonyusaki");

            //Webサーバーのdownloadフォルダーがない場合は作成
            if (!System.IO.Directory.Exists(folderPath))
            {
                System.IO.Directory.CreateDirectory(folderPath);
            }

            // 出力パス
            var expPath = Path.Combine(folderPath, tmpFilename);

            //ヘッダーをListに作成
            List<string> hederList = Util.HederListCreate("Nonyusaki");

            // Excelファイル生成
            Util.ExportExcel(dt, expPath, hederList, null, "", "納入先情報取込");

            var file = System.IO.File.ReadAllBytes(expPath);
            return File(file, System.Net.Mime.MediaTypeNames.Application.Octet, "納入先情報取込.xlsx");
        }

        private DataTable GetNonyusakiData()
        {
            DataTable data;
            using (MSSQLAccess database = new MSSQLAccess())
            {
                //パラメーターをクリアー
                database.ClearParam();

                int wid = int.Parse(User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).First().Value);//会社IDの取得
                //会社IDの取得
                database.SetParam("@wid", SqlDbType.Int, wid);//パラメータセット
                //---------------------------------------------------------------------------

                string sqlstr = @$" SELECT 
                                          A.into_cd,
                                          A.into_name,
                                          B.address,
                                          A.bin_no,
                                          A.available_car,
                                          A.work_time,
                                          CASE WHEN A.time_flg = 1 THEN '有' ELSE '無' END,
                                          A.start_time,
                                          A.end_time,
                                          CASE WHEN SUBSTRING(A.week, 1, 1) = 1 THEN '〇' ELSE '' END,
                                          CASE WHEN SUBSTRING(A.week, 2, 1) = 1 THEN '〇' ELSE '' END,
                                          CASE WHEN SUBSTRING(A.week, 3, 1) = 1 THEN '〇' ELSE '' END,
                                          CASE WHEN SUBSTRING(A.week, 4, 1) = 1 THEN '〇' ELSE '' END,
                                          CASE WHEN SUBSTRING(A.week, 5, 1) = 1 THEN '〇' ELSE '' END,
                                          CASE WHEN SUBSTRING(A.week, 6, 1) = 1 THEN '〇' ELSE '' END,
                                          CASE WHEN SUBSTRING(A.week, 7, 1) = 1 THEN '〇' ELSE '' END,
                                          C.area_name,
                                          CASE WHEN A.depo_flg = 1 THEN '〇' ELSE '' END
                                     FROM 
                                          [m_into] AS A
                                          LEFT OUTER JOIN [m_address] AS B
                                          ON A.address_no = B.address_no
                                          LEFT OUTER JOIN [m_area] AS C
                                          ON A.area_no = C.area_no 
                                         ";

                data = database.SelectData(sqlstr);
            }
            return data;
        }
    }
}