﻿using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using RouteOptiWeb.Common;
using RouteOptiWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace RouteOptiWeb.Controllers
{
    public class AreaTorikomiController : Controller
    {
        private readonly string ErrorMsg = "接続異常のためネットワーク接続を確認してください。";
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ExportExcel()
        {
            DataTable dt = GetAreaInfoData();

            //会社ID取得
            int wid = int.Parse(User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).First().Value);
            //ユーザー名取得
            string userCode = User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Skip(2).First().Value.ToString();

            // ファイル名
            var tmpFilename = "アリア情報取込_" + wid + "_" + userCode + DateTime.Now.ToString("yyyyMMddHHmmss");

            // 出力フォルダパス
            var rootPath = Directory.GetCurrentDirectory();
            var folderPath = Path.Combine(rootPath, @"wwwroot\DownloadFiles\Area");

            //Webサーバーのダウンロードのフォルダーが存在しないる場合に新規作成
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            // 出力パス
            var expPath = Path.Combine(folderPath, tmpFilename);

            //ヘッダーのリストを作成
            List<string> hederList = new List<string>();
            hederList.Add("アリア番号");
            hederList.Add("アリア名");

            // Excelファイル生成
            bool exportRs =  Util.ExportExcel(dt, expPath, hederList, null, "", "アリア情報取込");
            if (exportRs)
            {
                var file = System.IO.File.ReadAllBytes(expPath);
                return File(file, System.Net.Mime.MediaTypeNames.Application.Octet, "アリア情報取込.xlsx");                
            }
            else
            {
                ViewData["Error"] = ErrorMsg;
                return View("Index");
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult ImportExcel()
        {
            try
            {
                var result = GetDataFromExcelFile();
                return Json(new { status = true, data = JsonSerializer.Serialize(result) });
            }
            catch(Exception e)
            {
                return Json(new { status = false, message = ErrorMsg });
            } 
        }

        private DataTable GetAreaInfoData()
        {
            DataTable data;
            using (MSSQLAccess database = new MSSQLAccess())
            {
                //パラメーターをクリアー
                database.ClearParam();
                              
                //---------------------------------------------------------------------------
                string sqlstr = @$" SELECT * FROM  [m_area] ";
                data = database.SelectData(sqlstr);
            }
            return data;
        }

        private AreaTorikomiModel GetDataFromExcelFile()
        {
            var files = HttpContext.Request.Form.Files;
            AreaTorikomiModel result = new AreaTorikomiModel();
            if (files != null && files.Count() > 0)
            {
               



                FileInfo fi = new FileInfo(files[0].FileName);                
                

                //using (ExcelPackage package = new ExcelPackage(fi))
                //{
                //    ExcelPackage. = LicenseContext.NonCommercial;
                //    var workbook = package.Workbook;
                //    var worksheet = workbook.Worksheets.First();
                //    int colCount = worksheet.Dimension.End.Column;  //get Column Count
                //    int rowCount = worksheet.Dimension.End.Row;
                //    List<AreaTorikomiModel> customers = new List<AreaTorikomiModel>();
                //    AreaTorikomiModel customer = new AreaTorikomiModel();

                //    for (int row = 2; row <= rowCount; row++)
                //    {
                //        for (int col = 1; col <= colCount; col++)
                //        {
                //            if (col == 1)
                //            {
                //                customer.Area_Name = worksheet.Cells[row, col].Value?.ToString().Trim();
                //            }
                //            else
                //            {
                //                customer.Area_No = worksheet.Cells[row, col].Value?.ToString().Trim();
                //            }
                //        }

                //        customers.Add(customer);
                //    }
                //    customers.ToList().ForEach(x =>
                //    {
                //        Console.WriteLine(x);
                //    });
                //}

                if (fi.Extension == "xlsx" || fi.Extension == "xls")
                {
                    
                    result.Message = "インポート成功！！！！！！";
                }
                else
                {
                    result.Message = "EXCEL形式でないファイルが含まれているため登録できません。";
                }
            }
            else
            {
                result.Message = "サーバーとの通信に失敗しました。";
            }
            return result;
        }
    }
}
