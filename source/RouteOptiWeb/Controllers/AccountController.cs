﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RouteOptiWeb.Common;
using RouteOptiWeb.Models;

namespace RouteOptiWeb.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {

        // ログインビュー
        public IActionResult Index()
        {
            var accountModel = new AccountModel();
            return View(accountModel);
        }

        public IActionResult login()
        {
            return RedirectToAction("Index");
        }

        //TODO:AuthorizeにRoles指定時　AccessDeniedに勝手に飛ばされるよ　苦肉の策　直すべし
        public IActionResult AccessDenied()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(AccountModel account)
        {
            //入力規則チェック
            //if (!ModelState.IsValid) return View(accountViewModel);

            //ユーザー情報取得
            //var userInfoList = checkLoginUser(account.Input.WID, account.Input.DepoCode, account.Input.UserCode, account.Input.Password);
            var userInfoList = checkLoginUser(account.Input.UserCode, account.Input.Password);
            if (userInfoList.Count != 1)
            {
                //account.IsCompany = false;
                ViewData["Error"] = "ユーザーコードまたはパスワードが違います";
                return View(account);
            }
            var userInfo = userInfoList[0];

            // サインインに必要なプリンシパルを作る
            var claims = new[] {
                                new Claim(ClaimTypes.NameIdentifier, userInfo.WID.ToString()),
                                new Claim(ClaimTypes.Name, userInfo.WName),
                                new Claim(ClaimTypes.NameIdentifier, userInfo.DepoCode.ToString()),
                                new Claim(ClaimTypes.Name, userInfo.DepoName),
                                new Claim(ClaimTypes.NameIdentifier, userInfo.UserCode),
                                new Claim(ClaimTypes.Name, userInfo.UserName),
                                new Claim(ClaimTypes.Role, userInfo.Role.ToString())
                        };
            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = false,
                ExpiresUtc = DateTimeOffset.Now.AddDays(1),
                IsPersistent = account.Input.RememberMe,
            };

            // サインイン
            await HttpContext.SignInAsync(
              CookieAuthenticationDefaults.AuthenticationScheme,
              principal,
              authProperties
              );

            // ログインが必要なアクションにリダイレクト
            return RedirectToAction("Index", "Home");
        }

        public List<LoginUserModel> checkLoginUser(string userID, string password)
        {

            var ConnectionString = new GetConnectString().ConnectionString;
            var LoginUserList = new List<LoginUserModel>();

            //connection
            using (var connection = new SqlConnection(ConnectionString))
            {
                //open
                connection.Open();

                //commmand
                var commandText = $@"
                                SELECT
                                 w.WID,
                                 w.name,
                                 u.User_CD,
                                 u.User_CD,
                                 u.User_name,
                                 u.system_kanri_kubun,
                                 ud.Depo_CD,
                                 d.NLDENM
                                FROM m_user AS u
                                LEFT JOIN m_routeID AS w
                                 ON u.WID = w.WID
                                LEFT JOIN m_user_route AS ud
                                 ON u.WID = ud.WID AND u.User_CD = ud.User_CD AND ud.Main_FLG = 1
                                LEFT JOIN m_route AS d
                                 ON u.WID = d.WID AND ud.Depo_CD = d.NLDECD
                                WHERE 1=1
                                 AND u.User_CD    = '{userID}'
                                 AND u.login_pass = '{password}'
                                ";
                using (var command = new SqlCommand(commandText, connection))
                {
                    //reader
                    using (var reader = command.ExecuteReader())
                    {
                        //loop & write
                        while (reader.Read() == true)
                        {
                            var userInfo = new LoginUserModel
                            {
                                WID = Convert.ToInt32(reader["WID"]),
                                WName = (string)reader["name"],
                                UserCode = (string)reader["User_CD"],
                                UserName = (string)reader["User_name"],
                                Role = Convert.ToInt32(reader["system_kanri_kubun"]),
                                DepoCode = Convert.ToInt32(reader["Depo_CD"]),
                                DepoName = (string)reader["NLDENM"],
                            };
                            LoginUserList.Add(userInfo);
                        }
                    }
                }
            }
            return LoginUserList;
        }

        // ログアウト
        public async Task<IActionResult> Logout()
        {
            // サインアウト
            // 認証クッキーをレスポンスから削除
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            // ログインビューにリダイレクト
            return RedirectToAction("Index");
        }
    }
}
