﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Text.Json;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Authorization;
using RouteOptiWeb.Models;
using RouteOptiWeb.Common;

namespace THandyWeb.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ApiProgressController : ControllerBase
    {
        private string jsonOK = string.Empty;
        private string jsonNG = string.Empty;

        public ApiProgressController()
        {
            List<Dictionary<string, string>> parOK = new List<Dictionary<string, string>>();
            parOK.Add(new Dictionary<string, string>() { { Consts.C_JSON_NAME, Consts.C_JSON_OK } });
            jsonOK = JsonSerializer.Serialize(parOK);
            List<Dictionary<string, string>> parNG = new List<Dictionary<string, string>>();
            parNG.Add(new Dictionary<string, string>() { { Consts.C_JSON_NAME, Consts.C_JSON_NG } });
            jsonNG = JsonSerializer.Serialize(parOK);
        }

        // POST: api/<controller>
        [HttpPost]
        public string Post([FromBody] Dictionary<string, string> LstRecive)
        {

            Console.WriteLine("ApiProgressController Post : " + LstRecive);

            if (null == LstRecive)
            {
                return jsonNG;
            }

            //受信データにkeyが存在しない場合はNG
            if (!LstRecive.ContainsKey(Consts.C_JSON_SEQID))
            {
                return jsonNG;
            }

            //受信データのセット
            string strSeqID = LstRecive[Consts.C_JSON_SEQID];
            ProgressModel.GetInstance().setData(strSeqID, LstRecive);
                        
            return jsonOK;
        }
    }
}