﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text.Json;
using RouteOptiWeb.Common;
using RouteOptiWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace RouteOptiWeb.Controllers
{
    public class HaisouKeikakuTsuikaController : Controller
    {
        private readonly string ErrorMsg = "接続異常のためネットワーク接続を確認してください。";
        public IActionResult Index()
        {
            HaisouKeikakuTsuikaModel model = new HaisouKeikakuTsuikaModel();
            try
            {
                model.Deli_Date = GetDeliveryDate();
                model.Lst_HaisouKubun = GetHaisouKubun_List().Lst_HaisouKubun;
                ViewData["Error"] = GetHaisouKubun_List().ErrorMessage ?? "";
            }
            catch
            {
                ViewData["Error"] = ErrorMsg;
            }
            return View(model);
        }


        public ActionResult GetHaisouKeikakuInFor(HaisouKeikakuTsuikaModel model)
        {
            try
            {
                //ModelState.Clear();
                var result = GetHaisouKeikakuInForByHaisouKubun(model);

                return Json(new { status = true, data = JsonSerializer.Serialize(result) });
            }
            catch
            {
                return Json(new { status = false, message = ErrorMsg });
            }
        }
        public ActionResult GetHaisouKeikakuInForBySearch2(HaisouKeikakuTsuikaModel model)
        {
            try
            {
                var result = GetHaisouInForBySearch2(model);

                return Json(new { status = true, data = JsonSerializer.Serialize(result) });
            }
            catch
            {
                return Json(new { status = false, message = ErrorMsg });
            }
        }
        /*
         * 配送計画情報の配送日付を取得する
         */
        private DateTime GetDeliveryDate()
        {
            var result = DateTime.UtcNow; // 配送計画情報に値がないときは、システム日付を表示する。
            using (MSSQLAccess database = new MSSQLAccess())
            {                
                //パラメーターをクリアー
                database.ClearParam();
                database.SetTransaction();

                //SQLの準備
                string commandText = $@" SELECT DISTINCT [deli_date] as DeliveryDate FROM [t_deli_plan] order by DeliveryDate desc";
                var selectData = database.SelectData(commandText);
                if (selectData.Rows.Count > 0)
                {
                    result = (DateTime)selectData.Rows[0]["DeliveryDate"];
                }                  
                
            }
            return result;
        }
        /*
         * 配送区分情報の配送区分を取得する
         */
        private HaisouKeikakuTsuikaModel GetHaisouKubun_List()
        {
            var result = new HaisouKeikakuTsuikaModel();
            result.Lst_HaisouKubun = new List<SelectListItem>();
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    database.SetTransaction();

                    //SQLの準備
                    string commandText = $@" SELECT DISTINCT [deli_division_name] as Deli_Name ,[deli_division_no] as Deli_No FROM [m_deli_division] order by Deli_No";
                    var selectData = database.SelectData(commandText);
                    if (selectData.Rows.Count == 0)
                    {
                        result.ErrorMessage = "配送区分情報の配送区分が存在しません。";
                        return result;
                    }
                    var haisouKubun_List = new List<SelectListItem>();
                    for (int i = 0; i < selectData.Rows.Count; i++)
                    {
                        var data = selectData.Rows[i]["Deli_Name"].ToString();
                        var item = new SelectListItem { Text = data };
                        haisouKubun_List.Add(item);
                        result.Lst_HaisouKubun = haisouKubun_List;
                    }
                }
                catch
                {
                    result.ErrorMessage = ErrorMsg;
                    return result;
                }
            }
            return result;
        }
        /*
         * 配送区分情報の配送区分を取得する
         */
        private HaisouKeikakuTsuikaModel GetHaisouKeikakuInForByHaisouKubun(HaisouKeikakuTsuikaModel model)
        {
            HaisouKeikakuTsuikaModel result = new HaisouKeikakuTsuikaModel();
            if (model.HaisouKubun == "デポ")
            {
                result = GetHaisouKeikakuInForByHaisouKubun1(model);
            }
            else if (model.HaisouKubun == "休憩")
            {
                result = GetHaisouKeikakuInForByHaisouKubun2(model);
            }
            else
            {
                result = GetHaisouKeikakuInForByHaisouKubun3(model);
            }
            return result;
        }
        // 配送区分が「デポ」のとき, 納入先情報を取得する
        private HaisouKeikakuTsuikaModel GetHaisouKeikakuInForByHaisouKubun1(HaisouKeikakuTsuikaModel model)
        {
            HaisouKeikakuTsuikaModel result = new HaisouKeikakuTsuikaModel();
            var into_Cd_List = new List<SelectListItem>();
            var track_Cd_List = new List<SelectListItem>();
            var pos = ConvertWeekInChartToInt(model.Deli_Date);
            
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    database.SetTransaction();
                    //パラメータ必須-------------------------------------------------------------
                    database.SetParam("@kubun_Name", SqlDbType.NVarChar, model.HaisouKubun); //配送区分
                    database.SetParam("@depo_flg", SqlDbType.Int, 1); //デポフラグ
                    database.SetParam("@deli_date", SqlDbType.Date, model.Deli_Date); //配送日付
                    database.SetParam("@pos", SqlDbType.Int, pos); //Day Of Week

                    //SQLの準備
                    //オプション情報を取得する
                    string option_sql = $@" SELECT [Option1] as Option1 FROM [m_option]";
                    var option_data = database.SelectData(option_sql);
                    var leave_Time = "08:00";
                    if (option_data.Rows.Count > 0)
                    {
                        leave_Time = option_data.Rows[0]["Option1"].ToString();
                    }
                    //配送区分Noを取得する
                    string kubunNo_sql = $@" SELECT [deli_division_no] as Deli_TypeNo FROM [m_deli_division] WHERE [deli_division_name] =  @kubun_Name";
                    var deli_typeNo = database.SelectData(kubunNo_sql);
                    result.Deli_Type_No = 0;
                    if (deli_typeNo.Rows.Count > 0)
                    {
                        result.Deli_Type_No = (int)deli_typeNo.Rows[0]["Deli_TypeNo"];
                    }

                    // 納入先コードを取得する
                    string intoCD_sql = $@" SELECT T1.[into_cd], T1.[into_name],T1.[bin_no], T1.[work_time], T2.[address] FROM [m_into] AS T1"
                                         + " JOIN [m_address] as T2 ON T2.[address_no] = T1.[address_no]"
                                         + " WHERE T1.[depo_flg] = 1 AND (SUBSTRING(week,@pos,1) <> '0')";
                    var intoCd_data = database.SelectData(intoCD_sql);
                    if (intoCd_data.Rows.Count == 1)
                    {
                        var intoCd = intoCd_data.Rows[0]["into_cd"].ToString();
                        var item = new SelectListItem { Text = intoCd };
                        into_Cd_List.Add(item);
                        result.Bin_No = intoCd_data.Rows[0]["bin_no"].ToString();
                        result.Into_Name = intoCd_data.Rows[0]["into_name"].ToString();
                        result.Work_Time = (int)intoCd_data.Rows[0]["work_time"];
                        result.Into_Address = intoCd_data.Rows[0]["address"].ToString();
                        result.Leave_Time = leave_Time;
                    }
                    else
                    {
                        result.ErrorMessage = "対象日付の配送計画が存在しません。";
                        return result;
                    }

                    // トラックコードを取得する
                    string trackCD_sql = $@" SELECT DISTINCT [track_cd] as Track_Code 
                                            FROM [t_deli_car]
                                            where [deli_date] = @deli_date
                                            order by Track_Code asc";
                    var trackCd_data = database.SelectData(trackCD_sql);
                    if (trackCd_data.Rows.Count == 0)
                    {
                        result.ErrorMessage = "対象日付の配送計画が存在しません。";
                        return result;
                    }
                    for (int i = 0; i < trackCd_data.Rows.Count; i++)
                    {
                        var data = trackCd_data.Rows[i]["Track_Code"].ToString();
                        var item = new SelectListItem { Text = data };
                        track_Cd_List.Add(item);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    result.ErrorMessage = ErrorMsg;
                    return result;
                }
            }
            result.Lst_Track_Cd = track_Cd_List;
            result.Lst_Into_Cd = into_Cd_List;
            return result;
        }
        // 配送区分が「休憩」のとき, 車両情報を取得する
        private HaisouKeikakuTsuikaModel GetHaisouKeikakuInForByHaisouKubun2(HaisouKeikakuTsuikaModel model)
        {
            HaisouKeikakuTsuikaModel result = new HaisouKeikakuTsuikaModel();
            var track_Cd_List = new List<SelectListItem>();
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    database.SetTransaction();
                    //パラメータ必須-------------------------------------------------------------
                    database.SetParam("@deli_date", SqlDbType.Date, model.Deli_Date); //配送日付
                    database.SetParam("@kubun_Name", SqlDbType.NVarChar, model.HaisouKubun); //配送区分
                    //---------------------------------------------------------------------------

                    //SQLの準備                    
                    // トラックコードを取得する
                    string trackCD_sql = $@"
                                            SELECT DISTINCT [track_cd] as Track_Code 
                                            FROM [t_deli_car]
                                            where [deli_date] = @deli_date
                                            order by Track_Code asc";
                    var trackCd_data = database.SelectData(trackCD_sql);
                    if (trackCd_data.Rows.Count == 0)
                    {
                        result.ErrorMessage = "対象日付の配送計画が存在しません。";
                        return result;
                    }
                    for (int i = 0; i < trackCd_data.Rows.Count; i++)
                    {
                        var data = trackCd_data.Rows[i]["Track_Code"].ToString();
                        var item = new SelectListItem { Text = data };
                        track_Cd_List.Add(item);
                    }
                    //配送区分Noを取得する
                    string kubunNo_sql = $@" SELECT [deli_division_no] as Deli_TypeNo FROM [m_deli_division] WHERE [deli_division_name] =  @kubun_Name";
                    var deli_typeNo = database.SelectData(kubunNo_sql);
                    result.Deli_Type_No = 0;
                    if (deli_typeNo.Rows.Count > 0)
                    {
                        result.Deli_Type_No = (int)deli_typeNo.Rows[0]["Deli_TypeNo"];
                    }
                }
                catch
                {
                    result.ErrorMessage = ErrorMsg;
                    return result;
                }
            }
            result.Lst_Track_Cd = track_Cd_List;
            return result;
        }
        // 配送区分が「出荷」、「引取」、「引取納め(引取)」、「引取納め(納め)」のとき, 納入先情報を取得する
        private HaisouKeikakuTsuikaModel GetHaisouKeikakuInForByHaisouKubun3(HaisouKeikakuTsuikaModel model)
        {
            HaisouKeikakuTsuikaModel result = new HaisouKeikakuTsuikaModel();
            var into_Cd_List = new List<SelectListItem>();
            var pos = ConvertWeekInChartToInt(model.Deli_Date);
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    database.SetTransaction();

                    //SQLの準備
                    database.SetParam("@pos", SqlDbType.Int, pos); //Day Of Week
                    // 納入先コードを取得する
                    string intoCD_sql = $@" SELECT DISTINCT [into_cd] as Into_Code FROM [m_into]  
                                            WHERE (SUBSTRING(week,@pos,1) <> '0')
                                            order by Into_Code asc";
                    var intoCd_data = database.SelectData(intoCD_sql);

                    for (int i = 0; i < intoCd_data.Rows.Count; i++)
                    {
                        var data = intoCd_data.Rows[i]["Into_Code"].ToString();
                        var item = new SelectListItem { Text = data };
                        into_Cd_List.Add(item);
                    }
                }
                catch
                {
                    result.ErrorMessage = ErrorMsg;
                    return result;
                }
            }
            result.Lst_Into_Cd = into_Cd_List;
            return result;
        }

        // 配送日付,配送区分,納入先コード,便Noによって 納入先情報を取得する
        private HaisouKeikakuTsuikaModel GetHaisouInForBySearch2(HaisouKeikakuTsuikaModel model)
        {
            HaisouKeikakuTsuikaModel result = new HaisouKeikakuTsuikaModel();
            var pos = ConvertWeekInChartToInt(model.Deli_Date);
            var track_Cd_List = new List<SelectListItem>();
            
            using (MSSQLAccess database = new MSSQLAccess())
            {
                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    database.SetTransaction();
                    //パラメータ必須-------------------------------------------------------------
                    database.SetParam("@kubun_Name", SqlDbType.NVarChar, model.HaisouKubun); //配送区分
                    database.SetParam("@deli_date", SqlDbType.Date, model.Deli_Date); //配送日付
                    database.SetParam("@into_cd", SqlDbType.NVarChar, model.Into_Cd); //配送日付
                    database.SetParam("@bin_No", SqlDbType.NVarChar, model.Bin_No); //配送日付
                    database.SetParam("@pos", SqlDbType.Int, pos); //Day Of Week

                    //SQLの準備                    
                    //配送区分Noを取得する
                    string kubunNo_sql = $@" SELECT [deli_division_no] as Deli_TypeNo FROM [m_deli_division] WHERE [deli_division_name] =  @kubun_Name";
                    var deli_typeNo = database.SelectData(kubunNo_sql);
                    result.Deli_Type_No = 0;
                    if (deli_typeNo.Rows.Count > 0)
                    {
                        result.Deli_Type_No = (int)deli_typeNo.Rows[0]["Deli_TypeNo"];
                    }

                    // 納入先コードを取得する
                    string intoCD_sql = $@" SELECT T1.[into_name], T1.[work_time], T2.[address], T1.[available_car],T1.[start_time],T1.[end_time],T1.[time_flg] FROM [m_into] AS T1"
                                         + " JOIN [m_address] as T2 ON T2.[address_no] = T1.[address_no]"
                                         + " WHERE T1.[into_cd] = @into_cd AND T1.[bin_no] = @bin_No AND (SUBSTRING(week,@pos,1) <> '0')";
                    var intoCd_data = database.SelectData(intoCD_sql);
                    if (intoCd_data.Rows.Count == 1)
                    {
                        result.Into_Name = intoCd_data.Rows[0]["into_name"].ToString();
                        result.Work_Time = (int)intoCd_data.Rows[0]["work_time"];
                        result.Into_Address = intoCd_data.Rows[0]["address"].ToString();
                        result.Avalible_car = intoCd_data.Rows[0]["available_car"].ToString();
                        result.Assign_Start_Time = intoCd_data.Rows[0]["start_time"].ToString();
                        result.Assign_End_Time = intoCd_data.Rows[0]["end_time"].ToString();
                        result.Time_FLG = (int)intoCd_data.Rows[0]["time_flg"];
                    }
                    else
                    {
                        result.ErrorMessage = "対象日付の配送計画が存在しません。";
                        return result;
                    }

                    // トラックコードを取得する
                    string trackCD_sql = $@"
                                            SELECT DISTINCT [track_cd] as Track_Code 
                                            FROM [t_deli_car]
                                            where [deli_date] = @deli_date
                                            order by Track_Code asc";
                    var trackCd_data = database.SelectData(trackCD_sql);
                    if (trackCd_data.Rows.Count == 0)
                    {
                        result.ErrorMessage = "対象日付の配送計画が存在しません。";
                        return result;
                    }
                    for (int i = 0; i < trackCd_data.Rows.Count; i++)
                    {
                        var data = trackCd_data.Rows[i]["Track_Code"].ToString();
                        var item = new SelectListItem { Text = data };
                        track_Cd_List.Add(item);
                    }
                    //配送Noを取得する
                    string deli_plan_sql = $@" SELECT [deli_no] as Deli_No FROM [t_deli_plan] order by Deli_No desc";
                    var delo_No = database.SelectData(deli_plan_sql);
                    result.Deli_No = 0;
                    if (delo_No.Rows.Count > 0)
                    {
                        result.Deli_No = (int)delo_No.Rows[0]["Deli_No"] + 1;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    result.ErrorMessage = ErrorMsg;
                    return result;
                }
            }
            result.Lst_Track_Cd = track_Cd_List;
            return result;
        }
        [HttpPost]
        public IActionResult Add(HaisouKeikakuTsuikaModel model)
        {
            try
            {
                model.Deli_Date = GetDeliveryDate();
                model.Lst_HaisouKubun = GetHaisouKubun_List().Lst_HaisouKubun;
            }
            catch
            {
                ViewData["Error"] = ErrorMsg;
                return View("Index", new HaisouKeikakuTsuikaModel());
            }            
            using (MSSQLAccess database = new MSSQLAccess())
            {        

                try
                {
                    //パラメーターをクリアー
                    database.ClearParam();
                    //パラメータ必須-------------------------------------------------------------
                    database.SetTransaction();
                    //パラメータ必須[t_deli_plan]============================================
                    database.SetParam("@deli_date", SqlDbType.Date, model.Deli_Date != null ? model.Deli_Date : DateTime.Now);//配送日付
                    database.SetParam("@track_cd", SqlDbType.NVarChar, model.Track_Cd != null ? model.Track_Cd : "");//トラックコード
                    database.SetParam("@arrive_no", SqlDbType.Int, model.Arrive_No >= 0 ? model.Arrive_No : 0);//到着順
                    database.SetParam("@instruction_no", SqlDbType.NVarChar, model.Instruction_No != null ? model.Instruction_No : "---");//指示No
                    database.SetParam("@previous_flg", SqlDbType.Int, 0);//前回値フラグ
                    database.SetParam("@deli_type_no", SqlDbType.Int, model.Deli_Type_No >= 0 ? model.Deli_Type_No : 0);//配送区分
                    database.SetParam("@into_name", SqlDbType.NVarChar, model.Into_Name != null ? model.Into_Name : "---");//納入先名
                    database.SetParam("@into_address", SqlDbType.NVarChar, model.Into_Address != null ? model.Into_Address : "---");//納入先住所
                    database.SetParam("@arrive_time", SqlDbType.NVarChar, "---");//到着時間
                    database.SetParam("@leave_time", SqlDbType.NVarChar, model.Leave_Time != null ? model.Leave_Time : "---");//出発時間
                    database.SetParam("@work_time", SqlDbType.Int, model.Work_Time >= 0 ? model.Work_Time : 0);//納入先作業時間(分)
                    // 配送区分情報」の「配送区分名」によって質量をチェックする
                    if (model.HaisouKubun != null && (model.HaisouKubun == "出荷" || model.HaisouKubun == "引取納め（納め）"))
                    {
                        database.SetParam("@ship_weight", SqlDbType.Int, model.Shitsuryo >= 0 ? model.Shitsuryo : 0);//出荷質量
                        database.SetParam("@take_weight", SqlDbType.Int, 0);//引取質量
                    }
                    else if (model.HaisouKubun != null && (model.HaisouKubun == "引取" || model.HaisouKubun == "引取納め（引取）"))
                    {
                        database.SetParam("@ship_weight", SqlDbType.Int, 0);//出荷質量
                        database.SetParam("@take_weight", SqlDbType.Int, model.Shitsuryo >= 0 ? model.Shitsuryo : 0);//引取質量
                    }
                    else
                    {
                        database.SetParam("@ship_weight", SqlDbType.Int, 0);//出荷質量
                        database.SetParam("@take_weight", SqlDbType.Int, 0);//引取質量
                    }
                    database.SetParam("@available_weight", SqlDbType.Int, 0);//積載可能質量
                    database.SetParam("@take_plan_weight", SqlDbType.Int, 0);//引取予定質量
                    database.SetParam("@available_car", SqlDbType.NVarChar, model.Avalible_car != null ? model.Avalible_car : "---");//可能車両
                    database.SetParam("@assign_start_time", SqlDbType.NVarChar, model.Assign_Start_Time != null ? model.Assign_Start_Time : "---");//時間指定開始
                    database.SetParam("@assign_end_time", SqlDbType.NVarChar, model.Assign_End_Time != null ? model.Assign_End_Time :"---");//時間指定終了
                    database.SetParam("@area_name", SqlDbType.NVarChar, "手入力");//エリア名
                    database.SetParam("@deli_no", SqlDbType.Int, model.Deli_No >= 0 ? model.Deli_No : 0);//配送No
                    database.SetParam("@toll_road", SqlDbType.Int, model.Toll_Road);//有料道路使用
                    database.SetParam("@remark", SqlDbType.NVarChar, model.Remark != null ? model.Remark : "");//備考

                    string sql_add = $@"INSERT INTO [t_deli_plan] (
                                            deli_date,
                                            track_cd,
                                            arrive_no,
                                            instruction_no,
                                            previous_flg,
                                            deli_type_no,
                                            into_name,
                                            into_address,
                                            arrive_time,
                                            leave_time,
                                            work_time,
                                            ship_weight,
                                            take_weight,
                                            available_weight,
                                            take_plan_weight,
                                            available_car,
                                            assign_start_time,
                                            assign_end_time,
                                            area_name,
                                            deli_no,
                                            toll_road,
                                            remark)
                                    VALUES (
                                            @deli_date,
                                            @track_cd,
                                            @arrive_no,
                                            @instruction_no,
                                            @previous_flg,
                                            @deli_type_no,
                                            @into_name,
                                            @into_address,
                                            @arrive_time,
                                            @leave_time,
                                            @work_time,
                                            @ship_weight,
                                            @take_weight,
                                            @available_weight,
                                            @take_plan_weight,
                                            @available_car,
                                            @assign_start_time,
                                            @assign_end_time,
                                            @area_name,
                                            @deli_no,
                                            @toll_road,
                                            @remark)";
                    bool result_add = database.InsertData(sql_add);
                    database.CommitTransaction();

                    ViewData["Message"] = "配送計画追加が完了しました。";
                    return View("Index", model);
                }
                catch (Exception ex)
                {
                    var sqlException = ex as SqlException;
                    //transaction.Rollback();
                    if (sqlException.Number == 2627)
                    {
                        ViewData["Error"] = "配送計画情報を登録済みです。新規登録できません 。";　//配送計画情報の主キー重複時はデータ登録済みです
                    }
                    else
                    {
                        ViewData["Error"] = "エラーが発生しました。登録できません。";
                    }

                    return View("Index", model);
                }
                finally
                {
                    database.Dispose();
                }
            }
        }
        private int ConvertWeekInChartToInt(DateTime data)
        {
            //DayOfWeek: 日-0　月-1 火-2
            //var flag = 1 << ((7 - Convert.ToInt32(deliDate.DayOfWeek))%7);
            var pos = 7;
            switch (data.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    pos = 1;
                    break;
                case DayOfWeek.Tuesday:
                    pos = 2;
                    break;
                case DayOfWeek.Wednesday:
                    pos = 3;
                    break;
                case DayOfWeek.Thursday:
                    pos = 4;
                    break;
                case DayOfWeek.Friday:
                    pos = 5;
                    break;
                case DayOfWeek.Saturday:
                    pos = 6;
                    break;
            }
            return pos;
        }
    }
}
