﻿using Microsoft.AspNetCore.Mvc;
using RouteOptiWeb.Models;
using RouteOptiWeb.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Security.Claims;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using X.PagedList;
using Microsoft.AspNetCore.Http;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Text.Json;
using Microsoft.AspNetCore.Authorization;

namespace RouteOptiWeb.Controllers
{
    public class PlanController : Controller
    {
        private int iSeqId = 0;

        [HttpGet, HttpPost]
        public IActionResult Index(PlanModel model)
        {
            if (model == null)
            {
                model = new PlanModel();
            }
            iSeqId += 1;
            model.STATUS = Consts.C_JSON_OK;
            model.MESSAGE = string.Empty;
            model.SEQID = (iSeqId).ToString();
            model.PROGRESS = string.Empty;

            return View("Index", model);
        }

        [HttpGet, HttpPost]
        public IActionResult ResultOK(PlanModel model)
        {
            if (model == null)
            {
                model = new PlanModel();
            }

            model.STATUS = Consts.C_JSON_NG;
            return View("Result", model);
        }

        [HttpGet, HttpPost]
        public IActionResult ResultNG(PlanModel model)
        {
            if (model == null)
            {
                model = new PlanModel();
            }

            model.STATUS = Consts.C_JSON_NG;
            //model.ERR_MESSAGE = myString;
            return View("Result", model);
        }

        [HttpGet, HttpPost]
        public IActionResult DspList(PlanModel model)
        {
            if (model == null)
            {
                model = new PlanModel();
            }

            return View("DspList", model);
        }


        [HttpPost]
        public JsonResult GetProgress([FromBody] Dictionary<string, string> LstRecive)
        {
            ProgressModel progressModel = ProgressModel.GetInstance();
            string strSeqId = null;

            if (LstRecive.Count > 0) 
            {
                strSeqId = LstRecive[Consts.C_JSON_SEQID];
            }
            if (null == strSeqId || null == progressModel)
            {
                List<Dictionary<string, string>> parNG = new List<Dictionary<string, string>>();
                parNG.Add(new Dictionary<string, string>() { { Consts.C_JSON_STATUS, Consts.C_JSON_NG } });
                parNG.Add(new Dictionary<string, string>() { { Consts.C_JSON_MESSAGE, Consts.C_JSON_NG_CONTINUE } });
                parNG.Add(new Dictionary<string, string>() { { Consts.C_JSON_PROGRESS, "0" } });
                string jsonNG = JsonSerializer.Serialize(parNG);
                return Json(jsonNG);
            }


            List<Dictionary<string, string>> par = new List<Dictionary<string, string>>();
            par.Add(new Dictionary<string, string>() { { Consts.C_JSON_STATUS, progressModel.getStatus(strSeqId) } });
            par.Add(new Dictionary<string, string>() { { Consts.C_JSON_MESSAGE, progressModel.getMessage(strSeqId) } });
            par.Add(new Dictionary<string, string>() { { Consts.C_JSON_PROGRESS, progressModel.getProgress(strSeqId) } });
            string strJson = JsonSerializer.Serialize(par);

            return Json(strJson);
        }

        //計画作成
        public ActionResult Create(string strSeqId)
        {
            try
            {
                Dictionary<string, string> LstRecive = new Dictionary<string, string>();
                LstRecive.Add(Consts.C_JSON_SEQID, strSeqId);
                LstRecive.Add(Consts.C_JSON_STATUS, Consts.C_JSON_OK);
                LstRecive.Add(Consts.C_JSON_MESSAGE, "");
                LstRecive.Add(Consts.C_JSON_PROGRESS, "0");

                ProgressModel.GetInstance().setData(strSeqId, LstRecive);

                string myPythonApp = @"wwwroot\python\CVRP.py";
                //string myPythonApp = @"wwwroot\python\RouteOptiSample.py";
                Process myProcess = new Process()
                {
                    StartInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\python.exe")
                    {
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        Arguments = myPythonApp + " " + strSeqId
                    }
                };

                myProcess.Start();
                StreamReader myStreamReader = myProcess.StandardOutput;
                string myString = myStreamReader.ReadLine();
                myProcess.WaitForExit();
                myProcess.Close();

                Console.WriteLine("Value received from script: " + myString);

                if (myString.Equals(Consts.C_PYTHON_COMPLETE))
                {
                    return RedirectToAction("ResultOK");
                }
                else
                {
                    return RedirectToAction("ResultNG");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return RedirectToAction("ResultNG");
            }
        }

        public ActionResult DspList()
        {
            return RedirectToAction("DspList");
        }
    }
}