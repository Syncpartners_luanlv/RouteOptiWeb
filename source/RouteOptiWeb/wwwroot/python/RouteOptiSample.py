
"""Vehicles Routing Problem (VRP) with Time Windows."""

from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
import datetime
import pandas as pd
import sys
import urllib
import requests
import geopy.distance
import numpy as np
import plotly.graph_objs as go
from decimal import Decimal, ROUND_HALF_UP
import os
import math
# -*- coding: utf-8 -*-
from math import *
import openpyxl
from openpyxl.utils import column_index_from_string
from openpyxl.styles import PatternFill

########### SETTING ############################################

min_time=0
max_time=1440
time_out=10   # 検索時間timeout
CUL_KM_H=30   # 距離→時間変換の時速(km/h)
LIFT_TIME=30  # 配送先作業時間(分)
PENALTY=1_000_000_000
wait_time=0
DEBUG=True
D_BAR="---"
CUTTING_NAME="__"
BASE_START_TIME=datetime.time(5, 0, 0, 0)

# ローカル検索オプション
#local_searc_type=routing_enums_pb2.LocalSearchMetaheuristic.GREEDY_DESCENT
#local_searc_type=routing_enums_pb2.LocalSearchMetaheuristic.AUTOMATIC
local_searc_type=routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH

# 第1のソリューション戦略
first_strategy = routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC
#first_strategy = routing_enums_pb2.FirstSolutionStrategy.AUTOMATIC
#first_strategy = routing_enums_pb2.FirstSolutionStrategy.PATH_MOST_CONSTRAINED_ARC

########### COMMON ############################################

#https://memopy.hatenadiary.jp/entry/2017/06/11/092554
COLORS  =['gainsboro', 'black','peach puff',
    'lemon chiffon',
    'lavender blush', 'misty rose', 'light grey', 'cornflower blue', 'dark slate blue',
    'slate blue', 'medium slate blue', 'medium blue', 'royal blue',  'blue',
    'dodger blue', 'deep sky blue', 'sky blue', 'light sky blue', 'light steel blue',
    'light blue', 'powder blue', 'pale turquoise', 'dark turquoise', 'medium turquoise', 'turquoise',
    'cyan', 'light cyan', 'cadet blue', 'medium aquamarine', 'aquamarine', 
    'dark sea green', 'sea green', 'medium sea green', 'light sea green', 'pale green', 'spring green',
    'lawn green', 'medium spring green', 'green yellow', 'lime green', 'yellow green',
    'forest green', 'olive drab', 'dark khaki', 'khaki', 'pale goldenrod', 'light goldenrod yellow',
    'light yellow', 'yellow', 'gold', 'light goldenrod', 'goldenrod', 'dark goldenrod', 'rosy brown',
    'indian red', 'saddle brown', 'sandy brown',
    'dark salmon', 'salmon', 'light salmon', 'orange', 'dark orange',
    'coral', 'light coral', 'tomato', 'orange red', 'red', 'hot pink', 'deep pink', 'pink', 'light pink',
    'pale violet red', 'maroon', 'medium violet red', 'violet red',
    'medium orchid', 'dark orchid', 'dark violet', 'blue violet', 'purple', 'medium purple',
    'thistle', 'snow2', 'snow3',
    'snow4', 'seashell2', 'seashell3', 'seashell4', 'AntiqueWhite1', 'AntiqueWhite2',
    'AntiqueWhite3', 'AntiqueWhite4', 'bisque2', 'bisque3', 'bisque4', 'PeachPuff2',
    'PeachPuff3', 'PeachPuff4', 'NavajoWhite2', 'NavajoWhite3', 'NavajoWhite4',
    'LemonChiffon2', 'LemonChiffon3', 'LemonChiffon4', 'cornsilk2', 'cornsilk3',
    'cornsilk4', 'ivory2', 'ivory3', 'ivory4', 'honeydew2', 'honeydew3', 'honeydew4',
    'LavenderBlush2', 'LavenderBlush3', 'LavenderBlush4', 'MistyRose2', 'MistyRose3',
    'MistyRose4', 'azure2', 'azure3', 'azure4', 'SlateBlue1', 'SlateBlue2', 'SlateBlue3',
    'SlateBlue4', 'RoyalBlue1', 'RoyalBlue2', 'RoyalBlue3', 'RoyalBlue4', 'blue2', 'blue4',
    'DodgerBlue2', 'DodgerBlue3', 'DodgerBlue4', 'SteelBlue1', 'SteelBlue2',
    'SteelBlue3', 'SteelBlue4', 'DeepSkyBlue2', 'DeepSkyBlue3', 'DeepSkyBlue4',
    'SkyBlue1', 'SkyBlue2', 'SkyBlue3', 'SkyBlue4', 'LightSkyBlue1', 'LightSkyBlue2',
    'LightSkyBlue3', 'LightSkyBlue4', 'SlateGray1', 'SlateGray2', 'SlateGray3',
    'SlateGray4', 'LightSteelBlue1', 'LightSteelBlue2', 'LightSteelBlue3',
    'LightSteelBlue4', 'LightBlue1', 'LightBlue2', 'LightBlue3', 'LightBlue4',
    'LightCyan2', 'LightCyan3', 'LightCyan4', 'PaleTurquoise1', 'PaleTurquoise2',
    'PaleTurquoise3', 'PaleTurquoise4', 'CadetBlue1', 'CadetBlue2', 'CadetBlue3',
    'CadetBlue4', 'turquoise1', 'turquoise2', 'turquoise3', 'turquoise4', 'cyan2', 'cyan3',
    'cyan4', 'DarkSlateGray1', 'DarkSlateGray2', 'DarkSlateGray3', 'DarkSlateGray4',
    'aquamarine2', 'aquamarine4', 'DarkSeaGreen1', 'DarkSeaGreen2', 'DarkSeaGreen3',
    'DarkSeaGreen4', 'SeaGreen1', 'SeaGreen2', 'SeaGreen3', 'PaleGreen1', 'PaleGreen2',
    'PaleGreen3', 'PaleGreen4', 'SpringGreen2', 'SpringGreen3', 'SpringGreen4',
    'green2', 'green3', 'green4', 'chartreuse2', 'chartreuse3', 'chartreuse4',
    'OliveDrab1', 'OliveDrab2', 'OliveDrab4', 'DarkOliveGreen1', 'DarkOliveGreen2',
    'DarkOliveGreen3', 'DarkOliveGreen4', 'khaki1', 'khaki2', 'khaki3', 'khaki4',
    'LightGoldenrod1', 'LightGoldenrod2', 'LightGoldenrod3', 'LightGoldenrod4',
    'LightYellow2', 'LightYellow3', 'LightYellow4', 'yellow2', 'yellow3', 'yellow4',
    'gold2', 'gold3', 'gold4', 'goldenrod1', 'goldenrod2', 'goldenrod3', 'goldenrod4',
    'DarkGoldenrod1', 'DarkGoldenrod2', 'DarkGoldenrod3', 'DarkGoldenrod4',
    'RosyBrown1', 'RosyBrown2', 'RosyBrown3', 'RosyBrown4', 'IndianRed1', 'IndianRed2',
    'IndianRed3', 'IndianRed4', 'sienna1', 'sienna2', 'sienna3', 'sienna4', 'burlywood1',
    'burlywood2', 'burlywood3', 'burlywood4', 'wheat1', 'wheat2', 'wheat3', 'wheat4', 'tan1',
    'tan2', 'tan4', 'chocolate1', 'chocolate2', 'chocolate3', 'firebrick1', 'firebrick2',
    'firebrick3', 'firebrick4', 'brown1', 'brown2', 'brown3', 'brown4', 'salmon1', 'salmon2',
    'salmon3', 'salmon4', 'LightSalmon2', 'LightSalmon3', 'LightSalmon4', 'orange2',
    'orange3', 'orange4', 'DarkOrange1', 'DarkOrange2', 'DarkOrange3', 'DarkOrange4',
    'coral1', 'coral2', 'coral3', 'coral4', 'tomato2', 'tomato3', 'tomato4', 'OrangeRed2',
    'OrangeRed3', 'OrangeRed4', 'red2', 'red3', 'red4', 'DeepPink2', 'DeepPink3', 'DeepPink4',
    'HotPink1', 'HotPink2', 'HotPink3', 'HotPink4', 'pink1', 'pink2', 'pink3', 'pink4',
    'LightPink1', 'LightPink2', 'LightPink3', 'LightPink4', 'PaleVioletRed1',
    'PaleVioletRed2', 'PaleVioletRed3', 'PaleVioletRed4', 'maroon1', 'maroon2',
    'maroon3', 'maroon4', 'VioletRed1', 'VioletRed2', 'VioletRed3', 'VioletRed4',
    'magenta2', 'magenta3', 'magenta4', 'orchid1', 'orchid2', 'orchid3', 'orchid4', 'plum1',
    'plum2', 'plum3', 'plum4', 'MediumOrchid1', 'MediumOrchid2', 'MediumOrchid3',
    'MediumOrchid4', 'DarkOrchid1', 'DarkOrchid2', 'DarkOrchid3', 'DarkOrchid4',
    'purple1', 'purple2', 'purple3', 'purple4', 'MediumPurple1', 'MediumPurple2',
    'MediumPurple3', 'MediumPurple4', 'thistle1', 'thistle2', 'thistle3', 'thistle4',
    'gray1', 'gray2', 'gray3', 'gray4', 'gray5', 'gray6', 'gray7', 'gray8', 'gray9', 'gray10',
    'gray11', 'gray12', 'gray13', 'gray14', 'gray15', 'gray16', 'gray17', 'gray18', 'gray19',
    'gray20', 'gray21', 'gray22', 'gray23', 'gray24', 'gray25', 'gray26', 'gray27', 'gray28',
    'gray29', 'gray30', 'gray31', 'gray32', 'gray33', 'gray34', 'gray35', 'gray36', 'gray37',
    'gray38', 'gray39', 'gray40', 'gray42', 'gray43', 'gray44', 'gray45', 'gray46', 'gray47',
    'gray48', 'gray49', 'gray50', 'gray51', 'gray52', 'gray53', 'gray54', 'gray55', 'gray56',
    'gray57', 'gray58', 'gray59', 'gray60', 'gray61', 'gray62', 'gray63', 'gray64', 'gray65',
    'gray66', 'gray67', 'gray68', 'gray69', 'gray70', 'gray71', 'gray72', 'gray73', 'gray74',
    'gray75', 'gray76', 'gray77', 'gray78', 'gray79', 'gray80', 'gray81', 'gray82', 'gray83',
    'gray84', 'gray85', 'gray86', 'gray87', 'gray88', 'gray89', 'gray90', 'gray91', 'gray92',
    'gray93', 'gray94', 'gray95', 'gray97', 'gray98', 'gray99']

def chgHourFromMin(min):
    strTime="%02d:%02d" % (min // 60, min % 60)
    return strTime

def chgHourFromStr(str):
    intTime=int(str[0:2])*60+int(str[3:5])
    return intTime

auto_opti_name='株式会社プロスチール(自動割付)'
depo_name='株式会社プロスチール'

EXCEL_TYPE=".xlsx"

total_map_drop=pd.DataFrame()
total_map_result=pd.DataFrame()
total_excel_car=pd.DataFrame()
total_excel_plan=pd.DataFrame()
df_car_ret=pd.DataFrame()

NORMAL_LOOP="NORMAL"
FINISH_LOOP="FINISH"

HEAVT_TABLE=[
                ['8tまで','8ｔ車'],
                ['12tまで','12ｔ車'],
                ['15tまで','15ｔ車'],
                ['全てOK（トレーラ含む）','トレーラー'],
                ]


AVAILABLE_HEAVY=[
                '8tまで',
                '12tまで',
                '15tまで',
                '全てOK（トレーラ含む）'
                ]

DF_HEAVY_TABLE = pd.DataFrame(HEAVT_TABLE)
DF_HEAVY_TABLE = DF_HEAVY_TABLE.rename(columns={0:'可能車両'})
DF_HEAVY_TABLE = DF_HEAVY_TABLE.rename(columns={1:'車格'})
#DF_HEAVY_TABLE.columns = {'0':'可能車両','1':'車格'}

#dataFrameInsert
def insert(df, index, value):
    # index 行目までを抽出し、その直後に行を追加する。
    df1 = df.iloc[:index].append(pd.Series(value, index=df.columns), ignore_index=True)
    # index + 1 行目以降を抽出する。
    df2 = df.iloc[index:]
    # 縦方向に結合する。
    #print("insert[Bef]\n",df)
    #print("insert rec\n",df2)
    df_concat = pd.concat((df1, df2)).reset_index(drop=True)
    #print("insert[Aft]\n",df_concat)

    return df_concat

def reMakeExcel(strFileName):
    wb = openpyxl.load_workbook(strFileName)
    sheet = wb.active
    sheet.freeze_panes = 'A2'
    sheet.auto_filter.ref = sheet.dimensions
    ## 背景色を変更_範囲指定
    #for rows in sheet['A1':'BX1']:
    #    for cell in rows:
    #        if sheet[cell.coordinate].value != '':
    #            cell.fill = PatternFill(patternType='solid',fgColor='D3D3D3',bgColor='D3D3D3')
    wb.save(strFileName)

########### MAIN ############################################
"""Solve the VRP with time windows."""
startime=datetime.datetime.now()
#print最大表示数設定
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
print("start time:", startime)
strStartdate = "%04d%02d%02d" % (startime.year, startime.month, startime.day)
strStartime = strStartdate+"%02d%02d%02d" % (startime.hour, startime.minute, startime.second)
MAIN_PATH="data"
DATE_PATH=MAIN_PATH + '\\' + strStartdate
if False==os.path.exists(MAIN_PATH):
    os.mkdir(MAIN_PATH)
if False==os.path.exists(DATE_PATH):
    os.mkdir(DATE_PATH)
FilePath=DATE_PATH+"\data"+strStartime

########### GET CAR FILE DATA ############################################
def get_Car_Data():
    df_car = pd.read_excel('みどり運輸車格一覧表.xlsx',sheet_name ='車格',dtype = {'トラックコード': str} )
    df_car['MinStartTime']=0
    df_car['MaxEndTime']=0
    df_car['CarCost']=0

    for iIndex in range(len(df_car)):
        df_car['MinStartTime'][iIndex] = df_car['開始時間(Min)'][iIndex].hour*60+df_car['開始時間(Min)'][iIndex].minute
        df_car['MaxEndTime'][iIndex] = df_car['終了時間(Max)'][iIndex].hour*60+df_car['終了時間(Max)'][iIndex].minute
        #df_car['CarCost'][iIndex] = df_car['車両コスト'][iIndex]

    return df_car

df_car_main = get_Car_Data()
df_car_min = df_car_main.groupby(['車格']).agg({'最大積載量': 'min','トラックコード': 'count'})
df_car_min = df_car_min.rename(columns={'最大積載量': '最大積載量(min)'})
df_car_min = df_car_min.rename(columns={'トラックコード': 'count'})
DF_HEAVY_TABLE=DF_HEAVY_TABLE.merge(df_car_min, left_on=['車格'], right_on=['車格'], how='left', suffixes=['', '_right'])
DF_HEAVY_TABLE['仮合計回数'] = 0
DF_HEAVY_TABLE = DF_HEAVY_TABLE.reset_index()
print('!!!!!!!DF_HEAVY_TABLE!!!!!!!!\n',DF_HEAVY_TABLE)

########### GET POS #############
def get_lat_lon_from_address(df):
    df['緯度'] = 0.0
    df['経度'] = 0.0
    iIndex=0
    for Address in df['住所１']:
        makeUrl = "https://msearch.gsi.go.jp/address-search/AddressSearch?q="
        s_quote = urllib.parse.quote(Address)
        response = requests.get(makeUrl + s_quote)
        #print ("!!!!!GET TIME MATRIX!!!!!iIndex[",iIndex,"]経度[",response.json()[0]["geometry"]["coordinates"][0],"]緯度[",response.json()[0]["geometry"]["coordinates"][1])
        if response.json()[0]["geometry"]["coordinates"][0] is None:
            sys.exit ()
        df['経度'][iIndex]=response.json()[0]["geometry"]["coordinates"][0]
        df['緯度'][iIndex]=response.json()[0]["geometry"]["coordinates"][1]
        iIndex=iIndex+1
    #print (df)
    return df

########### GET DELI FILE DATA ############################################
def get_Data(df_car, lenData):
    df_base = pd.read_excel('配車テストデータ.xlsx',sheet_name ='出荷データ',dtype = {'トラック指定コード': str,'質量': int})
    #print("!!!!!!!df_base!!!!!!!\n",df_base)
    df_base['トラック指定コード']="0"
    df_base['重み']="0"
    
    df_buff = df_base.groupby(['住所１','時間指定開始','時間指定終了','引取・引取納め区分','トラック指定コード','指示№','可能車両'], dropna=False).agg({'納入先名': 'min', '質量': 'sum', '重み': 'max'})
    df_buff=df_buff.reset_index()
    df_buff = get_lat_lon_from_address(df_buff)
    df_buff = df_buff.groupby(['緯度','経度','時間指定開始','時間指定終了','引取・引取納め区分','トラック指定コード','指示№','可能車両'], dropna=False).agg({'住所１': 'min', '納入先名': 'min', '質量': 'sum', '重み': 'max'})
    #df_buff=pd.DataFrame(df_buff, columns=['住所１','時間指定開始','時間指定終了','引取・引取納め区分','納入先名','質量', '重み', 'トラック指定コード','指示№','可能車両'])
    df_buff = df_buff.reset_index()

    df_deli = pd.DataFrame({'住所１':['愛知県名古屋市緑区大根山1丁目1903番地'],'時間指定開始':[datetime.time(0, 0, 0, 0)],'時間指定終了':[datetime.time(23, 59, 59, 0)],'引取・引取納め区分':['デポ'],'納入先名':[depo_name],'質量':[0], '重み':[1], 'トラック指定コード':["0"],'指示№':['---'],'可能車両':['---']}) 
    df_deli = get_lat_lon_from_address(df_deli)
    df_deli = df_deli.append(df_buff)
    df_deli = df_deli.reset_index(drop=True)
    
    

    
    print("!!!!!!!df_deli!!!!!!!\n",df_deli)


    #for iIndex in range(len(df_car)):
    #    for iCnt in range(3):
    #        if df_car['MinStartReLoad'+str(iCnt+1)][iIndex]!=0 or df_car['MaxStartReLoad'+str(iCnt+1)][iIndex]!=0:
    #            dHeavy=1
    #            if(df_car['車格'][iIndex]=='トレーラー'):
    #                dHeavy=1
    #            df_depo = pd.DataFrame({'住所１':['愛知県名古屋市緑区大根山1丁目1903番地'],'時間指定開始':[df_car['再積込開始時間'+str(iCnt+1)+'(min)'][iIndex]],'時間指定終了':[df_car['再積込開始時間'+str(iCnt+1)+'(max)'][iIndex]],'引取・引取納め区分':['デポ'],'納入先名':[depo_name],'質量':[df_car['最大積載量'][iIndex]*(-1)], '重み':[dHeavy], 'トラック指定コード':[df_car['トラックコード'][iIndex]],'指示№':['---'],'可能車両':['---']}) 
    #            df_deli=df_deli.append(df_depo)
    #
    #    #for iCnt in range(5):
    #    #    dHeavy=0
    #    #    #if(df_car['車格'][iIndex]!='トレーラー') and iCnt<2:
    #    #    #    continue
    #    #    #if iCnt<2:
    #    #    #    #dHeavy=10
    #    #    #    dHeavy=10
    #    #    df_depo = pd.DataFrame({'住所１':['愛知県名古屋市緑区大根山1丁目1903番地'],'時間指定開始':[datetime.datetime(2999, 1, 1, 0, iCnt, 0, 0)],'時間指定終了':[datetime.datetime(2999, 1, 1, 23, 59, 59, 0)],'引取・引取納め区分':['デポ'],'納入先名':[auto_opti_name],'質量':[df_car['最大積載量'][iIndex]*(-1)], '重み':[dHeavy], 'トラック指定コード':[df_car['トラックコード'][iIndex]],'指示№':['---'],'可能車両':['---']}) 
    #    #    df_deli=df_deli.append(df_depo)

    df_Gr = df_deli.groupby(['緯度','経度','時間指定開始','時間指定終了','引取・引取納め区分','トラック指定コード'], dropna=False).agg({'質量': 'sum','指示№': 'count'})
    df_Gr= df_Gr.reset_index()

    print("!!!!!!!df_Gr1!!!!!!!\n",df_Gr)
    df_Gr = df_Gr.rename(columns={'質量': '質量Gr'})
    df_Gr = df_Gr.rename(columns={'指示№': 'GrCount'})
    df_Gr['GrNo']= df_Gr.index
    df_Gr['GrNo'].astype(int)
    print("!!!!!!!df_Gr2!!!!!!!\n",df_Gr)
    df_Gr=pd.DataFrame(df_Gr, columns=['緯度','経度','時間指定開始','時間指定終了','引取・引取納め区分','トラック指定コード','GrNo','質量Gr','GrCount'])

    df_deli=df_deli.merge(df_Gr, left_on=['緯度','経度','時間指定開始','時間指定終了','引取・引取納め区分','トラック指定コード']
                                 , right_on=['緯度','経度','時間指定開始','時間指定終了','引取・引取納め区分','トラック指定コード'], how='left', suffixes=['', '_right'])
    print ("!!!!!!!DF_HEAVY_TABLE!!!!!!\n", DF_HEAVY_TABLE)
    df_deli=df_deli.merge(DF_HEAVY_TABLE, left_on=['可能車両'], right_on=['可能車両'], how='left', suffixes=['', '_right'])
    #df_deli=df_deli.merge(df_car_min, left_on=['車格'], right_on=['車格'], how='left', suffixes=['', '_right'])
    print("!!!!!!!df_Gr3!!!!!!!\n",df_Gr)
    #df_deli=df_deli.reset_index(drop=True)
    df_deli['配送NoTemp']= df_deli.index
    df_deli['引取区分'] = df_deli['引取・引取納め区分'] #「・」が入っているとQueryができないので置き換え
    lenData=len(df_deli)

    #時間がnanだとGroupingできないのでここで値を設定
    df_deli_nan = df_deli.isnull()
    for iIndex in range(len(df_deli)):
        if df_deli_nan['時間指定開始'][iIndex]:
            #min使うためnullはMAX時間で仮置き
            df_deli['時間指定開始'][iIndex]=datetime.time(23, 59, 59, 0)
    depo_start=df_deli['時間指定開始'][0]
    df_deli['時間指定開始'][0]=datetime.time(23, 59, 59, 0)
    #min_datetime_start = datetime.datetime.combine(datetime.date.today(), min(df_deli['時間指定開始']))+datetime.timedelta(minutes=LIFT_TIME)
    #min_time_start=datetime.time(min_datetime_start.hour,min_datetime_start.minute,0,0)
    min_time_start=min(df_deli['時間指定開始'])
    df_deli['時間指定開始'][0]=depo_start
    for iIndex in range(len(df_deli)):
        if df_deli_nan['時間指定開始'][iIndex]:
            if "引取"==df_deli['引取区分'][iIndex]:
                # 引取は早朝帰り可
                #df_deli['時間指定開始'][iIndex]=min_time_start
                df_deli['時間指定開始'][iIndex]=BASE_START_TIME
            else:
                # その他は5時スタート
                df_deli['時間指定開始'][iIndex]=BASE_START_TIME
        if df_deli_nan['時間指定終了'][iIndex]:
            df_deli['時間指定終了'][iIndex]=datetime.time(23, 59, 59, 0)



    ##同じ納入先で指示Noで分かれてしまうと、時間指定等がない低優先対象の配送が分かれてしまうので、合体できるやつは合体する。
    #for iSummary in range(lenData):
    #    #if len(df_deli.index)>iSummary:
    #        iSummary=df_deli.index[iSummary]
    #        #合体可能
    #        if df_deli['GrCount'][iSummary] > 1 and df_deli['最大積載量(min)'][iSummary] > df_deli['質量Gr'][iSummary]:
    #            #対象取得し変換
    #            df_deli_hit = df_deli[df_deli['配送No']==df_deli['配送No'][iSummary]]
    #            df_deli_hit['質量'][iSummary]=df_deli_hit['質量Gr'][iSummary]
    #            #指示Noは他をつけて表示
    #            if "他" in df_deli_hit['指示№'][iSummary]:
    #                iCnt = int(df_deli_hit['指示№'][iSummary][-2])+1
    #                df_deli_hit['指示№'][iSummary]=df_deli_hit['指示№'][iSummary][:-3]+'他'+str(iCnt)+'件'
    #            else:
    #                df_deli_hit['指示№'][iSummary]=df_deli_hit['指示№'][iSummary]+'他1件'
    #            #対象をサマリー
    #            df_deli = df_deli[df_deli['GrNo']!=df_deli_hit['GrNo'][iSummary]]
    #            df_deli = pd.concat([df_deli, df_deli_hit])
    #
    #        ##1指示Noに対し、1回で回れない場合は分割する
    #        #if df_deli['最大積載量(min)'][iSummary] < df_deli['質量'][iSummary]:
    #        #   iLpCnt = math.ceil(df_deli['質量'][iSummary]/df_deli['最大積載量(min)'][iSummary])
    #        #   df_deli_part=pd.DataFrame()
    #        #   for iLp in range(iLpCnt-1):
    #        #       print ("!!!!!!!df_deli_part(Before)!!!!!!\n", df_deli)
    #        #       df_deli_part = df_deli[df_deli['配送No']==df_deli['配送No'][iSummary]]
    #        #       df_deli['質量'][iSummary]/=iLpCnt
    #        #       df_deli_part['質量'][iSummary]=df_deli['質量'][iSummary]
    #        #       df_deli = pd.concat([df_deli, df_deli_part])
    #        #       print ("!!!!!!!df_deli_part(After)!!!!!!\n", df_deli)
    #df_deli=df_deli.reset_index(drop=True)
    #df_deli['配送No']= df_deli.index
    #print ("!!!!!!!df_deli1!!!!!!\n", df_deli)
    
    if DEBUG:
        print ("!!!!!!!df_deli1!!!!!!\n", df_deli)
        df_Sum=df_deli.groupby('トラック指定コード').agg({'質量': 'sum'})
        print("!!!!!!合体前合計質量!!!!!!!![",df_Sum['質量'][0],"]件数[",len(df_deli))

    #同じ納入先で指示Noで分かれてしまうと、時間指定等がない低優先対象の配送が分かれてしまうので、合体できるやつは合体する。
    df_deli_Summary=df_deli
    df_deli['削除有無']=False
    for iSummary in range(lenData):
        #既に削除済みは対象外
        if df_deli['削除有無'][iSummary]==False:
            #合体可能か(1回でいけないのは対象外)
            if df_deli_Summary['GrCount'][iSummary] > 1 and df_deli_Summary['最大積載量(min)'][iSummary] > df_deli_Summary['質量'][iSummary]:
                iGrWeight = df_deli_Summary['質量'][iSummary]
                #対象取得Grを取得し合体
                df_deli_Gr = df_deli_Summary[df_deli_Summary['GrNo']==df_deli_Summary['GrNo'][iSummary]]
                df_deli_Gr = df_deli_Gr.sort_values(['配送NoTemp'])
                df_deli_Gr = df_deli_Gr.reset_index(drop=True)
                for iGr in range(len(df_deli_Gr)):
                    #自分対象外
                    if  df_deli_Summary['配送NoTemp'][iSummary]==df_deli_Gr['配送NoTemp'][iGr]:
                        continue

                    #条件違うのは一緒にしてはダメ
                    if  df_deli_Summary['時間指定開始'][iSummary]!=df_deli_Gr['時間指定開始'][iGr] \
                        or df_deli_Summary['時間指定終了'][iSummary]!=df_deli_Gr['時間指定終了'][iGr] \
                        or df_deli_Summary['引取区分'][iSummary]!=df_deli_Gr['引取区分'][iGr] \
                        or df_deli_Summary['トラック指定コード'][iSummary]!=df_deli_Gr['トラック指定コード'][iGr]:
                        continue

                    #可能車両のみが違う場合は指定の厳しい方に合わせる。
                    if df_deli_Summary['可能車両'][iSummary]!=df_deli_Gr['可能車両'][iGr] \
                       and df_deli_Summary['最大積載量(min)'][iSummary]>df_deli_Gr['最大積載量(min)'][iGr]:
                        continue
                       

                    ##引取・引取納め区分のみが違う場合で、引取と出荷の場合は合算する。
                    #strTradeType=df_deli_Summary['引取区分'][iSummary]
                    #strTradeDspType=df_deli_Summary['引取区分'][iSummary]
                    #if df_deli_Summary['引取区分'][iSummary]!=df_deli_Gr['引取区分'][iGr]:
                    #    if (df_deli_Summary['引取区分'][iSummary]=='出荷' and df_deli_Summary['引取区分'][iSummary]=='引取') \
                    #        or (df_deli_Summary['引取区分'][iSummary]=='引取' and df_deli_Summary['引取区分'][iSummary]=='出荷'):
                    #        if iGrWeight>0:
                    #            strTradeType=='出荷'
                    #        else:
                    #            strTradeType=='引取'
                    #        strTradeDspType='出荷/引取'
                    #    elif (df_deli_Summary['引取区分'][iSummary]=='出荷' and df_deli_Summary['引取区分'][iSummary]=='引取納め（引取）') \
                    #        or (df_deli_Summary['引取区分'][iSummary]=='引取納め（引取）' and df_deli_Summary['引取区分'][iSummary]=='出荷'):
                    #        if iGrWeight>0:
                    #            strTradeType=='出荷'
                    #        else:
                    #            strTradeType=='引取'
                    #        strTradeDspType='出荷/引取'
                    #    else:
                    #        continue


                    iGrWeight += df_deli_Gr['質量'][iGr]
                    if df_deli_Summary['最大積載量(min)'][iSummary] > iGrWeight:
                        iNo = int(df_deli_Gr['配送NoTemp'][iGr])
                        df_deli['削除有無'][iNo]=True
                        #print ("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n")
                        #print ("!!!!!!!合体元(消える方)!!!!!!\n", df_deli_Summary.loc[iNo])
                        #print ("!!!!!!!合体先合体前(質量増える方)!!!!!!\n", df_deli_Summary.loc[iSummary])
                        df_deli_Summary['質量'][iSummary]=iGrWeight
                        df_deli_Summary['指示№'][iSummary]+='/'+df_deli_Summary['指示№'][iNo]
                        #df_deli_Summary['引取区分'][iSummary]=strTradeType
                        #df_deli_Summary['引取・引取納め区分'][iSummary]=strTradeDspType
                        #print ("!!!!!!!合体先合体後(質量増える方)!!!!!!\n", df_deli_Summary.loc[iSummary])
                        #print ("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n")
                        df_deli_Summary = df_deli_Summary[df_deli['配送NoTemp']!=iNo]
                        #df_deli_Summary = df_deli_Summary.drop(iNo)
                    else:
                        iGrWeight -= df_deli_Gr['質量'][iGr]
    df_deli=df_deli_Summary.reset_index(drop=True)
    lenData=len(df_deli)

    if DEBUG:
        print ("!!!!!!!df_deli2!!!!!!\n", df_deli)
        df_Sum=df_deli.groupby('トラック指定コード').agg({'質量': 'sum'})
        print("!!!!!!合体後合計質量!!!!!!!![",df_Sum['質量'][0],"]len[",len(df_deli))

    #可能車両に対し、質量が入りきらない場合は分割する。
    #df_deli_Part=df_deli
    #for iSummary in range(lenData):
    #    #1指示Noに対し、1回で回れない場合は分割する
    #    if df_deli['最大積載量(min)'][iSummary] < df_deli['質量'][iSummary]:
    #        iLpCnt = math.ceil(df_deli['質量'][iSummary]/df_deli['最大積載量(min)'][iSummary])
    #        for iLp in range(iLpCnt-1):
    #            #print ("!!!!!!!df_deli_part(Before)!!!!!!\n", df_deli)
    #            df_deli_part = df_deli[df_deli['配送No']==df_deli['配送No'][iSummary]]
    #            df_deli['質量'][iSummary]/=iLpCnt
    #            df_deli_part['質量'][iSummary]=df_deli['質量'][iSummary]
    #            #指示Noは分割をつけて表示
    #            df_deli['指示№'][iSummary]=df_deli['指示№'][iSummary]+'分割'
    #            df_deli_part['指示№'][iSummary]=df_deli_part['指示№'][iSummary]+'分割'
    #            df_deli = pd.concat([df_deli, df_deli_part])
    #            #df_deli=df_deli.reset_index(drop=True)
    #            #print ("!!!!!!!df_deli_Part(After)!!!!!!\n", df_deli)
    #df_deli=df_deli.reset_index(drop=True)

    if DEBUG:
        print ("!!!!!!!df_deli3!!!!!!\n", df_deli)
        df_Sum=df_deli.groupby('トラック指定コード').agg({'質量': 'sum'})
        print("!!!!!!分解後合計質量!!!!!!!![",df_Sum['質量'][0],"]len[",len(df_deli))

    df_deli['開始分'] = 0
    df_deli['終了分'] = 0
    df_deli['質量abs'] = df_deli['質量']
    df_deli['仮重み'] = 0.0


    print ("!!!!!!!df_deli_nan!!!!!!\n", df_deli_nan)
    for iIndex in range(len(df_deli)):
        df_deli['開始分'][iIndex] = df_deli['時間指定開始'][iIndex].hour*60+df_deli['時間指定開始'][iIndex].minute
        df_deli['終了分'][iIndex] = df_deli['時間指定終了'][iIndex].hour*60+df_deli['時間指定終了'][iIndex].minute
        #出荷対応
        if "引取"==df_deli['引取区分'][iIndex] or "引取納め（引取）"==df_deli['引取区分'][iIndex]:
            df_deli['質量'][iIndex] = df_deli['質量'][iIndex] * -1

        if "デポ"==df_deli['引取区分'][iIndex]:
            df_deli['仮重み'][iIndex]=0
        #elif df_deli['可能車両'][iIndex]==AVAILABLE_HEAVY[0] and df_deli['終了分'][iIndex]<=780:
        #    df_deli['仮重み'][iIndex]=10
        #elif df_deli['可能車両'][iIndex]==AVAILABLE_HEAVY[0]:
        #    df_deli['仮重み'][iIndex]=3
        #elif df_deli['可能車両'][iIndex]==AVAILABLE_HEAVY[1] and df_deli['終了分'][iIndex]<=780:
        #    df_deli['仮重み'][iIndex]=7
        #elif df_deli['可能車両'][iIndex]==AVAILABLE_HEAVY[1]:
        #    df_deli['仮重み'][iIndex]=2
        #elif df_deli['可能車両'][iIndex]==AVAILABLE_HEAVY[2] and df_deli['終了分'][iIndex]<=780:
        #    df_deli['仮重み'][iIndex]=5
        #elif df_deli['可能車両'][iIndex]==AVAILABLE_HEAVY[2]:
        #    df_deli['仮重み'][iIndex]=1
        elif df_deli['終了分'][iIndex]<=60*6:
            df_deli['仮重み'][iIndex]=10
        elif df_deli['終了分'][iIndex]<=60*6.5:
            df_deli['仮重み'][iIndex]=9.5
        elif df_deli['終了分'][iIndex]<=60*7:
            df_deli['仮重み'][iIndex]=9
        elif df_deli['終了分'][iIndex]<=60*7.5:
            df_deli['仮重み'][iIndex]=8.5
        elif df_deli['終了分'][iIndex]<=60*8:
            df_deli['仮重み'][iIndex]=8
        elif df_deli['終了分'][iIndex]<=60*8.5:
            df_deli['仮重み'][iIndex]=7.5
        elif df_deli['終了分'][iIndex]<=60*9:
            df_deli['仮重み'][iIndex]=7
        elif df_deli['終了分'][iIndex]<=60*9.5:
            df_deli['仮重み'][iIndex]=6.5
        elif df_deli['終了分'][iIndex]<=60*10:
            df_deli['仮重み'][iIndex]=5
        elif df_deli['終了分'][iIndex]<=60*10.5:
            df_deli['仮重み'][iIndex]=4.5
        elif df_deli['終了分'][iIndex]<=60*11:
            df_deli['仮重み'][iIndex]=4
        elif df_deli['終了分'][iIndex]<=60*11.5:
            df_deli['仮重み'][iIndex]=3.5
        elif df_deli['終了分'][iIndex]<=60*12:
            df_deli['仮重み'][iIndex]=3
        elif df_deli['終了分'][iIndex]<=60*12.5:
            df_deli['仮重み'][iIndex]=2.5
        elif df_deli['終了分'][iIndex]<=60*13:
            df_deli['仮重み'][iIndex]=2
        elif df_deli['終了分'][iIndex]<=60*13.5:
            df_deli['仮重み'][iIndex]=1.5
        elif df_deli['終了分'][iIndex]<=60*14:
            df_deli['仮重み'][iIndex]=1
        else:
            df_deli['仮重み'][iIndex]=0.5
        if "引取"==df_deli['引取区分'][iIndex] or "引取納め（引取）"==df_deli['引取区分'][iIndex]:
            df_deli['仮重み'][iIndex]*=1.2

        #重いのは後で挽回しにくいので重みつける
        if df_deli['質量'][iIndex]>4000 and df_deli['可能車両'][iIndex]==DF_HEAVY_TABLE['可能車両'][0]:
            df_deli['仮重み'][iIndex]*=1.2
        elif df_deli['質量'][iIndex]>5000  and df_deli['可能車両'][iIndex]==DF_HEAVY_TABLE['可能車両'][0]:
            df_deli['仮重み'][iIndex]*=1.3
        elif df_deli['質量'][iIndex]>6000  and df_deli['可能車両'][iIndex]==DF_HEAVY_TABLE['可能車両'][0]:
            df_deli['仮重み'][iIndex]*=1.4
        elif df_deli['質量'][iIndex]>7000  and df_deli['可能車両'][iIndex]==DF_HEAVY_TABLE['可能車両'][0]:
            df_deli['仮重み'][iIndex]*=1.5
        elif df_deli['質量'][iIndex]>8000  and df_deli['可能車両'][iIndex]==DF_HEAVY_TABLE['可能車両'][0]:
            df_deli['仮重み'][iIndex]*=1.6
        elif df_deli['質量'][iIndex]>8000  and df_deli['可能車両'][iIndex]==DF_HEAVY_TABLE['可能車両'][1]:
            df_deli['仮重み'][iIndex]*=1.2
        elif df_deli['質量'][iIndex]>9000  and df_deli['可能車両'][iIndex]==DF_HEAVY_TABLE['可能車両'][1]:
            df_deli['仮重み'][iIndex]*=1.3
        elif df_deli['質量'][iIndex]>10000  and df_deli['可能車両'][iIndex]==DF_HEAVY_TABLE['可能車両'][1]:
            df_deli['仮重み'][iIndex]*=1.4
        elif df_deli['質量'][iIndex]>11000  and df_deli['可能車両'][iIndex]==DF_HEAVY_TABLE['可能車両'][1]:
            df_deli['仮重み'][iIndex]*=1.5

    #同じ場所の出荷→引取、出荷→引取納め（引取）は同じ車両とするためGrNoを設定する
    df_deli['SamePlaceFrom']=0
    df_deli['SamePlaceTo']=0
    iSamePlace=0
    for iFrom in range(len(df_deli)):
        if df_deli["引取区分"][iFrom]=='出荷':
            for iTo in range(len(df_deli)):
                if df_deli["緯度"][iFrom]==df_deli["緯度"][iTo] and df_deli["経度"][iFrom]==df_deli["経度"][iTo]:
                    if df_deli['引取区分'][iTo]=='引取' or df_deli['引取区分'][iTo]=='引取納め（引取）':
                        bHit=False
                        df_same_from=df_deli[df_deli['SamePlaceFrom']!=0].reset_index(drop=True)
                        #print('!!!!!!!!JUDGE SAMEPLACE!!!!!!!From[',iFrom,']To[',iTo,']iSamePlace[',iSamePlace)
                        #print('!!!!!!!!df_same_from!!!!!!!\n',df_same_from)
                        for iPickups in range(len(df_same_from)):
                            if df_deli["緯度"][iFrom]==df_same_from["緯度"][iPickups] and df_deli["経度"][iFrom]==df_same_from["経度"][iPickups]:
                                bHit=True
                                #print('!!!!!!!!NOTSAMEPLACE!!!!!!!From[',iFrom,']SamePlaceFrom[',df_same_from["SamePlaceFrom"][iPickups],']')
                        df_same_to=df_deli[df_deli['SamePlaceTo']!=0].reset_index(drop=True)
                        #print('!!!!!!!!df_same_to!!!!!!!\n',df_same_to)
                        for iPickups in range(len(df_same_to)):
                            if df_deli["緯度"][iTo]==df_same_to["緯度"][iPickups] and df_deli["経度"][iTo]==df_same_to["経度"][iPickups]:
                                bHit=True
                               # print('!!!!!!!!NOTSAMEPLACE!!!!!!!To[',iTo,']SamePlaceTo[',df_same_to["SamePlaceTo"][iPickups],']')
                        if False==bHit:
                            iSamePlace+=1
                            df_deli['SamePlaceFrom'][iFrom]=iSamePlace
                            df_deli['SamePlaceTo'][iTo]=iSamePlace
                            print('!!!!!!!!SAMEPLACE!!!!!!!From[',iFrom,']To[',iTo,']iSamePlace[',iSamePlace)

    #df_deli['質量'].astype(float) 
    df_deli['配送No']= df_deli.index   
    print("!!!!!!!!!!!df_deli(start)!!!!!!!!!!!\n",df_deli)

    #print (df_deli)
    return df_deli

lenData=0
df_deli_main = get_Data(df_car_main,lenData)


########### GET TIME MATRIX ############################################
# 楕円体
ELLIPSOID_GRS80 = 1 # GRS80
ELLIPSOID_WGS84 = 2 # WGS84

# 楕円体ごとの長軸半径と扁平率
GEODETIC_DATUM = {
    ELLIPSOID_GRS80: [
        6378137.0,         # [GRS80]長軸半径
        1 / 298.257222101, # [GRS80]扁平率
    ],
    ELLIPSOID_WGS84: [
        6378137.0,         # [WGS84]長軸半径
        1 / 298.257223563, # [WGS84]扁平率
    ],
}

# 反復計算の上限回数
ITERATION_LIMIT = 1000

'''
Vincenty法(逆解法)
2地点の座標(緯度経度)から、距離と方位角を計算する
:param lat1: 始点の緯度
:param lon1: 始点の経度
:param lat2: 終点の緯度
:param lon2: 終点の経度
:param ellipsoid: 楕円体
:return: 距離と方位角
'''
def vincenty_inverse(lat1, lon1, lat2, lon2, ellipsoid=None):

    # 差異が無ければ0.0を返す
    if isclose(lat1, lat2) and isclose(lon1, lon2):
        return {
            'distance': 0.0,
            'azimuth1': 0.0,
            'azimuth2': 0.0,
        }

    # 計算時に必要な長軸半径(a)と扁平率(ƒ)を定数から取得し、短軸半径(b)を算出する
    # 楕円体が未指定の場合はGRS80の値を用いる
    a, ƒ = GEODETIC_DATUM.get(ellipsoid, GEODETIC_DATUM.get(ELLIPSOID_GRS80))
    b = (1 - ƒ) * a

    φ1 = radians(lat1)
    φ2 = radians(lat2)
    λ1 = radians(lon1)
    λ2 = radians(lon2)

    # 更成緯度(補助球上の緯度)
    U1 = atan((1 - ƒ) * tan(φ1))
    U2 = atan((1 - ƒ) * tan(φ2))

    sinU1 = sin(U1)
    sinU2 = sin(U2)
    cosU1 = cos(U1)
    cosU2 = cos(U2)

    # 2点間の経度差
    L = λ2 - λ1

    # λをLで初期化
    λ = L

    # 以下の計算をλが収束するまで反復する
    # 地点によっては収束しないことがあり得るため、反復回数に上限を設ける
    for i in range(ITERATION_LIMIT):
        sinλ = sin(λ)
        cosλ = cos(λ)
        sinσ = sqrt((cosU2 * sinλ) ** 2 + (cosU1 * sinU2 - sinU1 * cosU2 * cosλ) ** 2)
        cosσ = sinU1 * sinU2 + cosU1 * cosU2 * cosλ
        σ = atan2(sinσ, cosσ)
        sinα = cosU1 * cosU2 * sinλ / sinσ
        cos2α = 1 - sinα ** 2
        cos2σm = cosσ - 2 * sinU1 * sinU2 / cos2α
        C = ƒ / 16 * cos2α * (4 + ƒ * (4 - 3 * cos2α))
        λʹ = λ
        λ = L + (1 - C) * ƒ * sinα * (σ + C * sinσ * (cos2σm + C * cosσ * (-1 + 2 * cos2σm ** 2)))

        # 偏差が.000000000001以下ならbreak
        if abs(λ - λʹ) <= 1e-12:
            break
    else:
        # 計算が収束しなかった場合はNoneを返す
        return None

    # λが所望の精度まで収束したら以下の計算を行う
    u2 = cos2α * (a ** 2 - b ** 2) / (b ** 2)
    A = 1 + u2 / 16384 * (4096 + u2 * (-768 + u2 * (320 - 175 * u2)))
    B = u2 / 1024 * (256 + u2 * (-128 + u2 * (74 - 47 * u2)))
    Δσ = B * sinσ * (cos2σm + B / 4 * (cosσ * (-1 + 2 * cos2σm ** 2) - B / 6 * cos2σm * (-3 + 4 * sinσ ** 2) * (-3 + 4 * cos2σm ** 2)))

    # 2点間の楕円体上の距離
    s = b * A * (σ - Δσ)

    # 各点における方位角
    α1 = atan2(cosU2 * sinλ, cosU1 * sinU2 - sinU1 * cosU2 * cosλ)
    α2 = atan2(cosU1 * sinλ, -sinU1 * cosU2 + cosU1 * sinU2 * cosλ) + pi

    if α1 < 0:
        α1 = α1 + pi * 2

    return {
        'distance': s,           # 距離
        'azimuth1': degrees(α1), # 方位角(始点→終点)
        'azimuth2': degrees(α2), # 方位角(終点→始点)
    }

######### Looping #####################################
def Looping(strArea, df, df_car,loopMode):
    global total_map_drop
    global total_map_result
    global total_excel_car
    global total_excel_plan
    global df_car_ret

    Vehicle_Cnt=len(df_car)
    data_distance=[]



    ########### DSP MAP ############################################
    def dsp_map(df_drop, result_table, loopMode):
        trace = []
        #DEPO
        df_depo=df.iloc[0:1]
        trace_route = go.Scattermapbox(
            lon = df_depo['経度'],
            lat = df_depo['緯度'],
            mode = 'markers + text + lines',
            text = df_depo['納入先名'] + '<br>',
            marker = dict(size=10),
            line = dict(color=COLORS[0]),
            name='デポ'
        )
        trace.append(trace_route)

        #DROP
        if len(df_drop)>0:
            trace_route = go.Scattermapbox(
            lon = df_drop['経度'],
            lat = df_drop['緯度'],
            mode = 'markers + text',
            text = df_drop['納入先名'] + '<br>'
                    '指示№: ' + df_drop['指示№'].astype(str) + '<br>' + 
                    '引取・引取納め区分: ' + df_drop['引取・引取納め区分'].astype(str) + '<br>' +
                    '配送質量[kg]: ' + df_drop['質量'].astype(str) + '<br>' + 
                    '時間指定開始: ' + df_drop['時間指定開始'].astype(str) + '<br>' + 
                    '時間指定終了: ' + df_drop['時間指定終了'].astype(str) + '<br>' + 
                    #'グループNo: ' + df_drop['GrNo'].astype(str) + '<br>' + 
                    #'配送グループ質量[kg]: ' + df_drop['質量Gr'].astype(str) + '<br>' + 
                    '可能車両: ' + df_drop['可能車両'].astype(str) + '<br>' +
                    '配送No: ' + df_drop['配送No'].astype(str),
                    #'トラック指定コード: ' + df_drop['トラック指定コード'].astype(str) + '<br>' + 
                    #'重み: ' + df_drop['重み'].astype(str),
            marker = dict(size=df_drop['円サイズ'],color=COLORS[1]),
            name='未割付:'+str(len(df_drop)) + '件'
            )
            trace.append(trace_route)

        #納入先
        for vehicle_id in range(Vehicle_Cnt):

            strQuery = "トラックコード=='" + df_car['トラックコード'][vehicle_id] +"'"
            route_v=result_table.query(strQuery)
            #if NORMAL_LOOP==dspMode and len(route_v)<=1:
            #    continue

            if len(route_v)>0:     
                route_v=route_v.append(route_v.iloc[0])
                #print("!!!!!!Scattermapbox!!!!!!!!!トラックコード[",df_car['トラックコード'][vehicle_id],"]vehicle_id[",vehicle_id,"]COLOR[",COLORS[vehicle_id+2])

                strCarName="[%s]最大積載量%skg車" % (df_car['トラックコード'][vehicle_id], df_car['最大積載量'][vehicle_id])
                trace_route = go.Scattermapbox(
                    lon = route_v['経度'],
                    lat = route_v['緯度'],
                    mode = 'markers + text + lines',
                    text = route_v['納入先名'] + '<br>' + 
                        '到着順: ' + route_v['到着順'].astype(str) + '<br>' + 
                        '指示№: ' + route_v['指示№'].astype(str) + '<br>' + 
                        '到着時間: ' + route_v['到着時間'].astype(str) + '<br>' + 
                        '出発時間: ' + route_v['出発時間'].astype(str) + '<br>' + 
                        '引取・引取納め区分: ' + route_v['引取・引取納め区分'].astype(str) + '<br>' + 
                        '配送質量[kg]: ' + route_v['配送質量[kg]'].astype(str) + '<br>' + 
                        '可能車両: ' + route_v['可能車両'].astype(str) + '<br>' + 
                        #'前地点出発時間: ' + route_v['前地点出発時間'].astype(str) + '<br>' + 
                        #'積載可能質量[kg]:' + route_v['積載可能質量[kg]'].astype(str) + '<br>' + 
                        #'累積時間: ' + route_v['累積時間'].astype(str) + '<br>' + 
                        #'累積直線距離[km]: ' + route_v['累積直線距離[km]'].astype(str) + '<br>' + 
                        #'グループNo: ' + route_v['グループNo'].astype(str) + '<br>' + 
                        #'配送グループ質量[kg]: ' + route_v['配送Gr質量[kg]'].astype(str) + '<br>' + 
                        '時間指定開始: ' + route_v['指定開始時間'].astype(str) + '<br>' + 
                        '時間指定終了: ' + route_v['指定終了時間'].astype(str) + '<br>' + 
                        '配送No: ' + route_v['配送No'].astype(str),
                    marker = dict(size=route_v['円サイズ'].astype(int)),
                    line = dict(color=COLORS[vehicle_id+2]),
                    name=strCarName
                )
                trace.append(trace_route)

        data2 = trace

        fig = go.Figure(data2)
        fig.update_layout(mapbox_style="open-street-map",
                          mapbox_center_lon=df['経度'][0],
                          mapbox_center_lat=df['緯度'][0],
                          mapbox={'zoom': 9},
                          margin={"r":0,"t":0,"l":0,"b":0},
        )
        #fig.show()

        return fig

    ###########最終的な結果を出力###########
    if loopMode=='DSP':
        df_final_drop=df_deli_main[df_deli_main['割付済']==0]
        df_final_drop = df_final_drop[df_final_drop['納入先名'] != depo_name]
        df_final_drop['円サイズ'] = abs(df_final_drop['質量']/100)
        df_final_drop.reset_index(drop=True)
        fig_fin=dsp_map(df_final_drop, total_map_result, loopMode)
        fig_fin.write_html("wwwroot\html\map.html")
        
        iMaxLen=len(total_excel_plan)
        #一度加工前で出しておく
        if iMaxLen>0:
            total_excel_plan = total_excel_plan.sort_values(['最大積載量','トラックコード', '出発時間'])
            total_excel_plan.to_excel(FilePath+"\配送計画情報(加工前)_"+strStartime+EXCEL_TYPE,encoding="shift jis", index = False)
        #加工し出力
        if iMaxLen>0:
            total_excel_plan = total_excel_plan.sort_values(['最大積載量','トラックコード', '出発時間'])
            total_excel_plan = total_excel_plan.reset_index(drop=True)
            chg_total_excel_plan=total_excel_plan

            
            strTrackCdBef=""
            strTrackCdNow=""
            strArriveTime=""
            serFristRec=[]
            iRec=-1       # レコード
            iSeqArrive=-1 # 到着順0origin
            while iRec < iMaxLen-1: #insertするのでiMaxLenが変わる
                iRec+=1
                iSeqArrive+=1
                strTrackCdNow=chg_total_excel_plan['トラックコード'][iRec]
                chg_total_excel_plan['到着順'][iRec]=str(iSeqArrive) # 到着順は累積時間順に再配置
            
                # エリア抽出
                idx = chg_total_excel_plan['エリア名'][iRec].find(CUTTING_NAME)  
                chg_total_excel_plan['エリア名'][iRec] = chg_total_excel_plan['エリア名'][iRec][idx+2:]
                
                if iRec == iMaxLen-1:
                    #最終レコード追加
                    chg_total_excel_plan = chg_total_excel_plan.append(serFristRec)
                    chg_total_excel_plan = chg_total_excel_plan.reset_index(drop=True)
                    #最終レコードデータ変換
                    chg_total_excel_plan['到着時間'][iRec+1]=strArriveTime
                    chg_total_excel_plan['出発時間'][iRec+1]=D_BAR
                    chg_total_excel_plan['到着順'][iRec+1]=str(iSeqArrive+1)
                    chg_total_excel_plan['エリア名'][iRec+1]=D_BAR
                    chg_total_excel_plan['配送質量[kg]'][iRec+1]=D_BAR
                elif strTrackCdNow!=strTrackCdBef:
                    strTrackCdBef=strTrackCdNow
                    
                    if iRec==0:
                        strArriveTime=chg_total_excel_plan['到着時間'][iRec]
                        chg_total_excel_plan['到着時間'][iRec]=D_BAR
                        chg_total_excel_plan['配送質量[kg]'][iRec]=D_BAR
                        chg_total_excel_plan['エリア名'][iRec]=D_BAR
                        serFristRec= chg_total_excel_plan.loc[iRec]
                    else:
                        
                        chg_total_excel_plan=insert(chg_total_excel_plan, iRec, serFristRec)
                        iMaxLen+=1
            
                        chg_total_excel_plan['到着時間'][iRec]=strArriveTime
                        chg_total_excel_plan['出発時間'][iRec]=D_BAR
                        chg_total_excel_plan['到着順'][iRec]=str(iSeqArrive)
                        chg_total_excel_plan['エリア名'][iRec]=D_BAR
                        chg_total_excel_plan['配送質量[kg]'][iRec]=D_BAR
                        serFristRec= chg_total_excel_plan.loc[iRec+1]
                        iSeqArrive=-1
            
                elif chg_total_excel_plan['配送No'][iRec]==0:
                    chg_total_excel_plan['配送質量[kg]'][iRec]=D_BAR
                    chg_total_excel_plan['エリア名'][iRec]=D_BAR
                    
                    if iSeqArrive==0:
                        strArriveTime=chg_total_excel_plan['到着時間'][iRec]
                        chg_total_excel_plan['到着時間'][iRec]=D_BAR
                    else:
                        strBuff=chg_total_excel_plan['到着時間'][iRec]
                        chg_total_excel_plan['到着時間'][iRec]=strArriveTime
                        strArriveTime = strBuff

            chg_total_excel_plan=pd.DataFrame(chg_total_excel_plan, columns=['トラックコード','最大積載量','到着順','指示№','引取・引取納め区分','納入先名','住所１'\
                                                                    ,'到着時間','出発時間','配送質量[kg]','可能車両'\
                                                                    ,'指定開始時間','指定終了時間','エリア名','配送No'])
            #chg_total_excel_plan = chg_total_excel_plan.sort_values(['最大積載量','トラックコード', '出発時間'])
            #chg_total_excel_plan = chg_total_excel_plan.reset_index(drop=True)

            strFileName=FilePath+"\配送計画情報_"+strStartime+EXCEL_TYPE
            chg_total_excel_plan.to_excel(strFileName,encoding="shift jis", index = False)
            reMakeExcel(strFileName)

        iMaxLen=len(total_excel_car)
        if iMaxLen>0:
            total_excel_car = total_excel_car.sort_values(['最大積載量','トラックコード', '出発時間'])
            total_excel_car.to_excel(FilePath+"\配車計画情報(加工前)_"+strStartime+EXCEL_TYPE,encoding="shift jis", index = False)
        if iMaxLen>0:
            total_excel_car=pd.DataFrame(total_excel_car, columns=['トラックコード','最大積載量'\
                                                                    ,'出発時間','到着時間','エリア名'\
                                                                    ])
            for iRec in range(len(total_excel_car)):

                # エリア抽出
                idx = total_excel_car['エリア名'][iRec].find(CUTTING_NAME)  
                total_excel_car['エリア名'][iRec] = total_excel_car['エリア名'][iRec][idx+2:]

            total_excel_car = total_excel_car.sort_values(['最大積載量','トラックコード', '出発時間'])
            strFileName=FilePath+"\配車計画情報_"+strStartime+EXCEL_TYPE
            total_excel_car.to_excel(strFileName,encoding="shift jis", index = False)
            reMakeExcel(strFileName)

        iMaxLen=len(df_final_drop)
        if iMaxLen>0:
            df_final_drop.to_excel(FilePath+"\未割付情報(加工前)_"+strStartime+EXCEL_TYPE,encoding="shift jis", index = False)
        if iMaxLen>0:
            df_final_drop=pd.DataFrame(df_final_drop, columns=['指示№'\
                                                                    ,'納入先名','住所１','時間指定開始','時間指定終了','引取・引取納め区分'\
                                                                    ,'質量','可能車両','配送No'])

            strFileName=FilePath+"\未割付情報_"+strStartime+EXCEL_TYPE
            df_final_drop.to_excel(strFileName,encoding="shift jis", index = False)
            reMakeExcel(strFileName)

        return df_car

    ########### CREATE DATA ############################################
    def create_data_model(df):
        """Stores the data for the problem."""
        data = {}
        data['time_windows'] = [df['開始分'].to_numpy (), df['終了分'].to_numpy ()]
        data['time_windows'] = np.array(data['time_windows']).T.tolist()

        data['time_matrix']=[]
        iIntervalTime=0 #depoからは0、その他はIntervalTimeつける
        iFromIdx=0
        for From_lat,From_lon,From_add,From_mode,From_SameNo  in zip(df['緯度'], df['経度'], df['納入先名'], df['引取区分'], df['SamePlaceFrom']):
            arrTime = []
            arrDis = []
            dis=0
            iToIdx=0
            for To_lat,To_lon,To_add,To_mode,To_SameNo in zip(df['緯度'], df['経度'], df['納入先名'], df['引取区分'], df['SamePlaceTo']):
                if iFromIdx==iToIdx:
                    dis=0
                    time=0
                elif "デポ"==From_mode and ("デポ"==To_mode or "入荷"==To_mode):
                    dis=PENALTY
                    time=PENALTY
                #elif ("引取"==From_mode or "引取納め（引取）"==From_mode or "引取納め（納め）"==From_mode) and "出荷"==To_mode and iToIdx!=0:
                elif ("引取"==From_mode or "引取納め（引取）"==From_mode) and "出荷"==To_mode and iToIdx!=0:
                    dis=PENALTY
                    time=PENALTY
                elif To_SameNo!=0 and To_SameNo!=From_SameNo:
                    #出荷→引取(納め引取)で同じ場所は出荷→引取(納め引取)になるようにペナルティを設定する
                    dis=PENALTY
                    time=PENALTY
                elif From_lat==To_lat and From_lon==To_lon:
                    dis=0
                    time=0
                else:
                    result = vincenty_inverse(From_lat, From_lon, To_lat, To_lon, 1)
                    dis=round(result['distance'], 3)
                    time=(((dis/1000)/CUL_KM_H)*60)+iIntervalTime
                arrTime.append(time)
                arrDis.append(dis)
                iToIdx+=1
                #print('%s→%s：dis[%s(m)]:time[%s(min)]' %(From_add, To_add, dis, time))
            data['time_matrix'].append(arrTime)
            data_distance.append(arrDis)
            iIntervalTime=LIFT_TIME
            iFromIdx+=1
        #print (data['time_matrix'])
        data['num_vehicles'] = Vehicle_Cnt
        data['depot'] = 0
        data['vehicle_max_distance'] = 30000
        

        data['pickups_deliveries'] = []                     
        #引取納め（引取）→引取納め（納め）は同じ車両とする。
        for iFrom in range(len(df)):
            if df["引取区分"][iFrom]=='引取納め（引取）':
                strSiji=df["指示№"][iFrom]
                for iTo in range(len(df)):
                    if df["引取区分"][iTo]=='引取納め（納め）' and strSiji== df["指示№"][iTo]:
                        data_take=[int(df.index[iFrom]),int(df.index[iTo]),0]
                        data['pickups_deliveries'].append(data_take)
        #同じ場所の出荷→引取、出荷→引取納め（引取）は同じ車両とする。
        #for iFrom in range(len(df)):
        #    if df["引取区分"][iFrom]=='出荷':
        #     for iTo in range(len(df)):
        #        if df["緯度"][iFrom]==df["緯度"][iTo] and df["経度"][iFrom]==df["経度"][iTo]:
        #            if df['引取区分'][iTo]=='引取' or df['引取区分'][iTo]=='引取納め（引取）':
        #                bHit=False
        #                for iPickups in range(len(data['pickups_deliveries'])):
        #                    if data['pickups_deliveries'][iPickups][0]==df.index[iFrom]:
        #                        bHit=True
        #                    elif data['pickups_deliveries'][iPickups][1]==df.index[iTo]:
        #                        bHit=True
        #                if False==bHit:
        #                    data_take=[int(df.index[iFrom]),int(df.index[iTo]),1]
        #                    data['pickups_deliveries'].append(data_take)   
        #
        print('!!!!!!!!time_matrix!!!!!!!\n',data['time_matrix'])

        return data

    #routing.NextVar(159).SetValues([159, 90])
    #routing.NextVar(90).SetValues([90, 128])
    #routing.NextVar(128).SetValues([128, 73])
    #routing.NextVar(73).SetValues([73, 54])
    #routing.NextVar(160).SetValues([160, 26]) 
    #routing.NextVar(160).SetValues([160, 26]) 
    #routing.NextVar(90).SetValues([90, 128]) 
    #routing.NextVar(128).SetValues([128, 91]) 
    #routing.NextVar(160).SetValues([159, 90]) 

    # Instantiate the data problem.
    data = create_data_model(df)

    ########### DATA CHACK ############################################
    def chkData():
        for iIndex in range(len(df)):
            if data['time_windows'][iIndex][0]+data['time_matrix'][iIndex][0]>max_time:
                if df['引取区分'][iIndex]!='デポ':
                    print('chkDataErr!!!iIndex:',iIndex)
                    return False
        return True

    # Instantiate the data problem.
    datechk = chkData()
    if False==datechk:
        sys.exit ()

    ########### SET ROUTING ############################################
    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(len(data['time_matrix']),
                                            data['num_vehicles'], data['depot'])

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)

    ########### TIME ############################################
    # Create and register a transit callback.
    def time_callback(from_index, to_index):
        """Returns the travel time between the two nodes."""
        # Convert from routing variable Index to time matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['time_matrix'][from_node][to_node]

    #transit_callback_index=[]
    #for vehicleId in range(len(df_car)):
    #    transit_callback_index.append(routing.RegisterTransitCallback(time_callback))
    transit_callback_index = routing.RegisterTransitCallback(time_callback)

    # Add Time Windows constraint.
    time = 'Time'
    #routing.AddDimensionWithVehicleTransitAndCapacity(
    routing.AddDimension(
        transit_callback_index,
        wait_time,
        #df_car['MaxEndTime'],# vehicle end time
        max_time,
        False,
        time)
    time_dimension = routing.GetDimensionOrDie(time)
    #routing.AddDimension(
    #    transit_callback_index,
    #    wait_time,  # allow waiting time
    #    max_time,  # maximum time per vehicle
    #    False,  # Don't force start cumul to zero.
    #    time)
    #time_dimension = routing.GetDimensionOrDie(time)
    # Add time window constraints for each location except depot.
    for location_idx, time_window in enumerate(data['time_windows']):
        if location_idx == data['depot']:
            continue
        index = manager.NodeToIndex(location_idx)
        time_dimension.CumulVar(index).SetRange(time_window[0], time_window[1])
    # Add time window constraints for each vehicle start node.
    #depot_idx = data['depot']
    #for vehicle_id in range(data['num_vehicles']):
    #    index = routing.Start(vehicle_id)
    #    time_dimension.CumulVar(index).SetRange(
    #        data['time_windows'][depot_idx][0],
    #        data['time_windows'][depot_idx][1])

    # Instantiate route start and end times to produce feasible times.
    for i in range(data['num_vehicles']):
        routing.AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(routing.Start(i)))
        routing.AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(routing.End(i)))
        
    # vehicle start time
    for vehicleId in range(len(df_car)):
        #ガード処理
        #if df_car['MaxStartTime'][vehicleId]<df_car['MinStartTime'][vehicleId]:
        #    df_car['MinStartTime'][vehicleId]=df_car['MaxStartTime'][vehicleId]
        #if df_car['MaxEndTime'][vehicleId]<df_car['MinEndTime'][vehicleId]:
        #    df_car['MinEndTime'][vehicleId]=df_car['MaxEndTime'][vehicleId]

        index = routing.Start(vehicleId)
        time_dimension.CumulVar(index).SetRange(int(df_car['MinStartTime'][vehicleId]),
                                                int(df_car['MaxEndTime'][vehicleId]))
        index = routing.End(vehicleId)
        time_dimension.CumulVar(index).SetRange(int(df_car['MinStartTime'][vehicleId]),
                                                int(df_car['MaxEndTime'][vehicleId]))


    # Instantiate route start and end times to produce feasible times.
    for i in range(data['num_vehicles']):
        totalDurationExpression = time_dimension.CumulVar(routing.End(i)) - time_dimension.CumulVar(routing.Start(i))
        #routing.solver().Add(totalDurationExpression <= int(df_car['MaxWorkingHours'][i]))
        #routing.solver().Add(totalDurationExpression >= int(df_car['MinWorkingHours'][i]))

    ########### CAPACITY ############################################

    # 配送量のcallbackを作成、登録
    def demand_callback(index):
        """Returns the demand of the node."""
        node = manager.IndexToNode(index)
        return df['質量'][node]
        #return 30
    
    demand_callback_index = routing.RegisterUnaryTransitCallback(demand_callback)

    iMinusWeight=0
    #for iAvailable in range(len(DF_HEAVY_TABLE)):
    #    if DF_HEAVY_TABLE['可能車両'][iAvailable] in strArea:
    #        iMinusWeight = int(DF_HEAVY_TABLE['最大積載量(min)'][iAvailable])
    #iMinusWeight=int(DF_HEAVY_TABLE['最大積載量(min)'][0])
    # 積載量の制約
    routing.AddDimensionWithVehicleCapacity(
        demand_callback_index,
        #0x7FFFFFFFFFFFFFF,  # null capacity slack  
        iMinusWeight,  # null capacity slack  
        df_car['最大積載量'],  # vehicle maximum capacities
        False,  # start cumul to zero
        'Capacity')
    capacity = routing.GetDimensionOrDie('Capacity')

    ########### ASSIGN ############################################
    # assign track
    for node in range(1, len(df)):
        if not df['トラック指定コード'][node] is None:
            for vehicleId in range(len(df_car)):
                if (df_car['トラックコード'][vehicleId] == df['トラック指定コード'][node]):
                    index = manager.NodeToIndex(node)
                    routing.VehicleVar(index).SetValues([-1, vehicleId])
                    #print("node:", node , "index:" , index ,"vehicleId:",vehicleId,"トラックコード:",df_car['トラックコード'][vehicleId],"納入先名:",df['納入先名'][node],"MinStartTime:",df_car['MinStartTime'][vehicleId],"MaxStartTime:",df_car['MaxStartTime'][vehicleId])


    ############ SAME PLACE ############################################
    ##df_same_place = df.sort_values(['緯度', '経度'])
    #for iIdx1 in range(len(df)):
    #    for iIdx2 in range(len(df)):
    #        if iIdx1<=iIdx2:
    #            break
    #        if df['緯度'][iIdx1]==df['緯度'][iIdx2] and df['経度'][iIdx1]==df['経度'][iIdx2] and int(df['質量Gr'][iIdx1])<8200:
    #            pickup_index = manager.NodeToIndex(iIdx1)
    #            delivery_index = manager.NodeToIndex(iIdx2)
    #            routing.AddPickupAndDelivery(pickup_index, delivery_index)
    #            routing.solver().Add(
    #                routing.VehicleVar(pickup_index) == routing.VehicleVar(
    #                    delivery_index))
    #            routing.solver().Add(
    #                time_dimension.CumulVar(pickup_index) <=
    #                time_dimension.CumulVar(delivery_index))
    #            routing.NextVar(pickup_index).SetValues([pickup_index, delivery_index]) 
    #

    ########### PICKUP ############################################
    for request in data['pickups_deliveries']:
        pickup_index = manager.NodeToIndex(request[0])
        delivery_index = manager.NodeToIndex(request[1])
        #pickup_index = request[0]
        #delivery_index = request[1]
        routing.AddPickupAndDelivery(pickup_index, delivery_index)
        routing.solver().Add(
            routing.VehicleVar(pickup_index) == routing.VehicleVar(
                delivery_index))
        routing.solver().Add(
            time_dimension.CumulVar(pickup_index) <=
            time_dimension.CumulVar(delivery_index))
        #if request[2]==1:
        #    routing.NextVar(pickup_index).SetValues([pickup_index, delivery_index]) 
        print('!!!!!!!!PICKUP!!!!!!!request0[',request[0],']request1[',request[1],']')
        print('!!!!!!!!PICKUP!!!!!!!From[',pickup_index,']To[',delivery_index,']')

    ########### LOCK     ############################################

    #routing.NextVar(142).SetValues([142, 26])
    #routing.NextVar(159).SetValues([159, 90])
    #routing.NextVar(90).SetValues([90, 128])
    #routing.NextVar(128).SetValues([128, 73])
    #routing.NextVar(73).SetValues([73, 54])
    #routing.NextVar(160).SetValues([160, 26]) 
    #routing.NextVar(160).SetValues([160, 26]) 
    #routing.NextVar(90).SetValues([90, 128]) 
    #routing.NextVar(128).SetValues([128, 91]) 
    #routing.NextVar(160).SetValues([159, 90]) 



    ########### COST ############################################
    # Allow to drop nodes
    for index in range(1, len(data['time_matrix'])):
        node = manager.NodeToIndex(index)
        pena=0
        heavy=df['重み'][node]
        #if df['引取・引取納め区分'][index]=='デポ' and node!=0:
        if heavy==99:
            pena=0x7FFFFFFFFFFFFFF
            #pena=-1
            #pena=1_000_000_000_000
            #pena=sys.maxsize
            #pena=df['重み'][node]*1000
        else:
            #pena=abs((heavy*df['質量'][node]*10)**3)
            if heavy>10:
                heavy=10
            pena=(heavy*50)**5
            #pena=heavy
            #pena=0
        #pena=df['重み'][node]**5*1000000
        #pena=df['質量'][node]*1000
        routing.AddDisjunction([manager.NodeToIndex(index)], int(pena))
        #print("node:", node , "index:" , index ,"pena:",pena,"納入先名:",df['納入先名'][node],"時間指定開始:",df['時間指定開始'][node])

    #for index in range(1, len(data['time_matrix'])):
    #    node = manager.NodeToIndex(index)
    #    pena=0
    #    heavy=df['重み'][node]
    #    if heavy==10:
    #        pena=0x7FFFFFFFFFFFFFF
    #        #pena=-1
    #        routing.AddDisjunction([manager.NodeToIndex(index)], int(pena))
    #    else:
    #
    #        pena=0
    #        routing.AddDisjunction([manager.NodeToIndex(index)], int(pena))
    #    
    #    print("node:", node , "index:" , index ,"pena:",pena,"納入先名:",df['納入先名'][node],"時間指定開始:",df['時間指定開始'][node])

    #for vehicleId in range(len(df_car)):
    #    #routing.SetArcCostEvaluatorOfVehicle(transit_callback_index[vehicleId], vehicleId)
    #    routing.SetArcCostEvaluatorOfVehicle(0, vehicleId)

    ## Define cost of each arc.
    #def cost_capa_callback(index):
    #    return 1/df['質量'][node]*100000000
    #
    #cost_capa_callback_index = routing.RegisterUnaryTransitCallback(cost_capa_callback)
    #routing.SetArcCostEvaluatorOfAllVehicles(cost_capa_callback_index)
    ##time_dimension.SetGlobalSpanCostCoefficient(10000000000)
    #capacity.SetGlobalSpanCostCoefficient(0)

    # Define cost of each arc.
    def cost_callback(from_index, to_index):
        #from_node = manager.IndexToNode(from_index)
        #to_node = manager.IndexToNode(to_index)
        #num=1
        ##if 'デポ'==df['引取・引取納め区分'][to_node] and data['depot']!=to_node and 'デポ'!=df['引取・引取納め区分'][from_node]:
        #num=1+df['重み'][to_node]*0.1
        #return data['time_matrix'][from_node][to_node]/2
        #bHit=False
        #for iLp in range(len(df_car)):
        #    if 0!=df_car['引取エリア'][iLp]:
        #        bHit=True
        #
        #if loopMode=='Last' or bHit==True:
        #    iMultiple=0.1
        #else:
        #    iMultiple=10
        iMultiple=1
        return time_callback(from_index, to_index)*iMultiple

    cost_callback_index = routing.RegisterTransitCallback(cost_callback)
    routing.SetArcCostEvaluatorOfAllVehicles(cost_callback_index)
    #time_dimension.SetGlobalSpanCostCoefficient(10)
    #
    #for vehicleId in range(len(df_car)):
        #routing.SetSpanCostCoefficientForVehicle(1,vehicleId)

    #TrackCost
    #trackCost=0.2
    #trackCost=trackCost*trackCost*trackCost*1000
    #for vehicleId in range(len(df_car)):
    #    routing.SetFixedCostOfVehicle(int(trackCost), int(vehicleId))


    for vehicleId in range(len(df_car)):
        #df_group=df.groupby(['住所１','開始分','終了分','引取・引取納め区分','トラック指定コード']).agg({'納入先名': 'min', '質量': 'sum', '重み': 'max'})
        #strQuery = "質量>=" + str(df_car['最大積載量'][vehicleId])
        #df_group_count=df_group.query(strQuery)
        #df_car['CarCost']={}
        heavy=df_car['CarCost'][vehicleId]
        #if df['引取・引取納め区分'][index]=='デポ' and node!=0:
        #if heavy==99 or len(df_group_count)<=0:
        #if loopMode=='Last':
        #    heavy=0
        if heavy==99:
            pena=0x7FFFFFFFFFFFFFF
            #pena=-1
            #pena=1_000_000_000_000
            #pena=sys.maxsize
            #pena=df['重み'][node]*1000
        else:
            #pena=abs((heavy*df['質量'][node]*10)**3)
            pena=(heavy*50)**5
        routing.SetFixedCostOfVehicle(int(pena), int(vehicleId))
        #routing.SetFixedCostOfVehicle(0, int(vehicleId))
        #carcost=1/df_car['最大積載量'][vehicleId]*100000000
        #carcost=df_car['最大積載量'][vehicleId]*10000
        #routing.SetFixedCostOfVehicle(int(carcost), int(vehicleId))


    ## Create and register a transit callback.
    #def distance_callback(from_index, to_index):
    #    """Returns the distance between the two nodes."""
    #    # Convert from routing variable Index to distance matrix NodeIndex.
    #    from_node = manager.IndexToNode(from_index)
    #    to_node = manager.IndexToNode(to_index)
    #    return data_distance[from_node][to_node]
    #
    #distance_callback_index = routing.RegisterTransitCallback(distance_callback)
    #
    ## Define cost of each arc.
    #routing.SetArcCostEvaluatorOfAllVehicles(distance_callback_index)
    #    
    #distance_callback_index = routing.RegisterTransitCallback(distance_callback_index)
    #distance = 'Distance'
    #routing.AddDimension(
    #    distance_callback_index,
    #    0,  # null slack
    #    data['vehicle_max_distance'],  # maximum distance per vehicle
    #    True,  # start cumul to zero
    #    distance)
    #distance_dimension = routing.GetDimensionOrDie(distance)
    ## Try to minimize the max distance among vehicles.
    ## /!\ It doesn't mean the standard deviation is minimized
    #distance_dimension.SetGlobalSpanCostCoefficient(100)

    ########### SEARCH TYPE ############################################
    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (first_strategy)

    # 検索戦略の変更
    #search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.local_search_metaheuristic = (local_searc_type)
    search_parameters.time_limit.seconds = time_out
    #search_parameters.log_search = True

    ########### SOLVE ############################################
    print("AreaName:", strArea)
    print("start solve time:", datetime.datetime.now())

    # Solve the problem.
    solution = routing.SolveWithParameters(search_parameters)

    ########### PRINT ############################################

    def print_solution(data, manager, routing, solution):
        """Prints solution on console."""
        print(f'Objective: {solution.ObjectiveValue()}')
        # Display dropped nodes.
        dropped_nodes = 'Dropped nodes:'
        for node in range(routing.Size()):
            if routing.IsStart(node) or routing.IsEnd(node):
                continue
            if solution.Value(routing.NextVar(node)) == node:
                dropped_nodes += ' {}'.format(manager.IndexToNode(node))
        print(dropped_nodes)
        # Display routes
        time_dimension = routing.GetDimensionOrDie('Time')
        total_time = 0
        for vehicle_id in range(data['num_vehicles']):
            index = routing.Start(vehicle_id)
            plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
            while not routing.IsEnd(index):
                time_var = time_dimension.CumulVar(index)
                plan_output += '{0} Time({1},{2}) -> '.format(
                    manager.IndexToNode(index), solution.Min(time_var),
                    solution.Max(time_var))
                index = solution.Value(routing.NextVar(index))
            time_var = time_dimension.CumulVar(index)
            plan_output += '{0} Time({1},{2})\n'.format(manager.IndexToNode(index),
                                                        solution.Min(time_var),
                                                        solution.Max(time_var))
            plan_output += 'Time of the route: {}min\n'.format(
                solution.Min(time_var))
            print(plan_output)
            total_time += solution.Min(time_var)
        print('Total time of all routes: {}min'.format(total_time))

    # Print solution on console.
    if solution:
        print("---------出力結果---------",)
        print("end solve time:", datetime.datetime.now())
        print_solution(data, manager, routing, solution)
        print("--------------------------")
    else:
        print("!!!!!!!!!!!!!!ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("ルートの最適化に失敗しました。設定値を変更して下さい。")
        print("!!!!!!!!!!!!!!ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        df_drop = []
        return df_drop
        #sys.exit ()

    ########### MAP ############################################

    #地図用データ取得
    def get_solution(manager, routing, solution):
        """Get optimization results"""
        result = dict()
        for vehicle_id in range(Vehicle_Cnt):
            #初期化
            startIndex = routing.Start(vehicle_id)
            index = startIndex
            route_distance = 0
            previous_max_time='---'
            lpCnt=-1
            total_weight=0
            weight=0
            total_time=0
            start_time=0
            GrNo=0
            #車両情報取得
            strCarCd = df_car['トラックコード'][vehicle_id]
            iCarCapa = df_car['最大積載量'][vehicle_id]

            #到着順取得
            if len(total_excel_plan)>0:
                strQuery = "トラックコード=='"+strCarCd +"'"
                buff_total_excel_plan=total_excel_plan.query(strQuery).groupby(['トラックコード']).agg({'到着順': 'max'})
                if len(buff_total_excel_plan)>0:
                    #print("!!!!!!!!buff_total_excel_plan!!!!!!!\n",buff_total_excel_plan)
                    lpCnt = int(buff_total_excel_plan['到着順'][0])
            FristLpCnt=lpCnt+1

            endIndex = routing.End(vehicle_id)
            max_weight = solution.Value(capacity.CumulVar(endIndex))
            while not routing.IsEnd(index):
                node_index = manager.IndexToNode(index)
                time_var = time_dimension.CumulVar(index)
                iDepartTime=0
                if 0!=node_index:
                    iDepartTime=LIFT_TIME
                else:
                    start_time = solution.Min(time_var)

                #到着順設定
                if GrNo!=df['GrNo'][node_index]:
                    lpCnt+=1
                GrNo=df['GrNo'][node_index]

                previous_index = index
                index = solution.Value(routing.NextVar(index))

                #weight=Decimal(df['質量'][node_index]).quantize(Decimal('1'), rounding=ROUND_HALF_UP)
                weight=df['質量'][node_index]
                total_weight =solution.Value(capacity.CumulVar(index))
                left_weight = max_weight-total_weight
                if df['引取区分'][node_index]=='デポ':
                    size=1
                else:
                    size=abs(weight/100)

                result[previous_index] = {'トラックコード': strCarCd,'最大積載量': iCarCapa,
                                          '配送No': df['配送No'][node_index], '到着順': str(lpCnt), '到着時間':chgHourFromMin(solution.Min(time_var)), '出発時間':chgHourFromMin(solution.Max(time_var)+iDepartTime), '引取・引取納め区分':df['引取・引取納め区分'][node_index],
                                          '配送質量[kg]': weight, '積載可能質量[kg]': iCarCapa - left_weight, '可能車両': df['可能車両'][node_index], '累積時間': chgHourFromMin(solution.Max(time_var) + iDepartTime - start_time), '累積直線距離[km]': route_distance,'グループNo': str(GrNo),'配送Gr質量[kg]': df['質量Gr'][node_index],
                                          '前地点出発時間':previous_max_time, '指定開始時間':chgHourFromMin(df['開始分'][node_index]), '指定終了時間':chgHourFromMin(df['終了分'][node_index]), '円サイズ':size}
                #前地点出発時間保持
                previous_max_time=chgHourFromMin(solution.Max(time_var)+iDepartTime)

                #print("ArcCost[", previous_index,"][", index,"][", vehicle_id,"][",str(routing.GetArcCostForVehicle(previous_index, index, vehicle_id)))
                    
                if(not routing.IsEnd(index)):
                    #累積直線距離保持
                    n_node_index = manager.IndexToNode(index)
                    route_distance += Decimal(data_distance[node_index][n_node_index]/1000).quantize(Decimal('0.1'), rounding=ROUND_HALF_UP)
                else:
                    #0地点上書き
                    route_distance += Decimal(data_distance[node_index][data['depot']]/1000).quantize(Decimal('0.1'), rounding=ROUND_HALF_UP)
                    result[startIndex]['累積直線距離[km]']=route_distance
                    n_time_var = time_dimension.CumulVar(index)
                    result[startIndex]['到着時間']=chgHourFromMin(solution.Max(n_time_var))
                    result[startIndex]['累積時間']=chgHourFromMin(solution.Max(n_time_var) + iDepartTime - start_time)
                    result[startIndex]['到着順']= str(FristLpCnt)
                    result[startIndex]['前地点出発時間']=previous_max_time
                    result[startIndex]['配送質量[kg]']=max_weight
                    #result[startIndex]['配送質量[kg]']='---'
                    result[startIndex]['グループNo']='---'
                    result[startIndex]['配送Gr質量[kg]']='---'
                    result[startIndex]['指定開始時間']='---'
                    result[startIndex]['指定終了時間']='---'
        return result

    #地図データ作成
    #Drop
    df_drop = []
    se_drop=[]
    for node in range(len(df)):
        if routing.IsStart(node) or routing.IsEnd(node):
            continue
        if solution.Value(routing.NextVar(node)) == node:
            se_drop.append(df.iloc[node])
    if len(se_drop)>0:
        df_drop=pd.DataFrame(se_drop)
        df_drop = df_drop[df_drop['納入先名'] != depo_name]
        df_drop['円サイズ'] = abs(df_drop['質量']/100)
    #for index in range(len(df_drop)):
    #    if depo_name==df_drop['納入先名'][index]:
    #        df_drop['円サイズ'][index]=30
    if len(df_drop)>0:
        df_drop=df_drop.reset_index()
        total_map_drop = pd.concat([total_map_drop, df_drop])
        total_map_drop=total_map_drop.reset_index(drop=True)

    #納入先
    result = get_solution(manager, routing, solution)
    result_table = pd.DataFrame(result).T
    result_table=result_table.reset_index(drop=True)
    #for vehicle_id in range(Vehicle_Cnt):
    #strQuery = "トラックコード=='" + df_car['トラックコード'][vehicle_id] +"'"
    route_ret=result_table.merge(df, left_on='配送No', right_on='配送No', how='left', suffixes=['', '_right'])

    total_map_result = pd.concat([total_map_result, route_ret])
    total_map_result=total_map_result.reset_index(drop=True)
    #print("####df####\n",df)
    #print("####result_table####\n",result_table)
    #print("####route_ret####\n",route_ret)

    fig=dsp_map(df_drop, route_ret, loopMode)


    ########### FILE ############################################
    #ファイル出力
    os.mkdir(FilePath+"\data"+strStartime +strArea)
    fig.write_html(FilePath+"\data"+strStartime +strArea+"\配送経路_" + strStartime +strArea+ ".html")

    if len(result_table)>0:
        df_result_data = pd.merge(result_table, df, left_on='配送No', right_on='配送No', sort=False, suffixes=['', '_right']).drop(columns=['開始分','終了分','時間指定開始','時間指定終了','質量'])
        df_result_data = df_result_data.sort_values(['トラックコード', '出発時間'])
        df_result_data = df_result_data[df_result_data['累積直線距離[km]']!=0]
        df_result_data = df_result_data.dropna(subset=['累積直線距離[km]'])
        df_result_data.to_excel(FilePath+"\data"+strStartime+strArea+"\配送計画情報_"+strStartime+strArea+EXCEL_TYPE,encoding="shift jis", index = False)
        df_result_data['エリア名']=strArea
        df_result_data=df_result_data.reset_index(drop=True)
        total_excel_plan = pd.concat([total_excel_plan, df_result_data])
        total_excel_plan=total_excel_plan.reset_index(drop=True)

    #df_result_data['配送質量[kg]']
    strQuery = "配送No==0"
    df_car_data=df_result_data.query(strQuery).merge(df_car, left_on='トラックコード', right_on='トラックコード', how='right',
                                                suffixes=['', '_right']).drop(columns=['最大積載量_right','配送No','前地点出発時間','住所１','納入先名','緯度','経度'])
    df_car_data = df_car_data.rename(columns={'最大積載量_left': '最大積載量'})
    df_car_data = df_car_data[df_car_data['累積直線距離[km]']!=0]
    df_car_data = df_car_data.dropna(subset=['累積直線距離[km]'])
    df_car_data['エリア名']=strArea
    df_car_data = df_car_data.reset_index(drop=True)
    df_car_data.to_excel(FilePath+"\data"+strStartime+strArea+"\配車計画情報_"+strStartime+strArea+EXCEL_TYPE,encoding="shift jis", index = False)
    total_excel_car = pd.concat([total_excel_car, df_car_data])
    total_excel_car=total_excel_car.reset_index(drop=True)

    df_car_ret = df_car[df_car['トラックコード'].isin(df_car_data['トラックコード'])]
    df_car_ret = df_car_ret.reset_index(drop=True)
    for vehicle_id in range(len(df_car_data)):
        if df_car_data['累積直線距離[km]'][vehicle_id]>0:

            ################## 開始終了時間再設定 ############################
            #1番長い空き時間に次の可能時間を入れ込む
            strQuery = "トラックコード=='" + df_car_data['トラックコード'][vehicle_id] + "'"
            df_car_main_query=df_car_main.query(strQuery)
            df_car_main_query = df_car_main_query.reset_index(drop=True)
            iDefaultStartTime =df_car_main_query['MinStartTime'][0]
            iDefaultEndTime =df_car_main_query['MaxEndTime'][0]
            
            strQuery = "トラックコード=='" + df_car_data['トラックコード'][vehicle_id] + "'"
            df_total_car_query=total_excel_car.query(strQuery)
            df_total_car_query = df_total_car_query.sort_values(['出発時間'])
            df_total_car_query = df_total_car_query.reset_index(drop=True)
            iStartTime=0
            iEndTime=0
            iMaxIntervalTime=0
            print("!!!!!! 開始終了時間再設定 トラックコード[",df_car_data['トラックコード'][vehicle_id],"]LoopLen[",len(df_total_car_query)+1,"]!!!!!!")
            print("!!!!!! df_total_car_query!!!!!!!!!!!!\n",df_total_car_query)
            for iTotal in range(len(df_total_car_query)+1):
                iIntervalTime=0
                if iTotal == 0:
                    iTotalStartTime =chgHourFromStr(df_total_car_query['出発時間'][iTotal])
                    iTotalEndTime =chgHourFromStr(df_total_car_query['到着時間'][iTotal])
                    print("!!!!!!Frist Start iTotalStartTime[",iTotalStartTime,"]iMaxIntervalTime[",iTotalStartTime-iDefaultStartTime-LIFT_TIME,"]iDefaultStartTime[",iDefaultStartTime,"]iDefaultEndTime[",iDefaultEndTime,"]!!!!!!")
                    #まずは、INPUTファイルの開始時間～初回割付出発時間を設定
                    iMaxIntervalTime=iTotalStartTime-iDefaultStartTime-LIFT_TIME
                    if iMaxIntervalTime>0:
                        #開始時間はINPUTファイルの開始時間
                        iStartTime=iDefaultStartTime
                        #終了時間は初回割付出発時間-30分
                        iEndTime=iTotalStartTime-LIFT_TIME
                    else:
                        #空いてないなら割付しない
                        iMaxIntervalTime=0
                        iStartTime=iDefaultEndTime
                        iEndTime=iDefaultEndTime  
                    print("!!!!!!Frist End iStartTime[",iStartTime,"]iEndTime[",iEndTime,"]iMaxIntervalTime[",iMaxIntervalTime,"]!!!!!!")
                elif iTotal==len(df_total_car_query):
                    print("!!!!!!Last Start iPreTotalEndTime[",iPreTotalEndTime,"]iMaxIntervalTime[",iMaxIntervalTime,"]iIntervalTime[",iDefaultEndTime-iPreTotalEndTime,"]!!!!!!")
                    iIntervalTime=iDefaultEndTime-iPreTotalEndTime-LIFT_TIME
                    if iIntervalTime>=iMaxIntervalTime:
                        iMaxIntervalTime=iIntervalTime
                        iEndTime=iDefaultEndTime
                        #開始時間は前ループの到着時間+30分
                        if iPreTotalEndTime+LIFT_TIME>iDefaultEndTime:
                            #最大値はINPUTファイルの終了時間になるようにする
                            iStartTime=iDefaultEndTime
                        else:
                            iStartTime=iPreTotalEndTime+LIFT_TIME
                    print("!!!!!!Last End iStartTime[",iStartTime,"]iEndTime[",iEndTime,"]iMaxIntervalTime[",iMaxIntervalTime,"]!!!!!!")
                else:
                    iTotalStartTime =chgHourFromStr(df_total_car_query['出発時間'][iTotal])
                    iTotalEndTime =chgHourFromStr(df_total_car_query['到着時間'][iTotal])
                    print("!!!!!!Center Start iTotal[",iTotal,"]iTotalStartTime[",iTotalStartTime,"]iTotalEndTime[",iTotalEndTime,"]iMaxIntervalTime[",iMaxIntervalTime,"]!!!!!!")

                    iIntervalTime=iTotalStartTime-iPreTotalEndTime-LIFT_TIME*2
                    if iIntervalTime>=iMaxIntervalTime:
                        iMaxIntervalTime=iIntervalTime
                        if iMaxIntervalTime>0:
                            print("!!!!!!Center Mid iPreTotalEndTime[",iPreTotalEndTime,"]iMaxIntervalTime[",iMaxIntervalTime,"]iTotalStartTime[",iTotalStartTime,"]!!!!!!")
                            ##開始時間は前ループの到着時間+30分
                            #if iPreTotalEndTime+LIFT_TIME>iDefaultEndTime:
                            #    #最大値はINPUTファイルの終了時間になるようにする
                            #    iStartTime=iDefaultEndTime
                            #else:
                            #    iStartTime=iPreTotalEndTime+LIFT_TIME
                            ##終了時間は今ループの出発時間-30分
                            #if iTotalStartTime-LIFT_TIME<iDefaultStartTime:
                            #    #最小値はINPUTファイルの開始時間になるようにする
                            #    iEndTime=iDefaultStartTime
                            #else:
                            #    iEndTime=iTotalStartTime-LIFT_TIME
                            iStartTime=iPreTotalEndTime+LIFT_TIME
                            iEndTime=iTotalStartTime-LIFT_TIME
                        else:
                            #割付しない
                            iStartTime=iDefaultEndTime
                            iEndTime=iDefaultEndTime
                    print("!!!!!!Center End iTotal[",iTotal,"]iStartTime[",iStartTime,"]iEndTime[",iEndTime,"]iMaxIntervalTime[",iMaxIntervalTime,"]!!!!!!")

                iPreTotalEndTime=iTotalEndTime
            print("!!!!!!Finish iStartTime[",iStartTime,"]iEndTime[",iEndTime,"]!!!!!!")
            df_car_ret['MinStartTime'][vehicle_id]=iStartTime
            df_car_ret['開始時間(Min)'][vehicle_id]=chgHourFromMin(iStartTime)
            df_car_ret['MaxEndTime'][vehicle_id]=iEndTime
            df_car_ret['終了時間(Max)'][vehicle_id]=chgHourFromMin(iEndTime)

            #chgGoTime=chgHourFromStr(df_car_data['出発時間'][vehicle_id])-LIFT_TIME
            #chgBackTime=chgHourFromStr(df_car_data['到着時間'][vehicle_id])+LIFT_TIME
            #if chgBackTime<df_car_ret['MaxEndTime'][vehicle_id]:
            #    df_car_ret['MinStartTime'][vehicle_id]=chgBackTime
            #    df_car_ret['開始時間(Min)'][vehicle_id]=chgHourFromMin(chgBackTime)
            #    df_car_ret['MinEndTime'][vehicle_id]=chgBackTime
            #    df_car_ret['終了時間(Min)'][vehicle_id]=chgHourFromMin(chgBackTime)
            #else:
            #    df_car_ret['MinStartTime'][vehicle_id]=df_car_ret['MaxStartTime'][vehicle_id]
            #    df_car_ret['開始時間(Min)'][vehicle_id]=df_car_ret['開始時間(Max)'][vehicle_id]
            #    df_car_ret['MinEndTime'][vehicle_id]=df_car_ret['MaxEndTime'][vehicle_id]
            #    df_car_ret['終了時間(Min)'][vehicle_id]=df_car_ret['終了時間(Max)'][vehicle_id]
    #df_car=df_car.reset_index(drop=True)


    #strQuery = "配送質量[kg]==0"
    #df_car_data=df_car_data.query(strQuery)

    df_drop_data=pd.DataFrame()
    if len(df_drop)>0:
        #df_drop = df_drop[df_drop.index >= lenData]
        df_drop_data=df_drop[['配送No', '指示№', '納入先名', '住所１', '時間指定開始', '時間指定終了', '質量','トラック指定コード','重み']]
        df_drop_data = df_drop_data.rename(columns={'index': '配送No'})
        df_drop_data['エリア名']=strArea
        df_drop_data.to_excel(FilePath+"\data"+strStartime+strArea+"\未割付情報_"+strStartime+strArea+EXCEL_TYPE,encoding="shift jis", index = False)

    df_time_matrix = pd.DataFrame(data['time_matrix']).T
    #df_time_matrix['配送No']=df.Index
    df_time_matrix['納入先名']=df['納入先名']
    df_time_matrix['住所１']=df['住所１']
    df_time_matrix['配送No']=df['配送No']
    df_time_matrix.to_excel(FilePath+"\data"+strStartime+strArea+"\移動時間表_"+strStartime+strArea+EXCEL_TYPE,encoding="shift jis")

    #print("!!!!!!df_car_ret!!!!!!\n",df_car_ret)

    print("finish time:", datetime.datetime.now())

    return df_drop_data

##### SELECT AREA ############
SEQTYPE_TYPE=0
SEQTYPE_TIMES=1
SEQTYPE_AREANAME=2
SEQTYPE_LAT=3
SEQTYPE_LON=4

strSeqType=[
        ['First','1','早朝','35.0','137.0'],
        ['Area','1','長野','35.5','137.9'],
        ['Area','1','静岡','34.7','137.9'],
        ['Area','3','西三河','35.0','137.0'],
        ['Area','2','東三河','34.8','137.3'],
        ['Area','2','岐阜_東尾張','35.2','137.0'],
        ['Area','3','名古屋_西尾張_三重_知多','35.1','136.9'],
        #['Time','1','最終割付(車両絞込有)','',''],
        ['Last','3','最終割付','',''],
        ]
strSeqName=[
    ['330'],
    ['長野'],
    ['静岡','浜松'],
    ['安城市','西尾市','碧南市','高浜市','豊田市','みよし市','三好','刈谷市','知立市'],
    ['新城市','豊橋市','豊川市','蒲郡市','額田','岡崎市'],
    ['岐阜','東郷','大口','日進市','小牧市','尾張旭市','瀬戸市'],
    ['東浦','大府','名古屋','弥富市','愛西市','あま市','桑名'],
    #['1400'],
    ['1440'],
    ]


df_Area_Assign = pd.DataFrame(strSeqName)
df_Area_Assign['仮合計質量']=0

for iAvailable in range(len(DF_HEAVY_TABLE)): 
    df_Area_Assign['質量'+DF_HEAVY_TABLE['車格'][iAvailable]]=0
    df_Area_Assign['件数'+DF_HEAVY_TABLE['車格'][iAvailable]]=0
    df_Area_Assign['合計件数'+DF_HEAVY_TABLE['車格'][iAvailable]]=0

df_deli_main['住所']=df_deli_main['住所１']

#マイエリアを設定する。
df_deli_main['マイエリア']=0
for iSeq in range(len(strSeqType)):
    if strSeqType[iSeq][0] == 'Area':
        strQuery= "住所.str.contains('" + strSeqName[iSeq][0] + "')"
        iLen=len(strSeqName[iSeq])-1
        for iIndex in range(iLen):
            strQuery = strQuery + " or 住所.str.contains('" + strSeqName[iSeq][iIndex+1] + "')"
        df_area_set=df_deli_main.query(strQuery)
        for iDeli in range(len(df_area_set)):
            df_deli_main['マイエリア'][df_area_set.index[iDeli]]=iSeq
#エリア割付チェック
df_area_none=df_deli_main[df_deli_main['マイエリア']==0]
if len(df_area_none)>0:
    print("!!!!!!!df_area_none マイエリア割付エラー!!!!!!!\n",df_area_none)
    exit

#デポはエリア設定しない
df_deli_main['マイエリア'][df_deli_main['引取区分']=='デポ']=0
print("!!!!!!!マイエリア df_deli_main!!!!!!!\n",df_deli_main)

####車両割付############ 
# 往復できるなら、対象エリア内でできるだけ同じ車両達で往復することで未割付を未然に防ぐ
# 車両割付しないと、最初の方のエリアで全車両使いってしまい、最後の方のエリアの時間指定に間に合わない

#エリア間の距離を求める
area_distance = []
for From_lat,From_lon,From_add,From_type  in zip([row[SEQTYPE_LAT] for row in strSeqType],[row[SEQTYPE_LON] for row in strSeqType],[row[SEQTYPE_AREANAME] for row in strSeqType],[row[SEQTYPE_TYPE] for row in strSeqType]):
    arrDis = []
    dis=0
    
    if From_type != 'Area':
        area_distance.append(arrDis)
        continue
    for To_lat,To_lon,To_add,To_type in zip([row[SEQTYPE_LAT] for row in strSeqType],[row[SEQTYPE_LON] for row in strSeqType],[row[SEQTYPE_AREANAME] for row in strSeqType],[row[SEQTYPE_TYPE] for row in strSeqType]):
        if To_type != 'Area':
            arrDis.append(1_000_000_000_000)
            continue
        if From_add == To_add:
            arrDis.append(1_000_000_000_000)
            continue
        result = vincenty_inverse(float(From_lat),float(From_lon), float(To_lat), float(To_lon), 1)
        dis=round(result['distance'], 3)
        arrDis.append(dis)
    area_distance.append(arrDis)  
print("!!!!!!!area_distance!!!!!!!!\n",area_distance)

#全体の必要台数を仮算出
df_car_Loop=df_car_main.copy()
df_car_Loop['エリア']=0
df_car_Loop['引取エリア']=0

for iSeq in range(len(strSeqType)):
    #AREA
    if strSeqType[iSeq][0] == 'Area':
        strQuery=""
        strQuery= strQuery + "(住所.str.contains('" + strSeqName[iSeq][0] + "')"
        iLen=len(strSeqName[iSeq])-1
        for iIndex in range(iLen):
            strQuery = strQuery + " or 住所.str.contains('" + strSeqName[iSeq][iIndex+1] + "')"
        for iAvailable in range(len(DF_HEAVY_TABLE)):
            
            strQueryLocal =strQuery+") and 可能車両=='"+DF_HEAVY_TABLE['可能車両'][iAvailable]+"'"
            strQueryLocal = strQueryLocal + "and 終了分>="+ strSeqName[0][0]
            strQueryLocal = strQueryLocal + "and (引取区分== '引取納め（引取）' or 引取区分== '出荷')"
            df_Sum=df_deli_main.query(strQueryLocal).groupby('可能車両').agg({'質量': 'sum'})
            
            iCount=0
            iWeight=0
            if len(df_Sum)>0:
                iWeight=df_Sum['質量'][0]
                df_Area_Assign['質量'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq]=iWeight
                df_Area_Assign['仮合計質量'][iSeq]+=iWeight
                #iCount = math.ceil(iWeight/(DF_HEAVY_TABLE['最大積載量(min)'][iAvailable]*int(strSeqType[iSeq][1])))
                if iAvailable==2 or iAvailable==3:
                    iCount =Decimal(iWeight/(DF_HEAVY_TABLE['最大積載量(min)'][iAvailable]*int(strSeqType[iSeq][1]))+0.45).quantize(Decimal('1'), rounding=ROUND_HALF_UP)
                else:
                    iCount =Decimal(iWeight/(DF_HEAVY_TABLE['最大積載量(min)'][iAvailable]*int(strSeqType[iSeq][1]))+0.3).quantize(Decimal('1'), rounding=ROUND_HALF_UP)
            df_Area_Assign['件数'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq]=iCount
            df_Area_Assign['合計件数'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq]=iCount
            DF_HEAVY_TABLE['仮合計回数'][iAvailable]+=iCount
 
print("!!!!!!!df_Area_Assign!!!!!!!\n",df_Area_Assign)
#積載量が少ない車両で見積りで余裕があるなら、積載量が少ないので出し、出す車両数を減らす
for iSeq in range(len(strSeqType)):
    if strSeqType[iSeq][0] == 'Area':
        iAvailableAssign=0XFF
        for iAvailable in range(len(DF_HEAVY_TABLE)):
            #割付あれば入れ替えチェック対象
            if df_Area_Assign['件数'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq]>0:
                #割付変更していていたら残りは取り消し
                if iAvailableAssign!=0XFF and df_Area_Assign['件数'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq]>0:
                    df_Area_Assign['件数'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq]-=1
                    DF_HEAVY_TABLE['仮合計回数'][iAvailable]-=1
                    DF_HEAVY_TABLE['仮合計回数'][iAvailableAssign]+=1

                #割付余裕あるか
                if DF_HEAVY_TABLE['仮合計回数'][iAvailable]<DF_HEAVY_TABLE['count'][iAvailable]:
                    #余裕ある車両でいけるように割付し直し
                    if df_Area_Assign['仮合計質量'][iSeq]<DF_HEAVY_TABLE['最大積載量(min)'][iAvailable]:
                        if iAvailableAssign==0XFF:
                            df_Area_Assign['件数'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq]+=1
                            iAvailableAssign=iAvailable

                df_Area_Assign['仮合計質量'][iSeq]-=df_Area_Assign['質量'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq]
       

#割付
for iSeq in range(len(strSeqType)):
    if strSeqType[iSeq][0] == 'Area':
        for iAvailable in range(len(DF_HEAVY_TABLE)):
            iCount=df_Area_Assign['件数'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq]
            for iCar in range(len(df_car_Loop)):
                if iCount<=0:
                    break   
                if df_car_Loop['車格'][iCar]==DF_HEAVY_TABLE['車格'][iAvailable] and df_car_Loop['エリア'][iCar]==0:
                    df_car_Loop['エリア'][iCar]=iSeq
                    iCount+=-1

print("!!!!!!!df_car_Loop['エリア']!!!!!!!\n",df_car_Loop['エリア'])

# 引取で自エリアで足りない場合は他エリアから割付(応援)
for iSeq in range(len(strSeqType)):
    #AREA
    if strSeqType[iSeq][0] == 'Area':
        strQuery=""
        strQuery= strQuery + "(住所.str.contains('" + strSeqName[iSeq][0] + "')"
        iLen=len(strSeqName[iSeq])-1
        for iIndex in range(iLen):
            strQuery = strQuery + " or 住所.str.contains('" + strSeqName[iSeq][iIndex+1] + "')"
        for iAvailable in range(len(DF_HEAVY_TABLE)):
            
            strQueryLocal =strQuery+") and 可能車両=='"+DF_HEAVY_TABLE['可能車両'][iAvailable]+"'"
            strQueryLocal = strQueryLocal + "and 終了分>="+ strSeqName[0][0]
            strQueryLocal = strQueryLocal + "and (引取区分== '引取')"
            df_Sum=df_deli_main.query(strQueryLocal).groupby('可能車両').agg({'質量abs': 'sum'})
            
            iCount=0
            iWeight=0
            if len(df_Sum)>0:
                iWeight=df_Sum['質量abs'][0]
                iCount=(math.ceil(iWeight/DF_HEAVY_TABLE['最大積載量(min)'][iAvailable])-df_Area_Assign['合計件数'+DF_HEAVY_TABLE['車格'][iAvailable]][iSeq])

            print("!!!!!!!引取エリア数 エリア[",strSeqType[iSeq][SEQTYPE_AREANAME],"]エリア番号[",str(iSeq),"]可能車両[",DF_HEAVY_TABLE['可能車両'][iAvailable],"]必要引取エリア数[",str(iCount))
            if iCount<=0:
               continue     
            iLp=0

            #自エリアで引取件数が足りない場合は他エリアから応援
            area_help=area_distance[iSeq].copy() #値渡し
            while iCount>0 and iLp<len(area_help):
                iCloseArea = np.argmin(np.abs(np.array(area_help) - 0))
                print("!!!!!!!応援エリア エリア[",strSeqType[iSeq][SEQTYPE_AREANAME],"]エリア番号[",str(iSeq),"]可能車両[",DF_HEAVY_TABLE['可能車両'][iAvailable],"]必要引取エリア数[",str(iCount),"]クロースエリア[",strSeqType[iCloseArea][SEQTYPE_AREANAME],"]クロースエリア番号[",str(iCloseArea))

                for iCar in range(len(df_car_Loop)):
                    if iCount<=0:
                        break
                    elif iCloseArea==df_car_Loop['エリア'][iCar] and df_car_Loop['引取エリア'][iCar]==0 and df_car_Loop['車格'][iCar]==DF_HEAVY_TABLE['車格'][iAvailable]:
                        df_car_Loop['引取エリア'][iCar]=iSeq
                        iRound=int(strSeqType[iCloseArea][1])
                        if iRound>=3: #3往復するぐらいならほかのエリアも使おう
                            iRound-=1
                        iCount=iCount+(iRound*-1)
                area_help[iCloseArea]=1_000_000_000_000
                iLp+=1

print("!!!!!!!df_car_Loop['引取エリア']!!!!!!!\n",df_car_Loop['引取エリア'])

####Looping#########
os.mkdir(FilePath)
df_deli_main['割付済']=0

def LoopExcete(iAvailable,iSeq,iBack):
    print("!!!!!!!!!!!!!!!!!\n!!!!!!startLoop!!!!!!!可能車両[",DF_HEAVY_TABLE['可能車両'][iAvailable],"]エリア[", strSeqName[iSeq][0],"]回数["+str(iBack)+"]\n!!!!!!!!!!!!!!!!!!!!!!!!")

    ####DELI DATE MAKE#######
    #if strSeqType[iSeq][0] != 'Last':
    #    time_out=
    #else:
    #    time_out=15

    strQuery=""

    #可能車両
    #if strSeqType[iSeq][0] == 'Time':
    #if iAvailable==0: #8tで行く
    #    #strQuery = strQuery+"(可能車両=='"+DF_HEAVY_TABLE['可能車両'][0] + "' or (可能車両!='"+DF_HEAVY_TABLE['可能車両'][0] + "'and 質量Gr<=8300)) and "
    #    strQuery = strQuery+"(可能車両=='"+DF_HEAVY_TABLE['可能車両'][iAvailable] + "') and "
    #elif  iAvailable==1: #12tで行く→「8tまでOK」は除外。「15t、トレーラまでOK」で12kg以下は分割になるので除外
    #    strQuery = strQuery+"(可能車両!='"+DF_HEAVY_TABLE['可能車両'][0] +"' and (可能車両=='"+DF_HEAVY_TABLE['可能車両'][1] + "' or (可能車両!='"+DF_HEAVY_TABLE['可能車両'][1] + "'and 質量Gr<="+str(DF_HEAVY_TABLE['最大積載量(min)'][iAvailable])+"))) and "
    #elif  iAvailable==2: #15tで行く→「8t,12tまでOK」は除外。「トレーラまでOK」で15kg以下は分割になるので除外
    #    strQuery = strQuery+"((可能車両!='"+DF_HEAVY_TABLE['可能車両'][0] +"' and 可能車両!='"+DF_HEAVY_TABLE['可能車両'][1] +"') and ((可能車両=='"+DF_HEAVY_TABLE['可能車両'][2]+"') or ( 可能車両!='"+DF_HEAVY_TABLE['可能車両'][2] + "' and 質量Gr<="+str(DF_HEAVY_TABLE['最大積載量(min)'][iAvailable])+"))) and "
    #elif  iAvailable==3: #トレーラで行く
    #    strQuery = strQuery+"(可能車両=='"+DF_HEAVY_TABLE['可能車両'][3]+"') and "
    #else:
    #    sys.exit ()
    if iAvailable==0: #8tで行く
        strQuery = strQuery+"(可能車両=='"+DF_HEAVY_TABLE['可能車両'][0] + "' or (可能車両!='"+DF_HEAVY_TABLE['可能車両'][0] + "'and 質量abs<="+str(DF_HEAVY_TABLE['最大積載量(min)'][iAvailable])+")) and "
        #strQuery = strQuery+"(可能車両=='"+DF_HEAVY_TABLE['可能車両'][iAvailable] + "') and "
    elif  iAvailable==1: #12tで行く→「8tまでOK」は除外。「15t、トレーラまでOK」で12kg以下は分割になるので除外
        strQuery = strQuery+"(可能車両!='"+DF_HEAVY_TABLE['可能車両'][0] +"' and (可能車両=='"+DF_HEAVY_TABLE['可能車両'][1] + "' or (可能車両!='"+DF_HEAVY_TABLE['可能車両'][1] + "'and 質量abs<="+str(DF_HEAVY_TABLE['最大積載量(min)'][iAvailable])+"))) and "
    elif  iAvailable==2: #15tで行く→「8t,12tまでOK」は除外。「トレーラまでOK」で15kg以下は分割になるので除外
        strQuery = strQuery+"((可能車両!='"+DF_HEAVY_TABLE['可能車両'][0] +"' and 可能車両!='"+DF_HEAVY_TABLE['可能車両'][1] +"') and ((可能車両=='"+DF_HEAVY_TABLE['可能車両'][2]+"') or ( 可能車両!='"+DF_HEAVY_TABLE['可能車両'][2] + "' and 質量abs<="+str(DF_HEAVY_TABLE['最大積載量(min)'][iAvailable])+"))) and "
    elif  iAvailable==3: #トレーラで行く
        strQuery = strQuery+"(可能車両=='"+DF_HEAVY_TABLE['可能車両'][3]+"') and "
    else:
        sys.exit ()
    #elif strSeqType[iSeq][0] != 'Time':
    #    strQuery = strQuery+"(可能車両=='"+DF_HEAVY_TABLE['可能車両'][iAvailable] + "') and "

    #strQuery = strQuery+"(可能車両=='"+DF_HEAVY_TABLE['可能車両'][iAvailable] + "') and "

    #AREA
    lstTargetArea=[]
    if strSeqType[iSeq][0] == 'Area':
        strQuery= strQuery + "(マイエリア==0 or マイエリア==" + str(iSeq)

    #AREA(引取)
        for iLp in range(len(df_car_Loop)):
            if iSeq==df_car_Loop['エリア'][iLp] and df_car_Loop['車格'][iLp]==DF_HEAVY_TABLE['車格'][iAvailable]:
                iTargetArea=df_car_Loop['引取エリア'][iLp]
                if strSeqType[iTargetArea][0] == 'Area':
                    strQuery= strQuery + " or (マイエリア==" + str(iTargetArea) + " and 引取区分== '引取')"
                    lstTargetArea.append(iTargetArea)

        strQuery = strQuery + ") and "
        
    #TIME
    if strSeqType[iSeq][0] != 'Area':
        endTime=strSeqName[iSeq][0]
        strQuery = strQuery + "(終了分<"+ endTime +") and"

    #未割付チェック
    strQuery = strQuery + " (割付済==0) or (引取区分=='デポ')"



    #QUERY
    print("!!!!!!df_deli_area strQuery!!!!!!!",strQuery)
    df_deli_area=df_deli_main.query(strQuery)
    df_deli_area=df_deli_area.reset_index(drop=True)

    #引取→納め(同一エリアでなければ無条件落下)
    strQuery = "引取区分== '引取納め（引取）' or 引取区分== '引取納め（納め）'"
    df_take=df_deli_area.query(strQuery)
    df_take=df_take.reset_index(drop=True)
    for iFrom in range(len(df_take)):
        bFromMode = 0
        if df_take["引取区分"][iFrom]=='引取納め（引取）':
            bFromMode = 1
        elif df_take["引取区分"][iFrom]=='引取納め（納め）':
            bFromMode = 2
        if 0!=bFromMode:
            bHit = False
            strSiji=df_take["指示№"][iFrom]
            strDeliNo=df_take["配送No"][iFrom]
            for iTo in range(len(df_take)):
                if strSiji== df_take["指示№"][iTo]:
                    if df_take["引取区分"][iTo]=='引取納め（納め）' and bFromMode==1:
                        bHit = True
                        break
                    elif df_take["引取区分"][iTo]=='引取納め（引取）' and bFromMode==2:
                        bHit = True
                        break
            if bHit == False:
                df_deli_area=df_deli_area[df_deli_area['配送No']!=strDeliNo]
                df_deli_area=df_deli_area.reset_index(drop=True)


    #同じ場所出荷→引取、引取納め（引取）(同一エリアでなければ無条件落下)
    strQuery = "SamePlaceFrom!=0 or SamePlaceTo!=0"
    df_same_place=df_deli_area.query(strQuery)
    df_same_place=df_same_place.reset_index(drop=True)
    for iFrom in range(len(df_same_place)):
        if df_same_place["SamePlaceFrom"][iFrom]!=0:
            strDeliNo=df_same_place["配送No"][iFrom]
            bHit=False
            for iTo in range(len(df_same_place)):
                if df_same_place['SamePlaceFrom'][iFrom]==df_same_place['SamePlaceTo'][iTo]:
                    bHit=True
                    break
            if bHit == False:
                df_deli_area=df_deli_area[df_deli_area['配送No']!=strDeliNo]
                df_deli_area=df_deli_area.reset_index(drop=True)
    for iTo in range(len(df_same_place)):
        if df_same_place["SamePlaceTo"][iTo]!=0:
            strDeliNo=df_same_place["配送No"][iTo]
            bHit=False
            for iFrom in range(len(df_same_place)):
                if df_same_place['SamePlaceFrom'][iFrom]==df_same_place['SamePlaceTo'][iTo]:
                    bHit=True
                    break
            if bHit == False:
                df_deli_area=df_deli_area[df_deli_area['配送No']!=strDeliNo]
                df_deli_area=df_deli_area.reset_index(drop=True)

    ######重み設定######
    #print("!!!!!!df_deli_area1!!!!!!!\n",df_deli_area)
    for iLp in range(len(df_deli_area)):
        #if strSeqType[iSeq][0] == 'Last':
        #    df_deli_area['重み']=10
        #else:
            df_deli_area['重み'][iLp]=df_deli_area['仮重み'][iLp]
            if df_deli_area['可能車両'][iLp]!=DF_HEAVY_TABLE['可能車両'][iAvailable]:
                df_deli_area['重み'][iLp]=df_deli_area['重み'][iLp]*0.1
            #引取エリア(応援)設定時は枯渇しているので優先度高
            for iTarger in range(len(lstTargetArea)):
                if df_deli_area['マイエリア'][iLp]==lstTargetArea[iTarger]:
                    df_deli_area['重み'][iLp]=10

        #for iLp in range(len(df_car)):
        #    if 0!=df_car['引取エリア'][iLp]:
        #        bHit=True

    print("!!!!!!df_deli_area2!!!!!!!\n",df_deli_area)

    #DELI DATA 件数チェック
    if len(df_deli_area[df_deli_area['引取区分']!='デポ'])>0 and len(df_deli_area[df_deli_area['可能車両']==DF_HEAVY_TABLE['可能車両'][iAvailable]])>0:
    ####CAR DATE MAKE#######
        #if iAvailable==0:
        #    strQuery = "車格=='8ｔ車'"
        #elif  iAvailable==1:
        #    strQuery = "車格=='12ｔ車' "
        #elif  iAvailable==2:
        #    strQuery = "車格=='15ｔ車' "
        #elif  iAvailable==3:
        #    strQuery = "車格=='トレーラー' "
        if strSeqType[iSeq][0] != 'Last':
            if iAvailable==0:
                strQuery = "車格=='8ｔ車'"
            elif  iAvailable==1:
                strQuery = "車格=='12ｔ車' "
            elif  iAvailable==2:
                strQuery = "車格=='15ｔ車' "
            elif  iAvailable==3:
                strQuery = "車格=='トレーラー' "
        elif strSeqType[iSeq][0] == 'Last':
            if iAvailable==0:
                strQuery = "車格=='8ｔ車'"
            elif  iAvailable==1:
                strQuery = "車格=='8ｔ車' or 車格=='12ｔ車'"
            elif  iAvailable==2:
                strQuery = "車格=='8ｔ車' or 車格=='12ｔ車' or 車格=='15ｔ車' "
            elif  iAvailable==3:
                strQuery = "車格=='8ｔ車' or 車格=='12ｔ車' or 車格=='15ｔ車' or 車格=='トレーラー' "
        #if iAvailable==0:
        #    strQuery = "車格=='8ｔ車'"
        #    df_car_query=df_car_Loop.query(strQuery)
        #elif  iAvailable==1:
        #    strQuery = "車格=='12ｔ車'"
        #    df_car_query=df_car_Loop.query(strQuery)
        #elif  iAvailable==2:
        #    strQuery = "車格=='12ｔ車' or 車格=='15ｔ車' "
        #elif  iAvailable==3:
        #    strQuery = "車格=='12ｔ車' or 車格=='15ｔ車' or 車格=='トレーラー' "
        if strSeqType[iSeq][0] == 'Area':
            strQuery = strQuery+" and エリア=="+str(iSeq)
        df_car_query=df_car_Loop.query(strQuery)
        df_car_query=df_car_query.reset_index(drop=True)

        ######重み設定######
        #print("!!!!!!df_deli_area1!!!!!!!\n",df_deli_area)
        for iLp in range(len(df_car_query)):
            if df_car_query['MinStartTime'][iLp]<=60*6:
                df_car_query['CarCost'][iLp]=0
            elif  df_car_query['MinStartTime'][iLp]<=60*7:
                df_car_query['CarCost'][iLp]=0.1
            elif  df_car_query['MinStartTime'][iLp]<=60*8:
                df_car_query['CarCost'][iLp]=0.2
            elif  df_car_query['MinStartTime'][iLp]<=60*9:
                df_car_query['CarCost'][iLp]=0.3
            elif  df_car_query['MinStartTime'][iLp]<=60*10:
                df_car_query['CarCost'][iLp]=0.4
            elif  df_car_query['MinStartTime'][iLp]<=60*11:
                df_car_query['CarCost'][iLp]=0.5
            elif  df_car_query['MinStartTime'][iLp]<=60*12:
                df_car_query['CarCost'][iLp]=0.6
            elif  df_car_query['MinStartTime'][iLp]<=60*13:
                df_car_query['CarCost'][iLp]=0.7
            elif  df_car_query['MinStartTime'][iLp]<=60*14:
                df_car_query['CarCost'][iLp]=0.8
            elif  df_car_query['MinStartTime'][iLp]<=60*15:
                df_car_query['CarCost'][iLp]=0.9
            elif  df_car_query['MinStartTime'][iLp]<=60*16:
                df_car_query['CarCost'][iLp]=1.0
            elif  df_car_query['MinStartTime'][iLp]<=60*17:
                df_car_query['CarCost'][iLp]=1.1
            elif  df_car_query['MinStartTime'][iLp]<=60*18:
                df_car_query['CarCost'][iLp]=1.2
            elif  df_car_query['MinStartTime'][iLp]<=60*19:
                df_car_query['CarCost'][iLp]=1.3
            else:
                df_car_query['CarCost'][iLp]=1.4

        print("!!!!!!df_car_query!!!!!!!\n",df_car_query)
        #print(df_car_query)
        #df_car_ret=Looping(strSeqType[iSeq]+DF_HEAVY_TABLE['可能車両'][iAvailable],df_deli_area,df_car_query,NORMAL_LOOP)
        ##Carデータ更新
        #for iCar in range(len(df_car_Loop)):
        #    for iRetCar in range(len(df_car_ret)):
        #        if df_car_Loop['トラックコード'][iCar] == df_car_ret['トラックコード'][iRetCar]:
        #            df_car_Loop['MinStartTime'][iCar]=df_car_ret['MinStartTime'][iRetCar]
        #            df_car_Loop['開始時間(Min)'][iCar]=df_car_ret['開始時間(Min)'][iRetCar]
        #            df_car_Loop['MinEndTime'][iCar]=df_car_ret['MinEndTime'][iRetCar]
        #            df_car_Loop['終了時間(Min)'][iCar]=df_car_ret['終了時間(Min)'][iRetCar]
        if len(df_car_query)>0:
            df_drop_data=Looping(DF_HEAVY_TABLE['可能車両'][iAvailable]+str(iBack+1)+"順目"+CUTTING_NAME+strSeqType[iSeq][2],df_deli_area,df_car_query,strSeqType[iSeq][0])
            print("!!!!!!df_drop_data!!!!!!\n",df_drop_data)
            #Carデータ更新
            for iCar in range(len(df_car_Loop)):
                for iRetCar in range(len(df_car_ret)):
                    if df_car_Loop['トラックコード'][iCar] == df_car_ret['トラックコード'][iRetCar]:
                        df_car_Loop['MinStartTime'][iCar]=df_car_ret['MinStartTime'][iRetCar]
                        df_car_Loop['開始時間(Min)'][iCar]=df_car_ret['開始時間(Min)'][iRetCar]
                        df_car_Loop['MaxEndTime'][iCar]=df_car_ret['MaxEndTime'][iRetCar]
                        df_car_Loop['終了時間(Max)'][iCar]=df_car_ret['終了時間(Max)'][iRetCar]
            #割付状態更新
            for iDeli in range(len(df_deli_area)):
                if df_deli_area['引取区分'][iDeli]=='デポ':
                    continue
                bDrop=False
                for iDrop in range(len(df_drop_data)):    
                    if df_deli_area['配送No'][iDeli] == df_drop_data['配送No'][iDrop]:
                        bDrop=True
                if bDrop==False:
                    iDeliNo=df_deli_area['配送No'][iDeli]
                    df_deli_main['割付済'][iDeliNo]=1

for iAvailable in reversed(range(len(DF_HEAVY_TABLE['可能車両']))):
    for iSeq in range(len(strSeqType)):
        if strSeqType[iSeq][SEQTYPE_TYPE] != "First":
            continue
        LoopBack=int(strSeqType[iSeq][1])
        for iBack in range(LoopBack):
            LoopExcete(iAvailable,iSeq,iBack)

for iAvailable in reversed(range(len(DF_HEAVY_TABLE['可能車両']))):
    for iSeq in range(len(strSeqType)):
        if strSeqType[iSeq][SEQTYPE_TYPE] != "Area":
            continue
        LoopBack=int(strSeqType[iSeq][1])
        for iBack in range(LoopBack):
            LoopExcete(iAvailable,iSeq,iBack)

for iAvailable in reversed(range(len(DF_HEAVY_TABLE['可能車両']))):
    for iSeq in range(len(strSeqType)):
        if strSeqType[iSeq][SEQTYPE_TYPE] != "Time":
            continue
        LoopBack=int(strSeqType[iSeq][1])
        for iBack in range(LoopBack):
            LoopExcete(iAvailable,iSeq,iBack)

for iAvailable in range(len(DF_HEAVY_TABLE['可能車両'])):

    for iSeq in range(len(strSeqType)):
        if strSeqType[iSeq][SEQTYPE_TYPE] != "Last":
            continue
        LoopBack=int(strSeqType[iSeq][1])
        for iBack in range(LoopBack):
            #最終時間を1時間ずつ増やす
            #for iCar in range(len(df_car_Loop)):
            #    strQuery = "トラックコード=='" + df_car_Loop['トラックコード'][iCar] + "'"
            #    df_time_add_car = total_excel_car.query(strQuery).groupby(['トラックコード'], dropna=False).agg({'到着時間': 'max'})
            #    df_time_add_car = df_time_add_car.reset_index(drop=True)
            #
            #    InitTime=pd.DataFrame(df_car_Loop)
            #    if len(df_time_add_car)>0:
            #        iTotalEndTime =chgHourFromStr(df_time_add_car['到着時間'][0])
            #        df_car_Loop['MinStartTime'][iCar] =iTotalEndTime+LIFT_TIME
            #        df_car_Loop['開始時間(Min)'][iCar]=chgHourFromMin(InitTime['MinStartTime'][iCar])
            #        df_car_Loop['MinEndTime']   [iCar]=iTotalEndTime+LIFT_TIME
            #        df_car_Loop['終了時間(Min)'][iCar]=chgHourFromMin(InitTime['MinEndTime'][iCar])
            #    else:
            #        df_car_Loop['MinStartTime'] [iCar]=df_car_main['MinStartTime'][iCar]
            #        df_car_Loop['開始時間(Min)'][iCar]=df_car_main['開始時間(Min)'][iCar]
            #        df_car_Loop['MinEndTime'] [iCar]  =df_car_main['MinEndTime'][iCar]
            #        df_car_Loop['終了時間(Min)'][iCar]=df_car_main['終了時間(Min)'][iCar]
            #    
            #    df_car_Loop['MaxStartTime'][iCar]   =df_car_main['MaxStartTime'][iCar]+60*(iBack+1)
            #    df_car_Loop['開始時間(Max)'][iCar]  =chgHourFromMin(df_car_Loop['MaxStartTime'][iCar])
            #    df_car_Loop['MaxEndTime'][iCar]     =df_car_main['MaxEndTime'][iCar]+60*(iBack+1)
            #    df_car_Loop['終了時間(Max)'][iCar]  =chgHourFromMin(df_car_Loop['MaxEndTime'][iCar])
            #    df_car_Loop['MaxWorkingHours'][iCar]=df_car_main['MaxWorkingHours'][iCar]+60*(iBack+1)

            LoopExcete(iAvailable,iSeq,iBack)


Looping(FINISH_LOOP,df_deli_main, df_car_main, 'DSP')

