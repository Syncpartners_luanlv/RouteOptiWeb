import pandas as pd
import requests
import json
import sys
import time

# 川崎の図書館のデータを使用
url = 'https://www.city.kawasaki.jp/170/cmsfiles/contents/0000116/116229/141305_public_facility_library.csv'
df = pd.read_csv(url, encoding='cp932')

#print(df.shape) # (12, 39)

import plotly.graph_objs as go

#trace = go.Scattermapbox(
#    lon = df.経度,
#    lat = df.緯度,
#    mode = 'markers + text',
#    text = df.名称,
#    marker = dict(size=df.絵本の蔵書数/1000,
#                  color="red",
#                 ),
#)
#data = [trace]
#fig = go.Figure(data)
#fig.update_layout(mapbox_style="open-street-map",
#                  mapbox_center_lat=35.6,
#                  mapbox_center_lon=139.6,
#                  mapbox={'zoom': 10},
#                  margin={"r":0,"t":0,"l":0,"b":0},
#)
#fig.show()

import pyproj
from ortools.constraint_solver import pywrapcp
import requests

print("Python Start")

#POST送信
url = "https://localhost:44333/api/ApiProgress"#POST先URL
seqId = sys.argv[1]

#print("Python Start Post"+seqId)

#seqId="1"

response = requests.post(url, json={"SEQID": seqId, "STATUS": "OK", "PROGRESS": "10"}, verify = False)

print("Python End Post")

time.sleep(5)

# index managerを作成
manager = pywrapcp.RoutingIndexManager(len(df), 2, 0)

# routing modelを作成
routing = pywrapcp.RoutingModel(manager)

# 移動距離のcallbackを作成、登録
def distance_callback(from_index, to_index):
    """Returns the distance between the two nodes."""
    # Indexの変換
    from_node = manager.IndexToNode(from_index)
    to_node = manager.IndexToNode(to_index)

    # 2点間の距離を計算
    g = pyproj.Geod(ellps='WGS84')
    distance = g.inv(
        df['経度'][from_node], df['緯度'][from_node], 
        df['経度'][to_node], df['緯度'][to_node]
    )[2]
    return distance

transit_callback_index = routing.RegisterTransitCallback(distance_callback)

# 移動コストを設定
routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

# 配送量のcallbackを作成、登録
def demand_callback(index):
    """Returns the demand of the node."""
    node = manager.IndexToNode(index)
    return df['絵本の蔵書数'][node] / 100

demand_callback_index = routing.RegisterUnaryTransitCallback(demand_callback)

# 積載量の制約
routing.AddDimensionWithVehicleCapacity(
    demand_callback_index,
    0,  # null capacity slack   
    [1500, 500],  # vehicle maximum capacities
    True,  # start cumul to zero
    'Capacity')
capacity = routing.GetDimensionOrDie('Capacity')

# 最適化の実行
search_parameters = pywrapcp.DefaultRoutingSearchParameters()
search_parameters.time_limit.seconds = 100
solution = routing.SolveWithParameters(search_parameters)

# 解を確認
#print(solution.ObjectiveValue())

def get_solution(manager, routing, solution):
    """Get optimization results"""
    result = dict()
    for vehicle_id in range(2):
        index = routing.Start(vehicle_id)
        route_distance = 0
        route_load = 0
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            route_load = solution.Value(capacity.CumulVar(index))
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id
            )
            result[previous_index] = {'地点': node_index, '累積積載量': route_load, '累積距離': route_distance, 'vehicle_id': vehicle_id}

    return result

# Print solution on console.
if solution:
    result = get_solution(manager, routing, solution)
    result_table = pd.DataFrame(result).T
    #print(result_table)

result_table['到着順'] = result_table.groupby('vehicle_id').cumcount()
route_v0 = result_table.query('vehicle_id==0').merge(df, left_on='地点', right_on=df.index)
route_v1 = result_table.query('vehicle_id==1').merge(df, left_on='地点', right_on=df.index)

route_v0 = route_v0.append(route_v0.iloc[0])
route_v1 = route_v1.append(route_v1.iloc[0])
depot = route_v0.iloc[0:1]
depot['累積距離'] = 0

trace = []
for route, color, name in zip([route_v0, route_v1, depot], 
                              ['red', 'blue', 'black'], 
                              ['vehicle1', 'vehicle2', 'depot']):
    trace_route = go.Scattermapbox(
        lon = route['経度'],
        lat = route['緯度'],
        mode = 'markers + text + lines',
        text = route['名称'] + '<br>' + 
            '到着順: ' + route['到着順'].astype(str) + '<br>' + 
            'ここまでの累積載量: ' + route['累積積載量'].astype(str) + '<br>' + 
            '次地点到着までの累積距離: ' + route['累積距離'].astype(str),
        marker = dict(size=route['絵本の蔵書数']/1000),
        line = dict(color=color),
        name=name
    )
    trace.append(trace_route)

data = trace

fig = go.Figure(data)
fig.update_layout(mapbox_style="open-street-map",
                  mapbox_center_lat=35.6,
                  mapbox_center_lon=139.6,
                  mapbox={'zoom': 10},
                  margin={"r":0,"t":0,"l":0,"b":0},
)
fig.write_html("wwwroot\html\map.html")
#fig.show()

#print("Err")

print("Complete")
