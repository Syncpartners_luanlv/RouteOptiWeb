﻿using RouteOptiWeb.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// MSSQLAccess の概要の説明です
/// </summary>
public class MSSQLAccess : IDisposable
{
    protected SqlConnection conn;
    protected SqlCommand cmd;
    protected SqlDataAdapter da;
    protected SqlDataReader rd;
    protected SqlTransaction trn;
    protected bool trn_rollback_flag = false;


    // protected string ConnectionString = "Data Source=DESKTOP-EBPSOSP;Initial Catalog=master;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
    //protected string ConnectionString = "Data Source=192.168.1.36;Initial Catalog=employee;User ID=tozanadmin;Password=0526250148";
    protected string ConnectionString = new GetConnectString().ConnectionString;
    //protected string ConnectionString = "Server=tcp:extozan.database.windows.net,1433;Initial Catalog=extozan;Persist Security Info=False;User ID=db-extozanadmin;Password=2Gn5.n.MZDzrKQh9MnjY5uE9#Pj63Ea5;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;";

    protected DataTable dt01;

    protected List<SQLParam> lstMbrSQLParam = new List<SQLParam>();

    //SQL Paramater用構造体
    protected struct SQLParam
    {
        public string strParam;
        public SqlDbType dbType;
        public object objValue;
    }

    //=================================================
    //----- DB接続を行うためのClass
    //=================================================

    public string ADB()
    {
        //機能 　  : DB接続
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2009年12月07日
        //作成者   : 加藤  宏章
        //機能説明 : SQLサーバの接続文字列
        //注意事項 : 
        //
        //___________________________________________________________________________________

        //テスト用  :   本番 TozandoDB
        //ConnectionString = SQLtzn.TozanDB();
        //ConnectionString = SQLtzn.TozandoDB();
        return ConnectionString;

    }

    /// <summary>
    /// このSQLクラスで使用するDBを指定します。
    /// </summary>
    /// <param name="db">"ascdb" / "laplacedb" / "tozandb" / (ConectionString直接指定)</param>
    public MSSQLAccess(string db)
    {
        //機能 　  : 
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年09月19日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        db = db.ToLower();

        ConnectionString = db;
        conn = new SqlConnection(this.ADB());
        conn.Open();
        cmd = new SqlCommand();
        da = new SqlDataAdapter();
        cmd = conn.CreateCommand();

    }

    public MSSQLAccess()
    {
        //機能 　  : 
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年09月19日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        //テスト用　本番 TozandoDB
        //ConnectionString = SQLtzn.TozanDB();

        //ConnectionString = SQLtzn.TozandoDB();

        conn = new SqlConnection(this.ADB());
        conn.Open();

        cmd = new SqlCommand();
        da = new SqlDataAdapter();
        cmd = conn.CreateCommand();

    }

    public bool SetTransaction()
    {
        //機能 　  : トランザクションの開始
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年10月26日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________
        try
        {
            trn = conn.BeginTransaction();
            //トランザクション処理の開始
            cmd.Transaction = trn;
            //コマンドをトランザクション処理に組み入れる

            return true;

        }
        catch (Exception ex)
        {
            if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
            {
                //代表的なエラー　nothingやNULLや例外

            }
            return false;
        }

    }

    public bool CommitTransaction()
    {
        //機能 　  : トランザクションのコミット
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年10月26日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        try
        {
            trn.Commit();
            trn.Dispose();
            trn = null;

            return true;


        }
        catch (Exception ex)
        {
            if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
            {
                //代表的なエラー　nothingやNULLや例外

            }
            RollbackTransaction();

            return false;

        }

    }


    private bool RollbackTransaction()
    {
        //機能 　  : トランザクションのロールバック
        //返り値   : True :ロールバックした
        //           False:ロールバックしなかった
        //引き数　 : なし
        //作成日 　: 2013年08月30日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________



        if ((trn == null) == true)
        {
            //Transactionオブジェクトが解放(trn = Nothing)済みの場合はCommit済みなので、ロールバックしない。
            return false;


        }
        else
        {
            trn.Rollback();
            //トランザクション処理のRollback
            trn_rollback_flag = true;
            //RollBackフラグをON
            trn.Dispose();
            trn = null;
            return true;

            //ViewParamTable() ←デバッグ時、コメントのままクイックウォッチするとパラメータの一覧が確認できます。
            //cmd.CommandText  ←デバッグ時、コメントのままクイックウォッチすると最後に実行されたSQL構文が確認できます。

        }


    }

    public void ResetTransactionFlag()
    {
        //機能 　  : トランザクションのフラグのリセット
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年10月26日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        trn_rollback_flag = false;

    }

    public DataTable SelectData(string SELECT_CMD)
    {
        //機能 　  : SQLの実行(Select文)
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年09月19日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________
        int i = 0;
        int j = 0;
        List<SqlParameter> lstParam = new List<SqlParameter>();
        DataTable dt1 = new DataTable();

        if (SELECT_CMD.Contains("SELECT") | SELECT_CMD.Contains("select"))
        {
            cmd.CommandText = SELECT_CMD;
            da.SelectCommand = cmd;

            cmd.Parameters.Clear();


            if (lstMbrSQLParam.Count ==0)
            {
                da.Fill(dt1);
                //SELECTの実行
            }
            else
            {
                for (i = 0; i <= lstMbrSQLParam.Count - 1; i++)
                {
                    lstParam.Add(new SqlParameter(lstMbrSQLParam[i].strParam, lstMbrSQLParam[i].dbType));
                    cmd.Parameters.Add(lstParam[i]);
                }

                for (j = 0; j <= lstParam.Count - 1; j++)
                {
                    lstParam[j].Value = lstMbrSQLParam[j].objValue;
                }
                da.SelectCommand.CommandTimeout = 0;
                da.Fill(dt1);
                //SELECTの実行

            }

        }

        return dt1;

    }

    public DataTable StoredSelectData(string STORED_PROCEDURE_CMD)
    {
        //機能 　  : ストアドプロシージャ文を使ってDataTable取り出し
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年09月19日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________
        int i = 0;
        int j = 0;
        List<SqlParameter> lstParam = new List<SqlParameter>();
        DataTable dt1 = new DataTable();

        //コマンドタイプをストアドプロシージャーにする
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = STORED_PROCEDURE_CMD;
        da.SelectCommand = cmd;

        cmd.Parameters.Clear();

        if (lstMbrSQLParam.Count == 0)
        {
            //'パラメータなし
            da.Fill(dt1);
            //SELECTの実行


        }
        else
        {
            for (i = 0; i <= lstMbrSQLParam.Count - 1; i++)
            {
                lstParam.Add(new SqlParameter(lstMbrSQLParam[i].strParam, lstMbrSQLParam[i].dbType));
                cmd.Parameters.Add(lstParam[i]);
            }

            for (j = 0; j <= lstParam.Count - 1; j++)
            {
                lstParam[j].Value = lstMbrSQLParam[j].objValue;
            }
            da.Fill(dt1);
            //SELECTの実行

        }


        //コマンドタイプを元に戻す
        cmd.CommandType = CommandType.Text;

        return dt1;

    }


    public bool ExecuteReaderSetSQL(string SELECT_CMD)
    {
        //bool functionReturnValue = false;
        //機能 　  : SQLの実行(Select文)
        //返り値   : 読取成功True,失敗はFalse 
        //引き数　 : ARG1 - Select文
        //作成日 　: 2012年11月17日
        //作成者   : 星野　邦晴
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________
        //'SQL構文の実行をします。
        int i = 0;
        int j = 0;
        List<SqlParameter> lstParam = new List<SqlParameter>();
        dt01 = new DataTable();


        if (SELECT_CMD.Contains("SELECT") | SELECT_CMD.Contains("select"))
        {
            cmd.CommandText = SELECT_CMD;
            cmd.Parameters.Clear();

            if (lstMbrSQLParam.Count == 0)
            {
                try
                {
                    //'パラメータなし、ExcecuteReaderで実行
                    rd = cmd.ExecuteReader();
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
                    {
                        //代表的なエラー　nothingやNULLや例外

                    }
                    return false;
                    //return functionReturnValue;
                }

            }
            else
            {
                for (i = 0; i <= lstMbrSQLParam.Count - 1; i++)
                {
                    lstParam.Add(new SqlParameter(lstMbrSQLParam[i].strParam, lstMbrSQLParam[i].dbType));
                    cmd.Parameters.Add(lstParam[i]);
                }

                //'For i = 0 To lstMbrSQLParam(0).objValue.Count - 1
                //'ExecuteReaderは実行が一つしかできないためパラメータの値の複数は対応できず
                i = 0;
                for (j = 0; j <= lstParam.Count - 1; j++)
                {
                    lstParam[j].Value = lstMbrSQLParam[j].objValue;
                }
                try
                {
                    //'パラメータなし、ExcecuteReaderで実行
                    rd = cmd.ExecuteReader();
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
                    {
                        //代表的なエラー　nothingやNULLや例外

                    }
                    return false;
                    //return functionReturnValue;
                }
                //'Next

            }

            return true;
        }
        else
        {
            return false;
        }
        //return functionReturnValue;

    }

    public Dictionary<string, object> ExecuteReader()
    {
        //機能 　  : 1行1行　実行して値の取り出しを行う
        //返り値   : フィールド名と値を返す 
        //引き数　 : なし
        //作成日 　: 2012年11月17日
        //作成者   : 星野　邦晴
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________
        int i = 0;
        Dictionary<string, object> dict = new Dictionary<string, object>();

        try
        {
            if (rd.Read())
            {
                for (i = 0; i <= rd.GetSchemaTable().Rows.Count - 1; i++)
                {
                    dict.Add(rd.GetName(i), rd.GetValue(i));
                }
            }

        }
        catch (Exception ex)
        {
            if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
            {
                //代表的なエラー　nothingやNULLや例外

            }
            ExecuteReaderClose();
        }

        return dict;
    }

    public void ExecuteReaderClose()
    {
        //機能 　  : ExecuteReader は解放しないと次が使えない
        //返り値   : 解放処理
        //引き数　 : なし
        //作成日 　: 2012年11月17日
        //作成者   : 星野　邦晴
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        rd.Close();
        rd = null;
    }

    public bool InsertData(string INSERT_CMD)
    {
        //bool functionReturnValue = false;
        //機能 　  : SQLの実行(Insert文) パラメータQUERY対応版
        //返り値   : なし
        //引き数　 : 1:SQL構文
        //作成日 　: 2012年09月19日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        //トランザクション処理を行い、ロールバックが発生した場合は、フラグを手動でOFFにするまではInsertとDeleteは動作しない。
        if (trn_rollback_flag == true)
        {
            return false;
            //return functionReturnValue;
        }



        if (INSERT_CMD.Contains("INSERT") | INSERT_CMD.Contains("insert"))
        {
            cmd.CommandText = INSERT_CMD;
            //'da.InsertCommand = cmd

            cmd.Parameters.Clear();

            //パラメータ設定なしの場合------------------
            if (lstMbrSQLParam.Count == 0)
            {

                //トランザクション中かどうか
                if ((trn == null))
                {
                    cmd.ExecuteNonQuery();
                    //INSERTの実行

                }
                else
                {
                    try
                    {
                        cmd.ExecuteNonQuery();
                        //INSERTの実行

                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
                        {
                            //代表的なエラー　nothingやNULLや例外

                        }
                        RollbackTransaction();
                        throw;
                        //return false;
                        //return functionReturnValue;
                    }

                }

                //パラメータ設定ありの場合---------------------------------------------
            }
            else
            {

                //トランザクション中かどうか
                if ((trn == null))
                {
                    ExecuteNonQueryUsingParameters();
                    //パラメータ付きInsertの実行
                }
                else
                {
                    try
                    {
                        ExecuteNonQueryUsingParameters();
                        //パラメータ付きInsertの実行

                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
                        {
                            //代表的なエラー　nothingやNULLや例外

                        }
                        RollbackTransaction();

                        //ViewParamTable()
                        throw;
                        //return false;
                        //return functionReturnValue;
                    }

                }

            }

            return true;


        }
        else
        {
            return false;

        }
        //return functionReturnValue;

    }

    public bool DeleteData(string DELETE_CMD)
    {
        //bool functionReturnValue = false;
        //機能 　  : SQLの実行(Delete文)
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年09月19日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        //トランザクション処理を行い、ロールバックが発生した場合は、フラグを手動でOFFにするまではInsertとDeleteは動作しない。
        if (trn_rollback_flag == true)
        {
            return false;
            //return functionReturnValue;
        }



        if (DELETE_CMD.Contains("DELETE") | DELETE_CMD.Contains("delete"))
        {
            cmd.CommandText = DELETE_CMD;
            //'da.DeleteCommand = cmd

            cmd.Parameters.Clear();

            //パラメータ設定なしの場合------------------
            if (lstMbrSQLParam.Count == 0)
            {

                //トランザクション中かどうか
                if ((trn == null))
                {
                    cmd.ExecuteNonQuery();
                    //DELETEの実行

                }
                else
                {
                    try
                    {
                        cmd.ExecuteNonQuery();
                        //DELETEの実行

                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
                        {
                            //代表的なエラー　nothingやNULLや例外

                        }
                        RollbackTransaction();

                        return false;
                        //return functionReturnValue;
                    }

                }

                //パラメータ設定ありの場合---------------------------------------------
            }
            else
            {

                //トランザクション中かどうか
                if ((trn == null))
                {
                    ExecuteNonQueryUsingParameters();
                    //パラメータ付きDELETEの実行

                }
                else
                {
                    try
                    {
                        ExecuteNonQueryUsingParameters();
                        //パラメータ付きDELETEの実行

                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
                        {
                            //代表的なエラー　nothingやNULLや例外

                        }
                        RollbackTransaction();

                        return false;
                        //return functionReturnValue;
                    }

                }
            }

            return true;


        }
        else
        {
            return false;

        }
        //return functionReturnValue;

    }

    public bool UpdateData(string UPDATE_CMD)
    {
        //bool functionReturnValue = false;
        //機能 　  : SQLの実行(Update文)
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年09月19日
        //作成者   : 
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________


        if (UPDATE_CMD.Contains("UPDATE") | UPDATE_CMD.Contains("update"))
        {
            cmd.CommandText = UPDATE_CMD;
            //'da.UpdateCommand = cmd

            cmd.Parameters.Clear();

            //パラメータ設定なしの場合------------------
            if (lstMbrSQLParam.Count == 0)
            {
                //トランザクション中かどうか
                if ((trn == null))
                {
                    cmd.ExecuteNonQuery();
                    //UPDATEの実行
                }
                else
                {
                    try
                    {
                        cmd.ExecuteNonQuery();
                        //UPDATEの実行

                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
                        {
                            //代表的なエラー　nothingやNULLや例外

                        }
                        RollbackTransaction();

                        return false;
                        //return functionReturnValue;
                    }

                }

                //パラメータ設定ありの場合---------------------------------------------
            }
            else
            {

                //トランザクション中かどうか
                if ((trn == null))
                {
                    ExecuteNonQueryUsingParameters();
                    //パラメータ付きUpdateの実行

                }
                else
                {
                    try
                    {
                        ExecuteNonQueryUsingParameters();
                        //パラメータ付きUpdateの実行

                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException ||
                        ex is InvalidOperationException ||
                        ex is ArgumentOutOfRangeException)
                        {
                            //代表的なエラー　nothingやNULLや例外

                        }
                        RollbackTransaction();
                        throw;
                        //return false;
                        //return functionReturnValue;
                    }

                }
            }

            return true;


        }
        else
        {
            return false;

        }
        //return functionReturnValue;


    }

    private void ExecuteNonQueryUsingParameters()
    {
        //機能 　  : パラメータ付きSQLの実行
        //返り値   : なし
        //引き数　 : なし
        //作成日 　: 2012年11月16日
        //作成者   : 
        //機能説明 : 
        //注意事項 : cmd.CommandText にSQL構文がセットされている前提で動作します。
        //
        //___________________________________________________________________________________
        int i = 0;
        int j = 0;
        List<SqlParameter> lstParam = new List<SqlParameter>();

        for (i = 0; i <= lstMbrSQLParam.Count - 1; i++)
        {
            lstParam.Add(new SqlParameter(lstMbrSQLParam[i].strParam, lstMbrSQLParam[i].dbType));
            cmd.Parameters.Add(lstParam[i]);
        }


        for (j = 0; j <= lstParam.Count - 1; j++)
        {
            lstParam[j].Value = lstMbrSQLParam[j].objValue;
        }

        //エラー時のデバッグ用(本番ではコメント化を推奨)
        string strSQL = cmd.CommandText;
        //strSQL = SQL構文
        DataTable dt1 = ViewParamTable();
        //dt1    = パラメータ一覧

        cmd.ExecuteNonQuery();
        //SQLの実行 

    }

    public object ExecuteScalar(string SQL_CMD)
    {
        //機能 　  : 返り値が１件(１行かつ１フィールド)しか返らないSQLを実行するときに使用する
        //返り値   : SQLの返り値
        //引き数　 : SQL構文
        //作成日 　: 2011年09月23日
        //作成者   : 加藤　宏章
        //機能説明 : 
        //注意事項 : ExecuteNonQueryとは違い、パラメータをList型で複数渡しても１度しか実行しない。
        //
        //___________________________________________________________________________________

        int i = 0;
        int j = 0;
        List<SqlParameter> lstParam = new List<SqlParameter>();
        cmd.CommandText = SQL_CMD;

        cmd.Parameters.Clear();

        object parm = "";

        if (lstMbrSQLParam.Count == 0)
        {
            parm = (object)cmd.ExecuteScalar();

        }
        else
        {
            for (i = 0; i <= lstMbrSQLParam.Count - 1; i++)
            {
                lstParam.Add(new SqlParameter(lstMbrSQLParam[i].strParam, lstMbrSQLParam[i].dbType));
                cmd.Parameters.Add(lstParam[i]);
            }

            for (j = 0; j <= lstParam.Count - 1; j++)
            {
                lstParam[j].Value = lstMbrSQLParam[j].objValue;
            }

            //ViewParamTable() ←エラー時、この関数をクイックウォッチするとパラメータ一覧が確認できる。

            parm = (object)cmd.ExecuteScalar();

        }
        return parm;

    }

    /// <summary>
    /// SQL構文のパラメータを指定します。
    /// 第３引数をObject型のList変数で渡すと、自動的に複数回のSQL(UPDATE/INSERT)が動作します。
    /// </summary>
    /// <param name="strParam1">@から始まるパラメータ文字列</param>
    /// <param name="dbType1">パラメータ文字列のデータ型(SqlDbType.***)</param>
    /// <param name="Value1">パラメータ文字列に結びつける値</param>
    public void SetParam(string strParam1, SqlDbType dbType1, object Value1)
    {
        //機能 　  : パラメータのセット「SELECTなどはこちら」
        //返り値   : SQLの返り値
        //引き数　 : ARG1 - パラメータ名
        //           ARG2 - パラメータの型
        //           ARG3 - パラメータの値(単一)
        //作成日 　: 2012年11月20日
        //作成者   : 加藤　宏章
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________
        int i;
        SQLParam stcSQLParam = default(SQLParam);

        //既に同一のパラメータ文字列がセットされてたら一旦除去
        for (i = 0; i <= lstMbrSQLParam.Count - 1; i++)
        {
            var _with1 = lstMbrSQLParam[i];
            if (_with1.strParam == strParam1)
            {
                lstMbrSQLParam.RemoveAt(i);
                break; // TODO: might not be correct. Was : Exit For
            }
        }

        stcSQLParam.strParam = strParam1;
        stcSQLParam.dbType = dbType1;
        stcSQLParam.objValue = Value1;

        lstMbrSQLParam.Add(stcSQLParam);

    }

    /// <summary>
    /// SQL構文のパラメータをクリアします。
    /// 引数に@から始まるパラメータ文字列を指定すると、特定のパラメータのみ削除できます。
    /// </summary>
    public void ClearParam()
    {
        //機能 　  : パラメータをすべてクリアーします
        //返り値   : パラメータをなくします
        //引き数　 : '作成日 　: 2012年11月20日
        //作成者   : 加藤　宏章
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        lstMbrSQLParam.Clear();

    }

    /// <summary>
    /// SQL構文のパラメータをクリアします。
    /// 引数に@から始まるパラメータ文字列を指定すると、特定のパラメータのみ削除できます。
    /// </summary>
    /// <param name="strParam1">@から始まるパラメータ文字列</param>
    public void ClearParam(string strParam1)
    {
        //機能 　  : パラメータをすべてクリアーします
        //返り値   : パラメータをなくします
        //引き数　 : '作成日 　: 2012年11月20日
        //作成者   : 加藤　宏章
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________
        int i = 0;

        for (i = 0; i <= lstMbrSQLParam.Count - 1; i++)
        {
            var _with2 = lstMbrSQLParam[i];
            if (_with2.strParam == strParam1)
            {
                lstMbrSQLParam.RemoveAt(i);
                return;
            }

        }

    }
    public DataTable ViewParamTable()
    {
        //機能 　  : パラメータ一覧の表示
        //返り値   : パラメータ一覧のDataTable
        //引き数　 : 
        //作成日 　: 2012年12月11日
        //作成者   : 加藤　宏章
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        DataTable dt1 = default(DataTable);
        DataRow dr1 = default(DataRow);
        int i;
        dt1 = new DataTable();
        //dt1.TableName = ""
        dt1.Columns.Add("strParam", Type.GetType("System.String"));
        dt1.Columns.Add("dbType", Type.GetType("System.String"));
        dt1.Columns.Add("objValue", Type.GetType("System.String"));

        for (i = 0; i <= lstMbrSQLParam.Count - 1; i++)
        {
            var _with3 = lstMbrSQLParam[i];
            dr1 = dt1.NewRow();
            dr1["strParam"] = _with3.strParam;
            dr1["dbType"] = _with3.dbType;
            dr1["objValue"] = _with3.objValue;
            dt1.Rows.Add(dr1);
        }

        return dt1;

    }
    public bool GetRollbackFlag()
    {

        return trn_rollback_flag;

    }

    // 重複する呼び出しを検出するには
    private bool disposedValue = false;

    // IDisposable
    protected virtual void Dispose(bool disposing)
    {
        //機能 　  : SQL文が終了すると自動でDisposeします
        //返り値   : トランザクション、コネクション、データリーダなどのすべて解放します
        //引き数　 : '作成日 　: 2012年11月20日
        //作成者   : 加藤　宏章
        //機能説明 : 
        //注意事項 : 
        //
        //___________________________________________________________________________________

        if (!this.disposedValue)
        {
            if (disposing)
            {
                // TODO: 他の状態を解放します (マネージ オブジェクト)。
            }

            // TODO: ユーザー独自の状態を解放します (アンマネージ オブジェクト)。

            //Disposeの時点でTransactionオブジェクトが解放(trn = Nothing)されずに残っている場合、エラーとみなし、トランザクション処理のRollback。
            //これを防ぐには、必ず CommitTransaction() を実行すること。
            RollbackTransaction();

            if ((rd == null) == false)
            {
                rd.Close();
                rd = null;
            }
            da.Dispose();
            cmd.Dispose();
            conn.Close();
            conn.Dispose();

            // TODO: 大きなフィールドを null に設定します。
        }
        this.disposedValue = true;
    }

    #region " IDisposable Support "
    // このコードは、破棄可能なパターンを正しく実装できるように Visual Basic によって追加されました。
    public void Dispose()
    {
        // このコードを変更しないでください。クリーンアップ コードを上の Dispose(ByVal disposing As Boolean) に記述します。
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    #endregion
}