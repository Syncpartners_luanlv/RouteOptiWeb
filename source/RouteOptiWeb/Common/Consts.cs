﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace RouteOptiWeb.Common
{
    // シングルトン
    public static class Consts
    {
        // JSON KEY
        public const string C_JSON_NAME = "NAME";
        public const string C_JSON_SEQID = "SEQID";
        public const string C_JSON_STATUS = "STATUS";
        public const string C_JSON_MESSAGE = "MESSAGE";
        public const string C_JSON_PROGRESS = "PROGRESS";

        // JSON DATA
        public const string C_JSON_OK = "OK";
        public const string C_JSON_NG = "NG";
        public const string C_JSON_NG_CONTINUE = "しばらくお待ちください...";

        // PYTHON MESSAGE
        public const string C_PYTHON_COMPLETE = "Complete";


    }
}